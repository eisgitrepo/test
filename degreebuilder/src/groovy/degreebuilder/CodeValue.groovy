package degreebuilder

class CodeValue {
	String code
	String value
	
	CodeValue (String code, String value){
		this.code = code
		this.value = value
	}
	
	String toString(){
		"${code} - ${value}"
	}
	
}
