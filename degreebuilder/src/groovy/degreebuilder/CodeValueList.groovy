package degreebuilder

import java.util.List;
import java.lang.String;

class CodeValueList {
	List<CodeValue> codeValues

	CodeValueList(List<Map> listOfMaps) {
		// each map is a row in a result set - the key is the column name - the value is the data
		// this constructor assumes that the sql has only one column called '*_code', and only one column called '*_desc'
		codeValues = new ArrayList<CodeValue>()
		String code
		String value
		listOfMaps.each() {
			code = null
			value = null
			it.each {
				String keyAsString = it.key as String
				if ((keyAsString).substring(keyAsString.size() - 5).toUpperCase() == '_CODE') {
					code = it.value
				}
				if ((keyAsString).substring(keyAsString.size() - 5).toUpperCase() == '_DESC') {
					value = it.value
				}
			}
			if (code && value){
				CodeValue codeValue = new CodeValue (code, value)
				codeValues.add(codeValue)
			}
		}
	}
	
}