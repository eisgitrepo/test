package degreebuilder;

import org.springframework.security.core.GrantedAuthority;

public class NetIdGrantedAuthority implements GrantedAuthority{
	
	/**
	 * Default Serial Version UID.
	 */
	private static final long serialVersionUID = 1L;
	
	/**
	 * The role granted this netId user.
	 */
	private String role;
	
	/**
	 * Single step constructor
	 * @param role
	 */
	public NetIdGrantedAuthority (String role){
		this.role = role;
	}
	
	/**
	 * No-arg constructor
	 */
	public NetIdGrantedAuthority(){
		this(null);
	}
	
	/*(non-Javadoc)
	 * @see org.springframework.security.core.GrantedAuthority#getAuthority()
	 */
	public String getAuthority(){
		return this.role;
	}
	
	
}
