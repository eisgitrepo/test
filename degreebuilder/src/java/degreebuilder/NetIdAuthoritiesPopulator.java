package degreebuilder;

import java.util.HashSet;
import java.util.Set;

import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import edu.internet2.middleware.grouperClient.api.GcGetGroups;
import edu.internet2.middleware.grouperClient.util.GrouperClientUtils;
import edu.internet2.middleware.grouperClient.ws.beans.WsGetGroupsResult;
import edu.internet2.middleware.grouperClient.ws.beans.WsGetGroupsResults;
import edu.internet2.middleware.grouperClient.ws.beans.WsGroup;

public class NetIdAuthoritiesPopulator implements UserDetailsService {

	/**
	 * Prefix of user role.
	 */
	private static final String ROLE_PREFIX = "ROLE_";
	public static final String ROLE_AUTHENTICATED = ROLE_PREFIX + "AUTHENTICATED";
	public static final String ROLE_STUDENT = ROLE_PREFIX + "STUDENT";
	public static final String ROLE_EMPLOYEE = ROLE_PREFIX + "EMPLOYEE";
	
	public NetIdAuthoritiesPopulator(){
	}
	
	/* (non-Javadoc)
	 * @see org.springframework.security.core.userdetails.UserDetailsService#loadUserByUsername(java.lang.String)
	 */
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		//Check for valid netId
		if (!username.matches("^[a-z]{2}[0-9]{6}[e|a]?$"))
			throw new UsernameNotFoundException("User Not Found for name: " + username);
 
		//Create a set of roles and add the givens...
		Set<NetIdGrantedAuthority> roles = new HashSet<NetIdGrantedAuthority>();
		roles.add(new NetIdGrantedAuthority("ROLE_AUTHENTICATED"));
		roles.add(new NetIdGrantedAuthority(username.endsWith("e") ? "ROLE_EMPLOYEE" : "ROLE_STUDENT"));
		roles.addAll(this.getGrouperRoles(username));
		return new User(username, "{HIDDEN}", roles);
	}
 
	/**
	 * Make a connection to the grouper service and get existing groups ("roles") for the
	 * given NetId
	 * @param username
	 * @return
	 */
	private Set<NetIdGrantedAuthority> getGrouperRoles(String username){
		/*
		 * Set service connection properties...
		 */
		GrouperClientUtils.grouperClientOverrideMap().put("grouperClient.webService.url", System.getProperty("grouper.serviceUrl"));
		GrouperClientUtils.grouperClientOverrideMap().put("grouperClient.webService.login", System.getProperty("grouper.user"));
		GrouperClientUtils.grouperClientOverrideMap().put("grouperClient.webService.password", System.getProperty("grouper.password"));
 
		/*
		 * Create a set of roles from the groups received
		 */
		Set<NetIdGrantedAuthority> roles = new HashSet<NetIdGrantedAuthority>();
		GcGetGroups getGroups = new GcGetGroups();
		getGroups.addSubjectId(username);
		WsGetGroupsResults results = getGroups.execute();
		for (WsGetGroupsResult result : results.getResults())
			{
			if(result.getWsGroups() != null)
				{
				for (WsGroup group : result.getWsGroups())
					{
					String role = ROLE_PREFIX + group.getName().replaceAll(":", "_").toUpperCase();					
					roles.add(new NetIdGrantedAuthority(role));
					}
				}
			}
		return roles;
		}
 
	}

