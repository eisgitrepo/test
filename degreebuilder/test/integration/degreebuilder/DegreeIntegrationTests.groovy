package degreebuilder

import static org.junit.Assert.*

import java.util.Date;

import org.junit.*

class DegreeIntegrationTests {

	@Before
	void setUp() {
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	@Test
	void testWhenValidThenOk() {
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		assertNotNull degree1.save()
		assertNotNull degree1.id
	}

	@Ignore
	void testWhenNullDateCreatedThenNotOk() {
		def degree1 = new Degree (department: 'CS', subject: 'Computer Science', catalogYear: '2013-2014')
		assertNull degree1.save()
		assertNull degree1.id
	}
}