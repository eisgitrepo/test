package degreebuilder

import static org.junit.Assert.*

import java.util.Date;

import org.junit.*

class CopyOfCourseIntegrationTests {

    @Before
    void setUp() {
        // Setup logic here
    }

    @After
    void tearDown() {
        // Tear down logic here
    }

    @Test
    void testWhenValidThenOk() {
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		assertNotNull degree1.save()
		assertNotNull degree1.id
		def degreeBlock1 = new DegreeBlock (degree: degree1, dateCreated: new Date())
		assertNotNull degreeBlock1.save()
		assertNotNull degreeBlock1.id
		def courseBlock1 = new CourseBlock (degreeBlock: degreeBlock1, dateCreated: new Date())
		assertNotNull courseBlock1.save()
		assertNotNull courseBlock1.id
		def course1 = new Course (courseBlock: courseBlock1, dateCreated: new Date())
		assertNotNull course1.save()
		assertNotNull course1.id
    }
	
	@Ignore
	void testWhenNullDateCreatedThenNotOk() {
		def degree1 = new Degree (department: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		assertNotNull degree1.save()
		assertNotNull degree1.id
		def degreeBlock1 = new DegreeBlock (degree: degree1, dateCreated: new Date())
		assertNotNull degreeBlock1.save()
		assertNotNull degreeBlock1.id
		def courseBlock1 = new CourseBlock (degreeBlock: degreeBlock1)
		assertNull courseBlock1.save()
		assertNull courseBlock1.id
		def course1 = new Course (courseBlock: courseBlock1)
		assertNull course1.save()
		assertNull course1.id
	}
}