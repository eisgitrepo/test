package degreebuilder
import org.junit.Ignore;
import static org.junit.Assert.*

import java.util.Date;

import org.junit.*

class DegreeBlockIntegrationTests {

    @Before
    void setUp() {
        // Setup logic here
    }

    @After
    void tearDown() {
        // Tear down logic here
    }

    @Test
    void testWhenValidThenOk() {
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		assertNotNull degree1.save()
		assertNotNull degree1.id
		def degreeBlock1 = new DegreeBlock (degree: degree1, dateCreated: new Date())
		assertNotNull degreeBlock1.save()
		assertNotNull degreeBlock1.id
		def foundDegreeBlock = DegreeBlock.get(degreeBlock1.id)
		assertNotNull foundDegreeBlock
    }
	
	@Ignore
	void testWhenNullDateCreatedThenNotOk() {
		def degree1 = new Degree (department: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		assertNotNull degree1.save()
		assertNotNull degree1.id
		def degreeBlock1 = new DegreeBlock (degree: degree1)
		assertNull degreeBlock1.save()
		assertNull degreeBlock1.id
	}
}