package degreebuilder

import static org.junit.Assert.*
import org.junit.*

class DegreeResourceServiceIntegrationTests extends GroovyTestCase {
	DegreeResourceService degreeResourceService
	
	@Before
	void setUp() {
		// Setup logic here
	}

	@After
	void tearDown() {
		// Tear down logic here
	}

	@Test
	void testServiceExists(){
		assertNotNull degreeResourceService
	}
	
	@Ignore
	//@Test
	void testWhenDegreeValidThenSaveOk() {
		def degree1 = new Degree()
		assertNotNull degree1.save()
	}

	@Ignore
	//@Test
	void testWhenDegreeBlockValidThenSaveOk() {
		def degree1 = new Degree().save()
		def degreeBlock1 = new DegreeBlock(degree: degree1)
		assertNotNull degreeBlock1.save()
	}
	@Ignore
	//@Test
	void testWhenCourseBlockValidThenSaveOk() {
		def degree1 = new Degree().save()
		def degreeBlock1 = new DegreeBlock(degree: degree1)
		def courseBlock1 = new CourseBlock(degreeBlock: degreeBlock1)
		assertNotNull courseBlock1.save()
	}

	@Ignore
	//@Test
	void testWhenCourseBlockValidThenSaveOk2() {
		def degree1 = new Degree()
		def degreeBlock1 = new DegreeBlock()
		def courseBlock1 = new CourseBlock()
		def course1 = new Course()
		courseBlock1.addToCourses(course1)
		degreeBlock1.addToCourseBlocks(courseBlock1)
		degree1.addToDegreeBlocks(degreeBlock1)
		degree1.validate()
	}

	@Ignore
	void testWhenCourseBlockValidThenSaveOk3() {
		def degree1 = new Degree()
		assertNotNull degree1.save()
		assertNotNull degree1.id
		def degreeBlock1 = new DegreeBlock()
		assertNotNull degreeBlock1.save(degree: degree1)
		assertNotNull degreeBlock1.id
	}
	
	@Ignore
	//@Test
	void testSortDegreeBlockTypes() {
    	def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '1')
    	def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '2')
		def degreeBlockTypes = []
		degreeBlockTypes = [degreeBlockType1, degreeBlockType2]
		      
		degreeBlockTypes = degreebuilderService.sortDegreeBlockTypes (degreeBlockTypes)
	}

	@Ignore
	//@Test
	void testSortDegreeBlocks() {
		def degreeBlock1 = new DegreeBlock()
		def courseBlock1 = new CourseBlock()
		degreeBlock1.addToCourseBlocks(courseBlock1)
		def degreeBlocks = [degreeBlock1]
		degreeBlocks = degreebuilderService.sortDegreeBlocks (degreeBlocks)
		assertNotNull degreeBlocks
	}

	@Ignore
	//@Test
	void testCopyDegreeWithNoDegreeBlocks() {
		def degree1 = new Degree (department: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date()).save()
//		assertNotNull degreebuilderService.deepClone(degree1)
		def degree1Clone = degreebuilderService.deepClone(degree1)
		assertEquals degree1, degree1Clone
	}

	@Ignore
	//@Test
	void testCopyDegreeWith1DegreeBlockNoCourseBlocks() {
		def degree1 = new Degree (department: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date()).save()
		def degreeBlock1 = new DegreeBlock()
		degree1.addToDegreeBlocks(degreeBlock1)
//		assertNotNull degreebuilderService.deepClone(degree1)
		def degree1Clone = degreebuilderService.deepClone(degree1)
		assertEquals degree1, degree1Clone
	}
	
	@Ignore
	@Test
	void testGetCourseDescriptionShouldPass() {
		degreebuilderService.getCourseDescription ('AAS','262')
	}
}	
