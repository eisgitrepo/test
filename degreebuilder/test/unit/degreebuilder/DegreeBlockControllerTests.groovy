package degreebuilder



import org.junit.*
import grails.test.mixin.*

/**
 * DegreeBlockControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(DegreeBlockController)
@Mock(DegreeBlock)
class DegreeBlockControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/degreeBlock/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.degreeBlockInstanceList.size() == 0
        assert model.degreeBlockInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.degreeBlockInstance != null
    }

    void testSave() {
        controller.save()

        assert model.degreeBlockInstance != null
        assert view == '/degreeBlock/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/degreeBlock/show/1'
        assert controller.flash.message != null
        assert DegreeBlock.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlock/list'


        populateValidParams(params)
        def degreeBlock = new DegreeBlock(params)

        assert degreeBlock.save() != null

        params.id = degreeBlock.id

        def model = controller.show()

        assert model.degreeBlockInstance == degreeBlock
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlock/list'


        populateValidParams(params)
        def degreeBlock = new DegreeBlock(params)

        assert degreeBlock.save() != null

        params.id = degreeBlock.id

        def model = controller.edit()

        assert model.degreeBlockInstance == degreeBlock
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlock/list'

        response.reset()


        populateValidParams(params)
        def degreeBlock = new DegreeBlock(params)

        assert degreeBlock.save() != null

        // test invalid parameters in update
        params.id = degreeBlock.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/degreeBlock/edit"
        assert model.degreeBlockInstance != null

        degreeBlock.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/degreeBlock/show/$degreeBlock.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        degreeBlock.clearErrors()

        populateValidParams(params)
        params.id = degreeBlock.id
        params.version = -1
        controller.update()

        assert view == "/degreeBlock/edit"
        assert model.degreeBlockInstance != null
        assert model.degreeBlockInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlock/list'

        response.reset()

        populateValidParams(params)
        def degreeBlock = new DegreeBlock(params)

        assert degreeBlock.save() != null
        assert DegreeBlock.count() == 1

        params.id = degreeBlock.id

        controller.delete()

        assert DegreeBlock.count() == 0
        assert DegreeBlock.get(degreeBlock.id) == null
        assert response.redirectedUrl == '/degreeBlock/list'
    }
}
