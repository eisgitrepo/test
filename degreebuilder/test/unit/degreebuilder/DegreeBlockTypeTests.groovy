package degreebuilder

import static org.junit.Assert.*
import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(DegreeBlockType)
class DegreeBlockTypeTests {

    void testCompareThisObjectGreaterThan1() {
		def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '1')
		def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '1')       
		assertEquals (1, degreeBlockType1.compareTo(degreeBlockType2))
	}

	void testCompareThisObjectGreaterThan2() {
    	def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '1')
    	def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '2')       
    	assertEquals (1, degreeBlockType1.compareTo(degreeBlockType2))
    }
	
    void testCompareThisObjectLessThan1() {
    	def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '1')
    	def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Core Courses', displaySeqno: '2')       
		assertEquals (-1, degreeBlockType1.compareTo(degreeBlockType2))
	}
	
    void testCompareThisObjectLessThan2() {
    	def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '1')
    	def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '2')       
		assertEquals (-1, degreeBlockType1.compareTo(degreeBlockType2))
	}
	
	void testCompareBothObjectsEqual() {
		def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '1')
		def degreeBlockType2 = new DegreeBlockType(name: 'UPPER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '1')
		assertEquals (0, degreeBlockType1.compareTo(degreeBlockType2))
	}
	
	void testGroups() {
		def degreeBlockType1 = new DegreeBlockType(name: 'LOWER_DIVISION_CORE', displayGroup: 'Degree Electives', displaySeqno: '1')
		assertEquals (1,degreeBlockType1.getGroupSeqno('Core Courses'))
		assertEquals (2,degreeBlockType1.getGroupSeqno('Degree Electives'))
		assertEquals (3,degreeBlockType1.getGroupSeqno('Degree Other'))
		assertEquals (4,degreeBlockType1.getGroupSeqno('Degree Specific Gen Ed Courses'))
	}
}
