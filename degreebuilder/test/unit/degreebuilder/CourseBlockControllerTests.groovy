package degreebuilder



import org.junit.*
import grails.test.mixin.*

/**
 * CourseBlockControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(CourseBlockController)
@Mock(CourseBlock)
class CourseBlockControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/courseBlock/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.courseBlockInstanceList.size() == 0
        assert model.courseBlockInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.courseBlockInstance != null
    }

    void testSave() {
        controller.save()

        assert model.courseBlockInstance != null
        assert view == '/courseBlock/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/courseBlock/show/1'
        assert controller.flash.message != null
        assert CourseBlock.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/courseBlock/list'


        populateValidParams(params)
        def courseBlock = new CourseBlock(params)

        assert courseBlock.save() != null

        params.id = courseBlock.id

        def model = controller.show()

        assert model.courseBlockInstance == courseBlock
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/courseBlock/list'


        populateValidParams(params)
        def courseBlock = new CourseBlock(params)

        assert courseBlock.save() != null

        params.id = courseBlock.id

        def model = controller.edit()

        assert model.courseBlockInstance == courseBlock
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/courseBlock/list'

        response.reset()


        populateValidParams(params)
        def courseBlock = new CourseBlock(params)

        assert courseBlock.save() != null

        // test invalid parameters in update
        params.id = courseBlock.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/courseBlock/edit"
        assert model.courseBlockInstance != null

        courseBlock.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/courseBlock/show/$courseBlock.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        courseBlock.clearErrors()

        populateValidParams(params)
        params.id = courseBlock.id
        params.version = -1
        controller.update()

        assert view == "/courseBlock/edit"
        assert model.courseBlockInstance != null
        assert model.courseBlockInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/courseBlock/list'

        response.reset()

        populateValidParams(params)
        def courseBlock = new CourseBlock(params)

        assert courseBlock.save() != null
        assert CourseBlock.count() == 1

        params.id = courseBlock.id

        controller.delete()

        assert CourseBlock.count() == 0
        assert CourseBlock.get(courseBlock.id) == null
        assert response.redirectedUrl == '/courseBlock/list'
    }
}
