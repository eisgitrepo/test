package degreebuilder

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(CourseBlock)
class CourseBlockTests {

    void testWhenValidThenOk() {
		mockForConstraintsTests CourseBlock
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		def degreeBlock1 = new DegreeBlock (degree: degree1, dateCreated: new Date())
		def courseBlock = new CourseBlock (degreeBlock: degreeBlock1, dateCreated: new Date())
		assert courseBlock.validate()
    }
	
	void testWhenNullDegreeBlockThenNotOk() {
		mockForConstraintsTests CourseBlock
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		def degreeBlock1 = new DegreeBlock (degree: degree1, dateCreated: new Date())
		def courseBlock = new CourseBlock (dateCreated: new Date())
		assert !courseBlock.validate()
	}
}