package degreebuilder



import org.junit.*
import grails.test.mixin.*

/**
 * DegreeTypeControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(DegreeTypeController)
@Mock(DegreeType)
class DegreeTypeControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/degreeType/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.degreeTypeInstanceList.size() == 0
        assert model.degreeTypeInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.degreeTypeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.degreeTypeInstance != null
        assert view == '/degreeType/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/degreeType/show/1'
        assert controller.flash.message != null
        assert DegreeType.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeType/list'


        populateValidParams(params)
        def degreeType = new DegreeType(params)

        assert degreeType.save() != null

        params.id = degreeType.id

        def model = controller.show()

        assert model.degreeTypeInstance == degreeType
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeType/list'


        populateValidParams(params)
        def degreeType = new DegreeType(params)

        assert degreeType.save() != null

        params.id = degreeType.id

        def model = controller.edit()

        assert model.degreeTypeInstance == degreeType
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeType/list'

        response.reset()


        populateValidParams(params)
        def degreeType = new DegreeType(params)

        assert degreeType.save() != null

        // test invalid parameters in update
        params.id = degreeType.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/degreeType/edit"
        assert model.degreeTypeInstance != null

        degreeType.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/degreeType/show/$degreeType.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        degreeType.clearErrors()

        populateValidParams(params)
        params.id = degreeType.id
        params.version = -1
        controller.update()

        assert view == "/degreeType/edit"
        assert model.degreeTypeInstance != null
        assert model.degreeTypeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/degreeType/list'

        response.reset()

        populateValidParams(params)
        def degreeType = new DegreeType(params)

        assert degreeType.save() != null
        assert DegreeType.count() == 1

        params.id = degreeType.id

        controller.delete()

        assert DegreeType.count() == 0
        assert DegreeType.get(degreeType.id) == null
        assert response.redirectedUrl == '/degreeType/list'
    }
}
