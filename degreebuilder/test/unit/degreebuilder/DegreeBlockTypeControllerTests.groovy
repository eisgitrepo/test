package degreebuilder



import org.junit.*
import grails.test.mixin.*

/**
 * DegreeBlockTypeControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(DegreeBlockTypeController)
@Mock(DegreeBlockType)
class DegreeBlockTypeControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/degreeBlockType/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.degreeBlockTypeInstanceList.size() == 0
        assert model.degreeBlockTypeInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.degreeBlockTypeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.degreeBlockTypeInstance != null
        assert view == '/degreeBlockType/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/degreeBlockType/show/1'
        assert controller.flash.message != null
        assert DegreeBlockType.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlockType/list'


        populateValidParams(params)
        def degreeBlockType = new DegreeBlockType(params)

        assert degreeBlockType.save() != null

        params.id = degreeBlockType.id

        def model = controller.show()

        assert model.degreeBlockTypeInstance == degreeBlockType
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlockType/list'


        populateValidParams(params)
        def degreeBlockType = new DegreeBlockType(params)

        assert degreeBlockType.save() != null

        params.id = degreeBlockType.id

        def model = controller.edit()

        assert model.degreeBlockTypeInstance == degreeBlockType
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlockType/list'

        response.reset()


        populateValidParams(params)
        def degreeBlockType = new DegreeBlockType(params)

        assert degreeBlockType.save() != null

        // test invalid parameters in update
        params.id = degreeBlockType.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/degreeBlockType/edit"
        assert model.degreeBlockTypeInstance != null

        degreeBlockType.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/degreeBlockType/show/$degreeBlockType.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        degreeBlockType.clearErrors()

        populateValidParams(params)
        params.id = degreeBlockType.id
        params.version = -1
        controller.update()

        assert view == "/degreeBlockType/edit"
        assert model.degreeBlockTypeInstance != null
        assert model.degreeBlockTypeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/degreeBlockType/list'

        response.reset()

        populateValidParams(params)
        def degreeBlockType = new DegreeBlockType(params)

        assert degreeBlockType.save() != null
        assert DegreeBlockType.count() == 1

        params.id = degreeBlockType.id

        controller.delete()

        assert DegreeBlockType.count() == 0
        assert DegreeBlockType.get(degreeBlockType.id) == null
        assert response.redirectedUrl == '/degreeBlockType/list'
    }
}
