package degreebuilder

import static org.junit.Assert.*

import grails.test.mixin.*
import grails.test.mixin.support.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.support.GrailsUnitTestMixin} for usage instructions
 */
@TestFor(DegreeBlock)
class DegreeBlockTests {

    void testWhenValidThenOk() {
		mockForConstraintsTests DegreeBlock
		def degree1 = new Degree (college: 'CS', subject: 'Computer Science', catalogYear: '2013-2014', dateCreated: new Date())
		def degree1Block = new DegreeBlock (degree: degree1, dateCreated: new Date())
		assert degree1Block.validate()
    }
	
	void testWhenNullDegreeThenNotOk() {
		mockForConstraintsTests DegreeBlock
		def degree1Block = new DegreeBlock (dateCreated: new Date())
		assert !degree1Block.validate()
	}
}