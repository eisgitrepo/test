package degreebuilder



import org.junit.*
import grails.test.mixin.*

/**
 * DegreeControllerTests
 * A unit test class is used to test individual methods or blocks of code without considering the surrounding infrastructure
 */
@TestFor(DegreeController)
@Mock(Degree)
class DegreeControllerTests {


    def populateValidParams(params) {
      assert params != null
      // TODO: Populate valid properties like...
      //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/degree/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.degreeInstanceList.size() == 0
        assert model.degreeInstanceTotal == 0
    }

    void testCreate() {
       def model = controller.create()

       assert model.degreeInstance != null
    }

    void testSave() {
        controller.save()

        assert model.degreeInstance != null
        assert view == '/degree/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/degree/show/1'
        assert controller.flash.message != null
        assert Degree.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/degree/list'


        populateValidParams(params)
        def degree = new Degree(params)

        assert degree.save() != null

        params.id = degree.id

        def model = controller.show()

        assert model.degreeInstance == degree
    }

    void testEdit() {
        controller.mergeeditold()

        assert flash.message != null
        assert response.redirectedUrl == '/degree/list'


        populateValidParams(params)
        def degree = new Degree(params)

        assert degree.save() != null

        params.id = degree.id

        def model = controller.mergeeditold()

        assert model.degreeInstance == degree
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/degree/list'

        response.reset()


        populateValidParams(params)
        def degree = new Degree(params)

        assert degree.save() != null

        // test invalid parameters in update
        params.id = degree.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/degree/edit"
        assert model.degreeInstance != null

        degree.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/degree/show/$degree.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        degree.clearErrors()

        populateValidParams(params)
        params.id = degree.id
        params.version = -1
        controller.update()

        assert view == "/degree/edit"
        assert model.degreeInstance != null
        assert model.degreeInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/degree/list'

        response.reset()

        populateValidParams(params)
        def degree = new Degree(params)

        assert degree.save() != null
        assert Degree.count() == 1

        params.id = degree.id

        controller.delete()

        assert Degree.count() == 0
        assert Degree.get(degree.id) == null
        assert response.redirectedUrl == '/degree/list'
    }
}
