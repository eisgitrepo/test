drop sequence course_block_id_sequence
drop synonym course_block_id_sequence

select * from all_synonyms where synonym_name = 'COURSE_BLOCK_ID_SEQUENCE';
select * from all_sequences where sequence_name = 'COURSE_BLOCK_ID_SEQUENCE';
select course_block_id_sequence.nextval from sys.dual;

-- Create sequence 
create sequence JUPITER.COURSE_BLOCK_ID_SEQUENCE
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
cache 20;

-- Create the synonym 
create or replace public synonym COURSE_BLOCK_ID_SEQUENCE
for JUPITER.COURSE_BLOCK_ID_SEQUENCE;

GRANT SELECT ON "JUPITER"."COURSE_BLOCK_ID_SEQUENCE" TO "DEGREQ_USR";
