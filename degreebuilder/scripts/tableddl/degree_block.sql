drop table degree_block;
create table degree_block
( 
  id                   number(10) not null,
  version              number(20,0),
  degree_id            number(10) not null,
  block_comment        clob,
  date_created         date not null, 
  last_updated         date,
  degree_block_type_id number(10) 
)

tablespace jupiter_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64k
    
    next 1m
    minextents 1
    maxextents unlimited
  );
-- create/recreate primary, unique and foreign key constraints 
alter table degree_block
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );

alter table JUPITER.DEGREE_BLOCK 
  drop constraint FK1_DEGREE_ID; 

alter table JUPITER.DEGREE_BLOCK
  add constraint FK1_DEGREE_ID foreign key (DEGREE_ID)
  references JUPITER.DEGREE (ID);

-- grant/revoke object privileges 

grant select, insert, update, delete on degree_block to DEGREQ_USR;
grant select, insert, update, delete on degree_block to UMINST1; 
create public synonym degree_block for jupiter.degree_block;
drop public synonym degree_block;


