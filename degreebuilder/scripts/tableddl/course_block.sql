drop table course_block;
create table course_block
( 
  id                     number(10) not null,
  version                number(20,0),
  degree_block_id        number(10) not null,
  name                   varchar2(500),
  course_block_rule      varchar2(500),
  credits                varchar2(10),
  eval                   varchar2(4),
  prereq_notes           clob,
  course_comment         clob, 
  date_created           date not null, 
  last_updated           date, 
  sort_id                number(10) 
)
tablespace jupiter_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- create/recreate primary, unique and foreign key constraints 
alter table course_block
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- grant/revoke object privileges 

alter table JUPITER.COURSE_BLOCK 
  drop constraint FK1_DEGREE_BLOCK_ID; 

alter table JUPITER.COURSE_BLOCK
  add constraint FK1_DEGREE_BLOCK_ID foreign key (DEGREE_BLOCK_ID)
  references JUPITER.DEGREE_BLOCK (ID);

grant select, insert, update, delete on course_block to degreq_usr;
grant select, insert, update, delete on course_block to UMINST1; 
create public synonym course_block for jupiter.course_block;
drop public synonym course_block;



