drop table degree;

create table degree
( 
  id                      number(10) not null,
  version                 number(20,0),
  department              varchar2(40),
  subject                 varchar2(60),
  catalog_year            varchar2(09),
  degree_level            varchar2(30),
  type                    varchar2(50),
  total_credits           number (3),
  cume_gpa_req            number (3,2),
  capstone                varchar2(30),
  degree_comment          clob,
  concentration           varchar2(30),
  track					  varchar2(50),
  updated_by              varchar2(9),
  date_created            date not null,
  last_updated            date
)
tablespace jupiter_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
  
-- create/recreate primary, unique and foreign key constraints 
alter table degree
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- grant/revoke object privileges 

grant select, insert, update, delete on degree to DEGREQ_USR
grant select, insert, update, delete on degree to UMINST1; 
create public synonym degree for jupiter.degree;
drop public synonym degree;

