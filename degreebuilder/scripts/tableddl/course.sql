drop table course;
create table course   
( 
  id                     number(10) not null,
  version                number(20,0),
  course_block_id        number(10) not null,
  subject_code           varchar2(5),
  course_number          varchar2(5),
  title                  varchar2(80),
  credits                varchar2(8),
  term                   varchar2(40),
  course_year            varchar2(20), 
  date_created           date not null, 
  last_updated           date 

)
tablespace jupiter_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- create/recreate primary, unique and foreign key constraints 
alter table course   
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- grant/revoke object privileges 

alter table JUPITER.COURSE_BLOCK 
  drop constraint FK1_COURSE_BLOCK_ID; 

alter table JUPITER.COURSE   
  add constraint FK1_COURSE_BLOCK_ID foreign key (COURSE_BLOCK_ID)
  references JUPITER.COURSE_BLOCK (ID);

grant select, insert, update, delete on course    to degreq_usr;
grant select, insert, update, delete on course    to UMINST1; 
create public synonym course for jupiter.course;
drop public synonym course;


