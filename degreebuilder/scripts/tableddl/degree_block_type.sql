create table degree_block_type
( 
  id            number(10) not null,
  version       number(20,0),
  name          varchar2(60) not null unique,
  display_group varchar2(30), 
  display_seqno number(3,0),
  date_created  date, 
  last_updated  date 
);

-- create/recreate primary, unique and foreign key constraints 
alter table degree_block_type
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );

-- grant/revoke object privileges 
grant select, insert, update, delete on degree_block_type to DEGREQ_USR;
grant select, insert, update, delete on degree_block_type to UMINST1; 
create public synonym degree_block_type for jupiter.degree_block_type;
