drop sequence degree_block_type_id_sequence
drop synonym degree_block_type_id_sequence

select * from all_synonyms where synonym_name = 'DEGREE_BLOCK_TYPE_ID_SEQUENCE';
select * from all_sequences where sequence_name = 'DEGREE_BLOCK_TYPE_ID_SEQUENCE';
select degree_block_type_id_sequence.nextval from sys.dual;

-- Create sequence 
create sequence JUPITER.DEGREE_BLOCK_TYPE_ID_SEQUENCE
minvalue 1
maxvalue 9999999999
start with 1
increment by 1
cache 20;

-- Create the synonym 
create or replace public synonym DEGREE_BLOCK_TYPE_ID_SEQUENCE
for JUPITER.DEGREE_BLOCK_TYPE_ID_SEQUENCE;

GRANT SELECT ON "JUPITER"."DEGREE_BLOCK_TYPE_ID_SEQUENCE" TO "DEGREQ_USR";
