drop table degree_type;

create table degree_type
( 
  id            number(10) not null, 
  version       number(20,0),
  code          varchar2(8), 
  description   varchar2(80),
  date_created  date,
  last_updated  date
)
tablespace jupiter_tablespace
  pctfree 10
  initrans 1
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
  
-- create/recreate primary, unique and foreign key constraints 
alter table degree_type
  add primary key (id)
  using index 
  tablespace jupiter_tablespace
  pctfree 10
  initrans 2
  maxtrans 255
  storage
  (
    initial 64k
    next 1m
    minextents 1
    maxextents unlimited
  );
-- grant/revoke object privileges 

grant select, insert, update, delete on degree_type to DEGREQ_USR
grant select, insert, update, delete on degree_type to UMINST1; 
create public synonym degree_type for jupiter.degree_type;


