
<%@ page import="degreebuilder.CourseBlock" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'courseBlock.label', default: 'CourseBlock')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-courseBlock" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "name")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.rule.label" default="Rule" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "rule")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.credits.label" default="Credits" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "courseBlockCredits")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.eval.label" default="Eval" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "eval")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.prereqNotes.label" default="Prereq Notes" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "prereqNotes")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.comment.label" default="Comment" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseBlockInstance, field: "courseBlockComment")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${courseBlockInstance?.dateCreated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${courseBlockInstance?.lastUpdated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.courses.label" default="Courses" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${courseBlockInstance.courses}" var="c">
<%--						<li><g:link controller="course" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>--%>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="courseBlock.degreeBlock.label" default="Degree Block" /></td>
				
				<td valign="top" class="value"><g:link controller="degreeBlock" action="show" id="${courseBlockInstance?.degreeBlock?.id}">${courseBlockInstance?.degreeBlock?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
