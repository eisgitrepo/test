<%@ page import="degreebuilder.CourseBlock" %>



			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'name', 'error')} ">
				<label for="name" class="control-label"><g:message code="courseBlock.name.label" default="Name" /></label>
				<div class="controls">
					<g:textField name="name" maxlength="80" value="${courseBlockInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')} ">
				<label for="rule" class="control-label"><g:message code="courseBlock.rule.label" default="Rule" /></label>
				<div class="controls">
					<g:textField name="rule" maxlength="80" value="${courseBlockInstance?.rule}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'courseBlockCredits', 'error')} ">
				<label for="credits" class="control-label"><g:message code="courseBlock.credits.label" default="Credits" /></label>
				<div class="controls">
					<g:textField name="credits" maxlength="10" value="${courseBlockInstance?.courseBlockCredits}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courseBlockCredits', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'eval', 'error')} ">
				<label for="eval" class="control-label"><g:message code="courseBlock.eval.label" default="Eval" /></label>
				<div class="controls">
					<g:textField name="eval" maxlength="4" value="${courseBlockInstance?.eval}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'eval', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'prereqNotes', 'error')} ">
				<label for="prereqNotes" class="control-label"><g:message code="courseBlock.prereqNotes.label" default="Prereq Notes" /></label>
				<div class="controls">
					<g:textArea name="prereqNotes" cols="40" rows="5" maxlength="2000" value="${courseBlockInstance?.prereqNotes}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'prereqNotes', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')} ">
				<label for="comment" class="control-label"><g:message code="courseBlock.comment.label" default="Comment" /></label>
				<div class="controls">
					<g:textArea name="comment" cols="40" rows="5" maxlength="2000" value="${courseBlockInstance?.courseBlockComment}"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'courses', 'error')} ">
				<label for="courses" class="control-label"><g:message code="courseBlock.courses.label" default="Courses" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${courseBlockInstance?.courses?}" var="c">
    <li><g:link controller="course" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="course" action="create" params="['courseBlock.id': courseBlockInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'course.label', default: 'Course')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courses', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'degreeBlock', 'error')} required">
				<label for="degreeBlock" class="control-label"><g:message code="courseBlock.degreeBlock.label" default="Degree Block" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="degreeBlock" name="degreeBlock.id" from="${degreebuilder.DegreeBlock.list()}" optionKey="id" required="" value="${courseBlockInstance?.degreeBlock?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'degreeBlock', 'error')}</span>
				</div>
			</div>

