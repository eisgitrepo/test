
<%@ page import="degreebuilder.CourseBlock" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'courseBlock.label', default: 'CourseBlock')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-courseBlock" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="name" title="${message(code: 'courseBlock.name.label', default: 'Name')}" />
			
				<g:sortableColumn property="rule" title="${message(code: 'courseBlock.rule.label', default: 'Rule')}" />
			
				<g:sortableColumn property="credits" title="${message(code: 'courseBlock.credits.label', default: 'Credits')}" />
			
				<g:sortableColumn property="eval" title="${message(code: 'courseBlock.eval.label', default: 'Eval')}" />
			
				<g:sortableColumn property="prereqNotes" title="${message(code: 'courseBlock.prereqNotes.label', default: 'Prereq Notes')}" />
			
				<g:sortableColumn property="comment" title="${message(code: 'courseBlock.comment.label', default: 'Comment')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${courseBlockInstanceList}" status="i" var="courseBlockInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${courseBlockInstance.id}">${fieldValue(bean: courseBlockInstance, field: "name")}</g:link></td>
			
				<td>${fieldValue(bean: courseBlockInstance, field: "rule")}</td>
			
				<td>${fieldValue(bean: courseBlockInstance, field: "credits")}</td>
			
				<td>${fieldValue(bean: courseBlockInstance, field: "eval")}</td>
			
				<td>${fieldValue(bean: courseBlockInstance, field: "prereqNotes")}</td>
			
				<td>${fieldValue(bean: courseBlockInstance, field: "courseBlockComment")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${courseBlockInstanceTotal}" />
	</div>
</section>

</body>

</html>
