<%@ page import="degreebuilder.Course" %>
			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'courseSubject', 'error')} ">
				<label for="subject" class="control-label"><g:message code="course.subject.label" default="Subject" /></label>
				<div class="controls">
					<g:textField name="subject" maxlength="5" value="${courseInstance?.courseSubject}"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'courseSubject', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'courseNumber', 'error')} ">
				<label for="courseNumber" class="control-label"><g:message code="course.courseNumber.label" default="Course Number" /></label>
				<div class="controls">
					<g:textField name="courseNumber" maxlength="5" value="${courseInstance?.courseNumber}"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'courseNumber', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'title', 'error')} ">
				<label for="title" class="control-label"><g:message code="course.title.label" default="Title" /></label>
				<div class="controls">
					<g:textField name="title" maxlength="80" value="${courseInstance?.title}"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'title', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'courseCredits', 'error')} ">
				<label for="courseCredits" class="control-label"><g:message code="course.credits.label" default="Credits" /></label>
				<div class="controls">
					<g:textField name="courseCredits" maxlength="8" value="${courseInstance?.courseCredits}"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'courseCredits', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'term', 'error')} ">
				<label for="term" class="control-label"><g:message code="course.term.label" default="Term" /></label>
				<div class="controls">
					<g:textField name="term" maxlength="10" value="${courseInstance?.term}"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'term', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: courseInstance, field: 'courseBlock', 'error')} required">
				<label for="courseBlock" class="control-label"><g:message code="course.courseBlock.label" default="Course Block" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="courseBlock" name="courseBlock.id" from="${degreebuilder.CourseBlock.list()}" optionKey="id" required="" value="${courseInstance?.courseBlock?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: courseInstance, field: 'courseBlock', 'error')}</span>
				</div>
			</div>

