
<%@ page import="degreebuilder.Course" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-course" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="subject" title="${message(code: 'course.subject.label', default: 'Subject')}" />
			
				<g:sortableColumn property="courseNumber" title="${message(code: 'course.courseNumber.label', default: 'Course Number')}" />
			
				<g:sortableColumn property="title" title="${message(code: 'course.title.label', default: 'Title')}" />
			
				<g:sortableColumn property="credits" title="${message(code: 'course.credits.label', default: 'Credits')}" />
			
				<g:sortableColumn property="term" title="${message(code: 'course.term.label', default: 'Term')}" />
			
				<g:sortableColumn property="lastUpdated" title="${message(code: 'course.lastUpdated.label', default: 'Last Updated')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${courseInstanceList}" status="i" var="courseInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${courseInstance.id}">${fieldValue(bean: courseInstance, field: "courseSubject")}</g:link></td>
			
				<td>${fieldValue(bean: courseInstance, field: "courseNumber")}</td>
			
				<td>${fieldValue(bean: courseInstance, field: "title")}</td>
			
				<td>${fieldValue(bean: courseInstance, field: "credits")}</td>
			
				<td>${fieldValue(bean: courseInstance, field: "term")}</td>
			
				<td><g:formatDate date="${courseInstance.lastUpdated}" /></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${courseInstanceTotal}" />
	</div>
</section>

</body>

</html>
