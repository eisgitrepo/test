
<%@ page import="degreebuilder.Course" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'course.label', default: 'Course')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-course" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.subject.label" default="Subject" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseInstance, field: "courseSubject")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.courseNumber.label" default="Course Number" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseInstance, field: "courseNumber")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.title.label" default="Title" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseInstance, field: "title")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.credits.label" default="Credits" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseInstance, field: "courseCredits")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.term.label" default="Term" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: courseInstance, field: "term")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${courseInstance?.lastUpdated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.courseBlock.label" default="Course Block" /></td>
				
				<td valign="top" class="value"><g:link controller="courseBlock" action="show" id="${courseInstance?.courseBlock?.id}">${courseInstance?.courseBlock?.encodeAsHTML()}</g:link></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="course.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${courseInstance?.dateCreated}" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
