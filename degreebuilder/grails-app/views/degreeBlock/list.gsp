
<%@ page import="degreebuilder.DegreeBlock" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degreeBlock.label', default: 'DegreeBlock')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-degreeBlock" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="blockType" title="${message(code: 'degreeBlock.blockType.label', default: 'Block Type')}" />
			
				<g:sortableColumn property="comment" title="${message(code: 'degreeBlock.comment.label', default: 'Comment')}" />
			
				<g:sortableColumn property="lastUpdated" title="${message(code: 'degreeBlock.lastUpdated.label', default: 'Last Updated')}" />
			
				<g:sortableColumn property="dateCreated" title="${message(code: 'degreeBlock.dateCreated.label', default: 'Date Created')}" />
			
				<th><g:message code="degreeBlock.degree.label" default="Degree" /></th>
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${degreeBlockInstanceList}" status="i" var="degreeBlockInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${degreeBlockInstance.id}">${fieldValue(bean: degreeBlockInstance, field: "blockType")}</g:link></td>
			
				<td>${fieldValue(bean: degreeBlockInstance, field: "degreeBlockComment")}</td>
			
				<td><g:formatDate date="${degreeBlockInstance.lastUpdated}" /></td>
			
				<td><g:formatDate date="${degreeBlockInstance.dateCreated}" /></td>
			
				<td>${fieldValue(bean: degreeBlockInstance, field: "degree")}</td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${degreeBlockInstanceTotal}" />
	</div>
</section>

</body>

</html>
