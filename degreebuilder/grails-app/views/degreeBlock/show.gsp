
<%@ page import="degreebuilder.DegreeBlock" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degreeBlock.label', default: 'DegreeBlock')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-degreeBlock" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.blockType.label" default="Block Type" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: degreeBlockInstance, field: "blockType")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.comment.label" default="Comment" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: degreeBlockInstance, field: "degreeBlockComment")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${degreeBlockInstance?.lastUpdated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.courseBlocks.label" default="Course Blocks" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${degreeBlockInstance.courseBlocks}" var="c">
						<li><g:link controller="courseBlock" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${degreeBlockInstance?.dateCreated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlock.degree.label" default="Degree" /></td>
				
<%--				<td valign="top" class="value"><g:link controller="degree" action="show" id="${degreeBlockInstance?.degree?.id}">${degreeBlockInstance?.degree?.encodeAsHTML()}</g:link></td>--%>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
