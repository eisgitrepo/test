<%@ page import="degreebuilder.DegreeBlock" %>



			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockInstance, field: 'blockType', 'error')} ">
				<label for="blockType" class="control-label"><g:message code="degreeBlock.blockType.label" default="Block Type" /></label>
				<div class="controls">
					<g:textField name="blockType" maxlength="50" value="${degreeBlockInstance?.blockType}"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockInstance, field: 'blockType', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockInstance, field: 'degreeBlockComment', 'error')} ">
				<label for="comment" class="control-label"><g:message code="degreeBlock.comment.label" default="Comment" /></label>
				<div class="controls">
					<g:textArea name="comment" cols="40" rows="5" maxlength="2000" value="${degreeBlockInstance?.degreeBlockComment}"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockInstance, field: 'degreeBlockComment', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockInstance, field: 'courseBlocks', 'error')} ">
				<label for="courseBlocks" class="control-label"><g:message code="degreeBlock.courseBlocks.label" default="Course Blocks" /></label>
				<div class="controls">
					
<ul class="one-to-many">
<g:each in="${degreeBlockInstance?.courseBlocks?}" var="c">
    <li><g:link controller="courseBlock" action="show" id="${c.id}">${c?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="courseBlock" action="create" params="['degreeBlock.id': degreeBlockInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'courseBlock.label', default: 'CourseBlock')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: degreeBlockInstance, field: 'courseBlocks', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockInstance, field: 'degree', 'error')} required">
				<label for="degree" class="control-label"><g:message code="degreeBlock.degree.label" default="Degree" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select id="degree" name="degree.id" from="${degreebuilder.Degree.list()}" optionKey="id" required="" value="${degreeBlockInstance?.degree?.id}" class="many-to-one"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockInstance, field: 'degree', 'error')}</span>
				</div>
			</div>

