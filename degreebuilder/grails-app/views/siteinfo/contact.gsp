<html>

<head>
<title><g:message code="default.contact.title" /></title>
<meta name="layout" content="kickstart" />
</head>

<body>

	<section id="intro">
		<p class="lead">If you have an issue you would like us to address
			in a future release, or for a new report request, please fill out the
			form below.</p>
	</section>
	<div>

		<div
			class="control-group fieldcontain ${hasErrors(bean: _DemoPageInstance, field: 'name', 'error')} ">
			<label for="name" class="control-label"><g:message
					code="_DemoPage.name.label" default="Name" /></label>
			<div class="controls">
				<g:textField name="name" value="${_DemoPageInstance?.name}" />
				<span class="help-inline"> ${hasErrors(bean: _DemoPageInstance, field: 'name', 'error')}
				</span>
			</div>
		</div>
		<div
			class="control-group fieldcontain ${hasErrors(bean: _DemoPageInstance, field: 'summary', 'error')} ">
			<label for="name" class="control-label"><g:message
					code="_DemoPage.name.label" default="Summary" /></label>
			<div class="controls">
				<g:textField name="summary" value="${_DemoPageInstance?.name}" />
				<span class="help-inline"> ${hasErrors(bean: _DemoPageInstance, field: 'summary', 'error')}
				</span>
			</div>
		</div>
		<div
			class="control-group fieldcontain ${hasErrors(bean: _DemoPageInstance, field: 'description', 'error')} ">
			<label for="name" class="control-label"><g:message
					code="_DemoPage.name.label" default="Description" /></label>
			<div class="controls">
				<g:textArea name="description" value="${_DemoPageInstance?.name}" />
				<span class="help-inline"> ${hasErrors(bean: _DemoPageInstance, field: 'description', 'error')}
				</span>
			</div>
		</div>


	</div>



	<g:submitButton name="create" class="btn btn-primary"
		value="Submit Issue" />

</body>

</html>
