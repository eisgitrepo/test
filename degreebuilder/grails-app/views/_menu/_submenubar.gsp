<!-- 
This menu is used to show function that can be triggered on the content (an object or list of objects).
-->

<%-- Only show the "Pills" navigation menu if a controller exists (but not for home) --%>
<g:if test="${params.controller != null	&&	params.controller != ''	&&	params.controller != 'home'}">
	<ul id="Menu" class="nav nav-pills">

		<g:set var="entityName" value="${message(code: params.controller+'.label', default: params.controller.substring(0,1).toUpperCase() + params.controller.substring(1).toLowerCase())}" />

		<%-- Degree List menu item for Admin pages --%>
		<g:if test="${params.controller == 'degreeType' || params.controller == 'degreeBlockType'|| params.controller == 'keyword'}">
			<li class="${ params.action == "list" ? 'active' : '' }">
				<g:link controller="degree" action="list"><i class="icon-th-list"></i> Degree List</g:link>
			</li>				
		</g:if>
		
		<%-- List menu item for all pages --%>
		<li class="${params.action == "list" ? 'active' : '' }">
			<g:link action="list"><i class="icon-th-list"></i> <g:message code="default.list.label" args="[entityName]"/></g:link>
		</li>

		<%-- New menu item for Admin pages --%>		
		<g:if test="${params.controller == 'degreeType' || params.controller == 'degreeBlockType'|| params.controller == 'keyword'}">
			<li class="${params.action == "create" ? 'active' : '' }">
				<g:link action="create"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
			</li>			
		</g:if>
		<%-- New menu item for non-Admin pages --%>
		<g:else>
			<li class="${params.action == "create" ? 'active' : '' }">
				<g:link action="palette" params="[calledFrom: 'create']"><i class="icon-plus"></i> <g:message code="default.new.label"  args="[entityName]"/></g:link>
			</li>			
		</g:else>

		<%-- Show Page menu items --%>
		<g:if test="${params.action == 'show'}">
			<%-- New menu item for Degree Type pages --%>
			<g:if test="${params.controller == 'degreeType'}">		
				<li class="${params.action == "edit" ? 'active' : '' }">
					<g:link controller="degreeType" action="edit" id="${params.id}"><i class="icon-th-list"></i> Edit Degree Type</g:link>
				</li>
			</g:if>
			
			<%-- New menu item for Degree Block Type pages --%>
			<g:if test="${params.controller == 'degreeBlockType'}">		
				<li class="${params.action == "edit" ? 'active' : '' }">
					<g:link controller="degreeBlockType" action="edit" id="${params.id}"><i class="icon-th-list"></i> Edit Degree Block Type</g:link>
				</li>
			</g:if>

			<%-- New menu item for Keyword pages --%>
			<g:if test="${params.controller == 'keyword'}">		
				<li class="${params.action == "edit" ? 'active' : '' }">
					<g:link controller="keyword" action="edit" id="${params.id}"><i class="icon-th-list"></i> Edit Keyword</g:link>
				</li>
			</g:if>
			
			<%-- Copy & edit menu items for non-Admin pages --%>
			<g:if test="${params.controller != 'degreeType' && params.controller != 'degreeBlockType' && params.controller != 'keyword'}">		
				<li class="${ params.action == "palette" ? 'active' : '' }">
					<g:link action="copy" params="[calledFrom: 'copy']" id="${params.id}"><i class="icon-pencil"></i> Copy Degree</g:link>
				</li>
				
				<li class="${ params.action == "edit" ? 'active' : '' }">
					<g:link action="palette" params="[calledFrom: 'edit']" id="${params.id}"><i class="icon-pencil"></i> <g:message code="default.edit.label"  args="[entityName]"/></g:link>
				</li>
			</g:if>
			<!-- the item is an object (not a list) -->

			<%-- Delete menu item for Admin pages --%>
			<g:if test="${params.controller == 'degreeType' || params.controller == 'degreeBlockType'|| params.controller == 'keyword'}">	
				<li class=""><g:render template="/_common/modals/deleteTextLink"/></li>
			</g:if>	
			<g:else>
			  	<li style="float:right;">
					<input type="button" class="btn btn-primary" id="print" onclick="window.print()" value="Print This Degree">
				</li>
			</g:else>
		</g:if>
		
		<g:if test="${params.action == 'edit' || params.action == 'palette'}">		
			<!-- the item is an object (not a list) -->
			<li class="${ params.action == "show" ? 'active' : '' }">
				<g:link action="show" id="${params.id}" target='_blank'></i> <g:message code="default.show.label"  args="[entityName]"/></g:link>
			</li>
			<li class="${ params.action == "edit" ? 'active' : '' }">
				<g:link controller="degreeKeyword" action="create" id="${params.id}"><i class="icon-pencil"></i>Add Keyword</g:link>
			</li>
			<g:if test="${session.isAdminUser == 'true'}">	
				<li class=""><g:render template="/_common/modals/deleteTextLink"/></li>
			</g:if>	
		</g:if>		

		<g:if test="${session.isAdminUser == 'true'}">
			<g:if test="${params.controller != 'degreeType'}">		
				<li><g:link controller="degreeType" action="list">Degree Type List</g:link></li>
			</g:if>
			<g:if test="${params.controller != 'degreeBlockType'}">	
				<li><g:link controller="degreeBlockType" action="list">Degree Block Type List</g:link></li>
			</g:if>
			<g:if test="${params.controller != 'keyword'}">	
				<li><g:link controller="keyword" action="list">Keyword List</g:link></li>
			</g:if>
			<g:if test="${params.controller != 'keyword'}">	
				<li><g:link controller="degree" action="copyDegrees">Copy Degrees</g:link></li>
			</g:if>
		</g:if>	

	</ul>
</g:if>
