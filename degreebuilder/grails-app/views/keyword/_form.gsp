<%@ page import="degreebuilder.Keyword" %>



			<div class="control-group fieldcontain ${hasErrors(bean: keywordInstance, field: 'value', 'error')} ">
				<label for="value" class="control-label"><g:message code="keyword.value.label" default="Value" /></label>
				<div class="controls">
					<g:textField name="value" maxlength="200" value="${keywordInstance?.value}"/>
					<span class="help-inline">${hasErrors(bean: keywordInstance, field: 'value', 'error')}</span>
				</div>
			</div>

