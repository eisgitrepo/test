
<%@ page import="degreebuilder.Keyword" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'keyword.label', default: 'Keyword')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>

<body>
	
<section id="list-keyword" class="first">

	<table class="table table-bordered">
		<thead>
			<tr>
			
				<g:sortableColumn property="value" title="${message(code: 'keyword.value.label', default: 'Value')}" />
			
				<g:sortableColumn property="dateCreated" title="${message(code: 'keyword.dateCreated.label', default: 'Date Created')}" />
			
				<g:sortableColumn property="lastUpdated" title="${message(code: 'keyword.lastUpdated.label', default: 'Last Updated')}" />
			
			</tr>
		</thead>
		<tbody>
		<g:each in="${keywordInstanceList}" status="i" var="keywordInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
			
				<td><g:link action="show" id="${keywordInstance.id}">${fieldValue(bean: keywordInstance, field: "value")}</g:link></td>
			
				<td><g:formatDate date="${keywordInstance.dateCreated}" /></td>
			
				<td><g:formatDate date="${keywordInstance.lastUpdated}" /></td>
			
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${keywordInstanceTotal}" />
	</div>
</section>

</body>

</html>
