<%@ page import="degreebuilder.Course" %>
<div id="dialog" class="web_dialog" style="width:725px; height:525px;">
	<div id="courseSearchHead" >
		<input type="button" onClick="HideDialog();" value="X" style="float:right; width:30px;"/>
		<span><strong>Search Courses</strong></span>
	</div>
	<g:hiddenField name="currentCourseBlockId" value="${courseBlockInstance?.id }"/>
	<div id="courseSearchBody" style="padding:0.5em; ">

	  <div class="coursediv" id="123"style="margin:0.5em 0em;">
	  	<table class="courseSearchTable" id="courseSearchTable" >
			<thead>
				<tr>
					<th style="border-top-left-radius:6px;"><input type="text" name="courseSubject" placeholder ="Search" class="search_init" style="width:75px;" /></th>
					<th><input type="text" name="courseNumber" placeholder ="Search" class="search_init" style="width:75px;"/></th>
					<th ><input type="text" name="courseTitle" placeholder="Search" class="search_init" style="width:275px;"/></th>
					<th style="border-top-right-radius:6px;" colspan="3"><div style="width:220px;"></div></th>
				</tr>
				<tr id="courses" style="border-bottom:1px solid lightgrey;">
					<th id="courses">Subject Code</th>
					<th id="courses">Course Number</th>
					<th id="courses">Title</th>
					<th id="courses">Credit Hour Low</th>
					<th id="courses"></th>
					<th id="courses">Credit Hour High</th>
				</tr>
			</thead>
			<tbody>
				<g:each in="${bannerCourses}" var="course">
					<tr id="courses">
						<td style="width:15%;" class="courseSubject" id="">${course.SCBCRSE_SUBJ_CODE}</td>
						<td style="width:15%;" class="courseNumber" id="">${course.SCBCRSE_CRSE_NUMB}</td>
						<td style="width:44%;" class="courseTitle" id="">${course.SCBCRSE_TITLE}</td>
						<td style="width:10%;" class="courseCreditLow" id="">${course.SCBCRSE_CREDIT_HR_LOW}</td>
						<td style="width:6%;" class="courseCreditInd" id="">${course.SCBCRSE_CREDIT_HR_IND}</td>
						<td style="width:10%;" class="courseCreditHigh" id="">${course.SCBCRSE_CREDIT_HR_HIGH}</td>
					</tr>
				</g:each>
			</tbody>
		</table>
	</div>
	
	<strong>Semester course is typically:</strong>
	<div class="courseSemesters">
		<div style="width:21%; float:left;">
			<label><input type=checkbox name="Fall" value="F">Fall</label><br>
			<label><input type=checkbox name="Spring" value="S">Spring</label>
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox name="Fall-Odd" value="FO">Fall-Odd</label><br>
			<label><input type=checkbox name="Spring-Odd" value="SO">Spring-Odd</label>
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox name="Fall-Even" value="FE">Fall-Even</label><br>
			<label><input type=checkbox name="Spring-Even" value="SE">Spring-Even</label>
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox name="Summer" value="SU">Summer</label><br>
			<label><input type=checkbox name="Intermittently" value="I">Intermittently</label>
		</div>
		<div style="width:21%; float:left;">
	    	<input class="btn btn-primary addCourse" type="button" value="Add Course"/>
        </div>
	</div>
  </div>
</div>
