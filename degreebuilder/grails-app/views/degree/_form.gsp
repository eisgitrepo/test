<%@ page import="degreebuilder.Degree" %>



			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'college', 'error')} ">
				<label for="college" class="control-label"><g:message code="degree.college.label" default="College" /></label>
				<div class="controls">
					<g:textField name="college" maxlength="30" value="${degreeInstance?.college}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'college', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')} ">
				<label for="degreeSubject" class="control-label"><g:message code="degree.degreeSubject.label" default="Subject" /></label>
				<div class="controls">
					<g:textField name="degreeSubject" maxlength="30" value="${degreeInstance?.degreeSubject}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')} ">
				<label for="catalogYear" class="control-label"><g:message code="degree.catalogYear.label" default="Catalog Year" /></label>
				<div class="controls">
					<g:textField name="catalogYear" maxlength="9" value="${degreeInstance?.catalogYear}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'level', 'error')} ">
				<label for="level" class="control-label"><g:message code="degree.level.label" default="Level" /></label>
				<div class="controls">
					<g:textField name="level" maxlength="15" value="${degreeInstance?.level}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'level', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'type', 'error')} ">
				<label for="type" class="control-label"><g:message code="degree.type.label" default="Type" /></label>
				<div class="controls">
					<g:textField name="type" maxlength="50" value="${degreeInstance?.type}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'type', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')} ">
				<label for="totalCredits" class="control-label"><g:message code="degree.totalCredits.label" default="Total Credits" /></label>
				<div class="controls">
					<g:field type="number" name="totalCredits" value="${degreeInstance.totalCredits}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')} ">
				<label for="cumulativeGPArequired" class="control-label"><g:message code="degree.cumulativeGPArequired.label" default="Cumulative GPA" /></label>
				<div class="controls">
					<g:field type="number" name="cumulativeGPArequired" step="any" value="${degreeInstance.cumulativeGPArequired}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')} ">
				<label for="degreeApproved" class="control-label"><g:message code="degree.degreeApproved.label" default="Degree Approved" /></label>
				<div class="controls">
					<g:field checkBox name="degreeApproved"  value="${degreeInstance.degreeApproved}" onLabel="Yes" offLabel="No"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')}</span>
				</div>
			</div>

<ul class="one-to-many">
<g:each in="${degreeInstance?.degreeBlocks?}" var="d">
    <li><g:link controller="degreeBlock" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
</g:each>
<li class="add">
<g:link controller="degreeBlock" action="create" params="['degree.id': degreeInstance?.id]">${message(code: 'default.add.label', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock')])}</g:link>
</li>
</ul>

					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeBlocks', 'error')}</span>
				</div>
			</div>

