<%@ page import="degreebuilder.Degree"%>

<!doctype html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<%--<script src="//code.jquery.com/jquery-1.9.1.js"></script>--%>
<script src="//code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<%--<script	src="//lightswitch05.github.io/table-to-json/javascripts/jquery.tabletojson.min.js"></script>--%>
<r:require modules="paletteScript"/>
<%--<g:javascript src="list.min.js" />--%>
<g:javascript src="tabSlideOut.js"/>
<%--<g:javascript src="Scroller.min.js" />--%>
<meta name="layout" content="kickstart" />

<g:set var="entityName"
	value="${message(code: 'degree.label', default: 'Degree')}" />
<title><g:message code="default.create.label"
		args="[entityName]" /></title>
<link rel="stylesheet" href="${resource(dir: 'css', file: 'palette.css')}" type="text/css">
<link rel="stylesheet" href="https://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css">
<style type="text/css" media="screen">
.slide-out-div {
          padding: 20px;
          width: 400px;
          background: #ccc;
          border: 1px solid #29216d;
          position:fixed;
      } 
</style>
<script type="text/javascript">
<%--Need to initialize subject and option selects when the page loads	--%>
$(document).ready(function() {

	var college = $("#college").val();
	var subject = "${degreeInstance?.degreeSubject}";
	var concentration = "${degreeInstance?.degreeConcentration}";
	if(college.length > 0){
	$.ajax({
			type:'POST',
			data:'college='+ escape(college), 
			url:'/degreebuilder/degree/updateMajors',
			success:function(data){
				updateMajor(data);
				$("#degreeSubject").val(subject);
				updateConcentrationEdit(concentration);
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
	});
		}
    var calledFrom = "${calledFrom}";
    if (calledFrom !== "create" && calledFrom !== 'edit'){
		$("#update").attr("disabled",true);

		$('.dataInput').on('change', function(){
			//alert("change handler");
			if (
				$("#catalogYear").val() != "${degreeInstance?.catalogYear}" ||
				$("#college").val() != "${degreeInstance?.college}" ||
				$("#degreeType").val() != "${degreeInstance?.degreeType}" ||
				$("#degreeSubject").val() != "${degreeInstance?.degreeSubject}" ||
				$("#level").val() != "${degreeInstance?.level}" ||
				$("#degreeConcentration").val() != "${degreeInstance?.degreeConcentration}" ||
				$("#degreeTrack").val() != "${degreeInstance?.degreeTrack}" ||
				$("#totalCredits").val() != "${degreeInstance?.totalCredits}" ||
				$("#cumulativeGPArequired").val() != "${degreeInstance?.cumulativeGPArequired}" 
			) {
				$("#update").removeAttr("disabled");
			}			
		});
    }

    disableSave();   
});
</script>
</head>
<body>
<script type="text/javascript">
  
	function createDegreeBlock(){
		var degree = ${degreeInstance.id}; 
		var $element = $('#elements ul li.selectedAdd');
		$element.removeClass('selectedAdd');
		$.ajax({
			type:'POST',
			data:'degree='+ escape(degree) +'&blockType=' + escape(blockType) + '&displaySeqno=' + escape(displaySeqno), 
			url:'/degreebuilder/degree/createDegreeBlock',
			success:function(data,textStatus){
				$('#right-side').append(data);
				},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
		});
	}

	$(document).on('click',".addCategory",function(e){
		var degreeBlockId = $(this).parent().parent().find('input.degreeBlockId').val();
		
		var courseBlock = $(this).siblings('.courseBlocks');
		$.ajax({
			type:'POST',
			data:'degreeBlockId='+ escape(degreeBlockId), 
			url:'/degreebuilder/degree/createCourseBlock',
			success:function(data){
				$(courseBlock).append(data);
				updateSubcategory();
				},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
		});
	});
	$(document).on('click',".deleteCourseBlock",function(e){
		var courseBlockId = $(this).siblings('input.courseBlockId').val();
		var x;
		var r=confirm("Would you like to delete this subcategory?");
		if (r===true){
			$.ajax({
				type:'POST',
				data:'courseBlockId='+ escape(courseBlockId), 
				url:'/degreebuilder/degree/deleteCourseBlock',
				success:function(data){
					},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
			$(this).parent().remove();
		}
		else{}
	});
	
	$(document).on('click',".saveCourseBlock",function(e){
		var courseBlockId = $(this).siblings('input.courseBlockId').val();
		var categoryName = $(this).siblings('div').find('input#categoryName').val();
		var rule = $(this).siblings('div').find('input#rule').val();
		var credits = $(this).siblings('div').find('input#credits').val();
		var courseBlockEval = $(this).siblings('div').find('select#eval').val();
		var courseBlockComment = $(this).siblings('div').find('textarea#comment').val();
		var degreeBlockComment = $(this).parent().parent().siblings('div').find('textarea#degreeBlockComment').val();
   
		$.ajax({
			type:'POST',
			data:'courseBlockId='+ escape(courseBlockId)+ '&categoryName='+escape(categoryName)+'&rule='+escape(rule)+'&credits='+escape(credits)+'&eval='+escape(courseBlockEval)+'&courseBlockComment='+escape(courseBlockComment)+'&degreeBlockComment='+escape(degreeBlockComment), 
			url:'/degreebuilder/degree/updateCourseBlock',
			success:function(data,textStatus){
				$(this).parent().parent().minimize();
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
		});
	});
	
	$(document).on('click',".deleteCourse",function(e){
		currentClassTable = $(this).parent().parent().parent().parent();
		var x;
		var r=confirm("Would you like to delete this course?");
		if (r===true){
		    var courseId = $(this).parent().parent().find('.courseId').html();
		    $.ajax({
				type:'POST',
				data:'courseId='+ escape(courseId),
				url:'/degreebuilder/degree/deleteCourse',
				success:function(data){
					$(currentClassTable).html(data);
					},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
		}
	});

	$(document).on('click',".handle",function(e){
	    $.ajax({
			type:'POST',
			url:'/degreebuilder/degree/displayDegreeBlockTypes',
			success:function(data){
				$('#elements').html(data);
				},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
		});
	});
	
$(function() {
    $( "#accordion" ).accordion({
		collapsible:true,
		heightStyle:"content"
     });
    
  });
$(function(){
    $('.slide-out-div').tabSlideOut({
        tabHandle: '.handle',                     //class of the element that will become your tab
        pathToTabImage: '${resource(dir: 'images', file: 'element_tab.gif')}', //path to the image for the tab //Optionally can be set using css
        imageHeight: '122px',                     //height of tab image           //Optionally can be set using css
        imageWidth: '40px',                       //width of tab image            //Optionally can be set using css
        tabLocation: 'left',                      //side of screen where tab lives, top, right, bottom, or left
        speed: 300,                               //speed of animation
        action: 'click',                          //options: 'click' or 'hover', action to trigger animation
        topPos: '140px',                          //position from the top/ use if tabLocation is left or right
        leftPos: '20px',                          //position from left/ use if tabLocation is bottom or top
        fixedPosition: true                      //options: true makes it stick(fixed position) on scroll
    });

});

 function disableSave(){ 
	if ($("#degreeApprovedCB").is(':checked')){
		 //alert("degree approved - disable save");
    	 $("#update").attr("disabled",true); 
    	 $(".slide-out-div").hide(); 
    	 $(".removeElement").hide(); 
    	 $(".btnShowModal").hide(); 
    	 $(".deleteCourse").hide(); 
    	 $(".deleteCourseBlock").hide(); 
         $(".addSubcategory").hide(); 
         $(".addCategory").hide(); 
    	 
    	 
 	}   
	else {
		//alert("degree unapproved - enable save");
		$("#update").removeAttr("disabled");
	   	$(".slide-out-div").show(); 
   	    $(".removeElement").show();
     	$(".btnShowModal").show(); 
   		$(".deleteCourse").show(); 
      	$(".deleteCourseBlock").show(); 
      	$(".addSubcategory").show(); 
        $(".addCategory").show(); 
       
    	   
 	}
 };

$(document).on('click',"#degreeApprovedCB",function(e){
	var degreeId = $('#degreeId').val();
    $.ajax({
		type:'POST',
		data:'degreeId='+ escape(degreeId),

		// Calls the degree controller method approveDegree
		url:'/degreebuilder/degree/approveDegree',
		success:function(data){
			alert(data);
			disableSave();
		},
		error:function(XMLHttpRequest,textStatus,errorThrown){}
	});
});

</script>
	<g:set var="catalogYear" value="${degreeInstance?.catalogYear}"/>
	<g:set var="college" value="${degreeInstance?.college}" />
	<g:set var="degreeTypeSubject" value="${degreeInstance?.degreeType} in ${degreeInstance?.degreeSubject}"/>
	<g:set var="degreeLevel" value="${degreeInstance?.level}"/>
	<section id="create-degree" class="first">
		<g:hasErrors bean="${degreeInstance}">
			<div class=" alert-error">
				<g:renderErrors bean="${degreeInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form action="update" class="form-horizontal">
			<fieldset class="form">
				<g:hiddenField id="degreeId" name="degree" value="${degreeInstance?.id}" />
				<div id="palette-left" style="float:left; width:48%;">
					<div class="${hasErrors(bean: degreeInstance, field: 'college', 'error')} ">
						<label for="college" class="control-label"><g:message code="degree.college.label" default="School/College" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="college" class="dataInput" required="" from="${bannerColleges}" optionKey="stvcoll_desc" optionValue="stvcoll_desc" noSelection="${['':'-Choose College-']}" value="${degreeInstance?.college}"
								onChange="${remoteFunction(controller: 'degree',action: 'updateMajors',params: '\'college=\' + escape(this.value)',onSuccess: 'updateMajor(data)')}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'college', 'error')}</span>
						</div>
					</div>
						
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')} ">
						<label for="degreeType" class="control-label"><g:message code="degree.type.label" default="Degree Type" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeType" class="dataInput" value="${degreeInstance?.degreeType }" optionKey="desc" optionValue="desc"  from="${degreeType.findAll({sort:'desc'})}" noSelection="${['':'-Choose Type-']}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')}</span>
						</div>
					</div>
					
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')} ">
						<label for="degreeSubject" class="control-label"><g:message code="degree.degreeSubject.label" default="Degree Subject" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeSubject" class="dataInput" required="" from="${bannerMajors}"  noSelection="${['':'-Choose Subject-']}" value="${degreeInstance?.degreeSubject }"
								onChange="${remoteFunction(controller: 'degree',action: 'updateConcentrations',params: '\'major=\' + escape(this.value)',onSuccess: 'updateConcentration(data)')}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')}</span>
						</div>
					</div>
					
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeTrack', 'error')}">
						<label for="degreeTrack" class="control-label"><g:message code="degree.degreeTrack.label" default="Degree Track" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:field type="text" name="degreeTrack" class="dataInput" style="width: 206px; height:24px;" value="${degreeInstance.degreeTrack}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeTrack', 'error')}</span>
						</div>
					</div>
					
					<div class="${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')} required">
						<label for="totalCredits" class="control-label"><g:message code="degree.totalCredits.label" default="Total Degree Credits" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:field type="text" name="totalCredits" class="dataInput" required=""style="width: 206px; height:24px;" value="${degreeInstance.totalCredits}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')}</span>
						</div>
					</div>					
				</div>
				
				<div id="palette-right" style="float:right;width:48%;">
					<div class="${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')} required">
						<label for="catalogYear" class="control-label"><g:message code="degree.catalogYear.label" default="Catalog Year" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="catalogYear" class="dataInput" required="" from="${['2014-2015','2015-2016','2016-2017','2017-2018','2018-2019','2019-2020','2020-2021','2021-2022']}" noSelection="${['':'-Choose Year-']}" value="${degreeInstance?.catalogYear }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'level', 'error')} ">
						<label for="subtype" class="control-label"><g:message code="degree.level.label" default="Degree Level" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="level" class="dataInput" required="" from="${['Major','Minor','Certificate','Teaching Licensure']}" noSelection="${['':'-Choose Level-']}" value="${degreeInstance?.level }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'level', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')} required">
						<label for="degreeConcentration" class="control-label"><g:message code="degree.degreeConcentration.label" default="Degree Option" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeConcentration" class="dataInput" from="${bannerConcentrations}" noSelection="${['':'']}" value="${degreeInstance?.degreeConcentration }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')} ">
						<label for="cumulativeGPArequired" class="control-label"><g:message code="degree.cumulativeGPArequired.label" default="Cumulative GPA:" /></label>
						<div class="controls">
							<g:select name="cumulativeGPArequired" required="" class="dataInput" from="${['4.0','3.95','3.9','3.85','3.8','3.75','3.7','3.65','3.6','3.55','3.5','3.45','3.4','3.35','3.3','3.25','3.2','3.15','3.1','3.05','3.0','2.95','2.9','2.85','2.8','2.75','2.7','2.65','2.6','2.55','2.5','2.45','2.4','2.35','2.3','2.25','2.2','2.15','2.1','2.05','2.0','1.95','1.9','1.85','1.8','1.75','1.7']}" noSelection="['':'']" value="${degreeInstance?.cumulativeGPArequired}" />
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')}</span>
						</div>
					</div>
					<g:if test="${session.isAdminUser == 'true'}">				
						<div class="${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')} ">
							<label for="degreeApproved" class="control-label"><g:message code="degree.degreeApproved.label" default="Degree Approved:" /></label>
							<div class="controls">
								<g:checkBox id="degreeApprovedCB" name="degreeApprovedCB" checked="${degreeInstance?.degreeApproved == '1' }" value="1"/>	
								<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')}</span>
							</div>
						</div>
					</g:if>	
				</div>
				<div style="clear:both;"></div>	
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')} required">
				<label for="degreeComment" class="control-label"><g:message code="degree.degreeComment.label" default="Degree Comment" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:textArea name="degreeComment" class="dataInput" value="${degreeInstance?.degreeComment.decodeHTML().replaceAll('<br>','\n')}" rows="2" style="width:88%;"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')}</span>
				</div>
			</div>
				<div id="bottomSection">
					<div class="row">
<%--						<div class="span4" style="outline: 1px solid transparent;">--%>
<%--							<h4>Element Options</h4>--%>
<%--							<g:render template="elements" />--%>
<%--						</div>--%>
<%--						<div class="span1"--%>
<%--							style="margin-top: 30%; width: 54px; margin-left: 0px;">--%>
<%--							<ul>--%>
<%--								<li><input id="add" onclick="createDegreeBlock()"--%>
<%--									type="button" value="->" /></li>--%>
<%--							</ul>--%>
<%--						</div>--%>
						<div class="span12" style="outline: 1px solid transparent;">
							<h4>Degree Template</h4>
							<%--						<div class="boxed ui-widget-content" id="droppable"></div>--%>
							<div class="limit"><div class="boxed" id="right-side">
<%--								<g:each in="${degreebuilderService.sortDegreeBlocks(degreeInstance.degreeBlocks)}" var="degreeBlockInstance">--%>
<%--    							   <g:render template="degreeBlock" model="[degreeBlockInstance:degreeBlockInstance]"/> --%>
<%--								</g:each>--%>
								<g:each in="${degreeInstance?.degreeBlocks?.sort()}" var="degreeBlockInstance">
    							   <g:render template="degreeBlock" model="[degreeBlockInstance:degreeBlockInstance]"/> 
								</g:each>
								<div style='clear:both;'></div>
							</div>	
							<ul></ul>
							</div>
						</div>
					</div>
				</div>
			</fieldset>
			<div class="form-actions" >
				<g:submitButton name="update" class="btn btn-primary" value="${message(code: 'default.button.save.label', default: 'Save')}" />
			    <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
			</div>
		</g:form>

		<div id="output"></div>

		<div id="overlay" class="web_dialog_overlay"></div>
		<g:render template="courseSearch" />
	    <g:render template="courseEdit" />
	</section>
	<div class="slide-out-div">
         <a class="handle" href="">Elements</a>
         <div class="">
			<div class="" style="outline: 1px solid transparent;">
				<h4>Element Options  (Double click to add)</h4>
				<g:render template="elements" />
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function updateMajor(items) {
		    var x = document.getElementById('degreeSubject');
		    var i = x.length;
		    while (i > 1) {
		        i--;
		        x.remove(i);
		    }	
		    // Rebuild the select
		    for (i=0; i < items.length; i++) {
				var option = document.createElement('option');
		        option.text = items[i];
		        try {
		                x.add(option,x.options[null]); // doesn't work in IE
		        }
		        catch(ex) {
		                x.add(option); // IE only
		        }
		    }
		}
	    function updateConcentration(items) {
		    var x = document.getElementById('degreeConcentration');
		    var i = x.length;
		    while (i > 1) {
		        i--;
		        x.remove(i);
		    }
		
		    // Rebuild the select
		    for (i=0; i < items.length; i++) {
		    	var option = document.createElement('option');
		        option.text = items[i];
		        try {
		                x.add(option,x.options[null]); // doesn't work in IE
		        }
		        catch(ex) {
		                x.add(option); // IE only
		        }
		    }
		}
		function updateConcentrationEdit(concentration){
			$.ajax({
				type:'POST',
				data:'major='+ escape($(degreeSubject).val()),
				url:'/degreebuilder/degree/updateConcentrations',
				success:function(data){
					updateConcentration(data);
					$("#degreeConcentration").val(concentration);
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
	}
	</script>
</body>
</html>
