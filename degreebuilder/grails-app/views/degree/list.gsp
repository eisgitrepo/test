
<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
	<style type="text/css" media="screen">
		table tbody{
			cursor:pointer;
		}
		table tbody tr:hover {
			background-color:lightblue;
		}
	</style>	
	<%--<g:javascript src="datatables.js" />--%>

</head>

<body>		
<section id="list-degree" class="first">
<div style="overflow=auto">
	<table class="table table-bordered" id="degreeSearchTable">
	<thead>
		<tr>
			<th><input type="text" name="id" placeholder="S" class="search_init" style="width: 10px"/></th>
			<th><input type="text" name="AI" placeholder ="S" class="search_init" style="width: 7px"/></th>
			<th><input type="text" name="updateby" placeholder ="Search" class="search_init" style="width: 43px"/></th>
			<th><input type="text" name="catalogYear" placeholder ="Search" class="search_init" style="width: 45px"/></th>
			<th><input type="text" name="college" placeholder="Search" class="search_init" style="width: 170px"/></th>
			<th><input type="text" name="degreeType_version" placeholder="Search" class="search_init" style="width: 170px"/></th>
			<th><input type="text" name="level" placeholder="search_init" style="width: 38px"/></th>
			<th><input type="text" name="degreeSubject" placeholder="Search" class="search_init" style="width: 170px"/></th>
			<th><input type="text" name="degreeConcentration" placeholder="Search" class="search_init" style="width: 171px"/></th>
			<th><input type="text" name="degreeTrack" placeholder="Search" class="search_init" style="width: 40px"/></th>
			<th><input type="text" name="totalCredits" placeholder="S" class="search_init" style="width: 10px"/></th>
		</tr>	
		<tr>
<%--			<g:sortableColumn property="id" title="${message(code: 'degree.id.label', default: 'Id')}" />--%>
<%--			<g:sortableColumn property="degreeApproved" title="${message(code: 'degree.degreeApproved.label', default: 'AI')}" />--%>
<%--			<g:sortableColumn property="updatedBy" title="${message(code: 'degree.updatedBy.label', default: 'UpdatedBy')}" />--%>
<%--			<g:sortableColumn property="catalogYear" title="${message(code: 'degree.catalogYear.label', default: 'Year')}" />--%>
<%--			<g:sortableColumn property="college" title="${message(code: 'degree.college.label', default: 'School/College')}" />--%>
<%--			<g:sortableColumn property="degreeType" title="${message(code: 'degree.type.label', default: 'Type')}" />--%>
<%--			<g:sortableColumn property="level" title="${message(code: 'degree.level.label', default: 'Level')}" />--%>
<%--			<g:sortableColumn property="degreeSubject" title="${message(code: 'degree.degreeSubject.label', default: 'Subject')}" />--%>
<%--			<g:sortableColumn property="degreeConcentration" title="${message(code: 'degree.degreeConcentration.label', default: 'Option/Concentration')}" />--%>
<%--			<g:sortableColumn property="degreeTrack" title="${message(code: 'degree.degreeTrack.label', default: 'Track')}" />--%>
<%--			<g:sortableColumn property="totalCredits" title="${message(code: 'degree.totalCredits.label', default: 'CR')}" />--%>
			
			<th>ID</th>
			<th>AI</th>
			<th>UpdatedBy</th>
			<th>Year</th>
			<th>School/College</th>
			<th>Type</th>
			<th>Level</th>
			<th>Subject</th>
			<th>Option/Concentration</th>
			<th>Track</th>
			<th>CR</th>

		</tr>
	</thead>
		<tbody>	
			<g:each in="${degreeInstanceList}" status="i" var="degreeInstance">

			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}" onclick='window.location = "${createLink(action: "show", id: degreeInstance.id)}"'>
	     		<td>${degreeInstance.id}</td>
	           <g:if test= "${degreeInstance.degreeApproved == '1'}">	
	                  <td>Y</td>     		
				</g:if>
				<g:else>
                    <td></td>
                </g:else>     

<%--	     		<td>${fieldValue(bean: degreeInstance, field: "degreeApproved")}</td>--%>
				<td>${fieldValue(bean: degreeInstance, field: "updatedBy")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "catalogYear")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "college")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "degreeType")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "level")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "degreeSubject")}</td>				
			    <td>${fieldValue(bean: degreeInstance, field: "degreeConcentration")}</td>
			    <td>${fieldValue(bean: degreeInstance, field: "degreeTrack")}</td>
				<td>${fieldValue(bean: degreeInstance, field: "totalCredits")}</td>
			</tr>
			</g:each>

		</tbody>
	</table>
</div>
<script type="text/javascript">
$(document).ready(function() {
	var oTable = $('#degreeSearchTable').dataTable( {
        "bScrollInfinite": true,
        "bScrollCollapes": true,
        "sScrollY": "350px"
	
<%--		"oLanguage": {--%>
<%--			"sSearch": "Search all columns:"--%>
<%--		}--%>
	} );
	
	$("thead input").keyup( function () {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter( this.value, $("thead input").index(this) );
	} );
	
		
	/*
	 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
	 * the footer
	 */
	$("thead input").each( function (i) {
		asInitVals[i] = this.value;
	} );
	
	$("thead input").focus( function () {
		if ( this.className == "search_init" )
		{
			this.className = "";
			this.value = "";
		}
	} );
	
	$("thead input").blur( function (i) {
		if ( this.value == "" )
		{
			this.className = "search_init";
			this.value = asInitVals[$("thead input").index(this)];
		}
	} );
} );
</script>

</section>
</body>
</html>
