
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')} required">
				<label for="catalogYear" class="control-label"><g:message code="degree.catalogYear.label" default="Catalog Year" /><span class="required-indicator">:</span></label>
				<div class="controls">
<%--					<g:textField name="catalogYear" value="${degreeInstance?.catalogYear}"/>--%>
					<g:select name="catalogYear" from="${['2013-2014','2014-2015','2015-2016','2016-2017','2017-2018','2018-2019','2019-2020','2020-2021']}" noSelection="${['':'-Choose Year-']}" value="${degreeInstance?.catalogYear }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'college', 'error')} ">
				<label for="college" class="control-label"><g:message code="degree.college.label" default="School/College" /><span class="required-indicator">*</span></label>
				<div class="controls">
<%--					<g:textField name="college" value="${degreeInstance?.college}"/>--%>
					<g:select name="college" from="${bannerColleges}" noSelection="${['':'-Choose College-']}" value="${degreeInstance?.college }"
						onChange="${remoteFunction(controller: 'degree',action: 'updateMajors',params: '\'college=\' + escape(this.value)',onSuccess: 'updateMajor(data)')}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'college', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'type', 'error')} ">
				<label for="type" class="control-label"><g:message code="degree.type.label" default="Degree Type" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select name="type" value="${degreeInstance?.type }" from="${degreeType.list()}" noSelection="${['':'-Choose Degree Type-']}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'type', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')} ">
				<label for="degreeSubject" class="control-label"><g:message code="degree.degreeSubject.label" default="Subject" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select name="degreeSubject" from="${bannerMajors}" noSelection="${['':'-Choose Subject-']}" value="${degreeInstance?.degreeSubject }"
						onChange="${remoteFunction(controller: 'degree',action: 'updateConcentrations',params: '\'major=\' + escape(this.value)',onSuccess: 'updateConcentration(data)')}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'level', 'error')} ">
				<label for="subtype" class="control-label"><g:message code="degree.level.label" default="Degree Level" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select name="level" from="${['Major','Minor','Concentration','Certificate']}" noSelection="${['':'-Choose level-']}" value="${degreeInstance?.level }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'level', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')} required">
				<label for="degreeConcentration" class="control-label"><g:message code="degree.degreeConcentration.label" default="Option/Concentration" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select name="degreeConcentration" from="${bannerConcentrations}" noSelection="${['':'']}" value="${degreeInstance?.degreeConcentration }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')} required">
				<label for="totalCredits" class="control-label"><g:message code="degree.totalCredits.label" default="Total Credits" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number decimal" name="totalCredits" required="" value="${degreeInstance.totalCredits}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')} required">
				<label for="cumulativeGPArequired" class="control-label"><g:message code="degree.cumulativeGPArequired.label" default="Cumulative GPA" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number decimal" name="cumulativeGPArequired" required="" value="${degreeInstance.cumulativeGPArequired}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')}</span>
				</div>
			</div>
		

