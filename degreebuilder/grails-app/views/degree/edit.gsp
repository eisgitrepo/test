<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<script type="text/javascript">
<%--Need to initialize subject and option selects when the page loads	--%>
$(document).ready(function() {
	var college = $("#college").val();
	var subject = "${degreeInstance?.degreeSubject}";
	var concentration = "${degreeInstance?.degreeConcentration}"
	$.ajax({
			type:'POST',
			data:'college='+ escape(college), 
			url:'/degreebuilder/degree/updateMajors',
			success:function(data){
				updateMajor(data);
				$("#degreeSubject").val(subject);
				updateConcentrationEdit(concentration);
			},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
	});
	$("#update").attr("disabled",true);

	$('.dataInput').on('change', function(){
		//alert("change handler");
		if (
			$("#catalogYear").val() != "${degreeInstance?.catalogYear}" ||
			$("#college").val() != "${degreeInstance?.college}" ||
			$("#degreeType").val() != "${degreeInstance?.degreeType}" ||
			$("#degreeSubject").val() != "${degreeInstance?.degreeSubject}" ||
			$("#level").val() != "${degreeInstance?.level}" ||
			$("#degreeConcentration").val() != "${degreeInstance?.degreeConcentration}" ||
			$("#totalCredits").val() != "${degreeInstance?.totalCredits}" ||
			$("#cumulativeGPArequired").val() != "${degreeInstance?.cumulativeGPArequired}" ||
			$("#degreeApproved").val() != "${degreeInstance?.degreeApproved}" ||
			$("#degreeComment").val() != "${degreeInstance?.degreeComment}"
		) {
			$("#update").removeAttr("disabled");
		};				
	});
});
</script>
	<g:set var="catalogYear" value="${degreeInstance?.catalogYear}"/>
	<g:set var="college" value="${degreeInstance?.college}" />
	<g:set var="degreeTypeSubject" value="${degreeInstance?.degreeType} in ${degreeInstance?.degreeSubject}"/>
	<g:set var="degreeLevel" value="${degreeInstance?.level}"/>
	<section id="edit-degree" class="first">
		<g:hasErrors bean="${degreeInstance}">
			<div class=" alert-error">
				<g:renderErrors bean="${degreeInstance}" as="list" />
			</div>
		</g:hasErrors>
		<g:form action="update" class="form-horizontal">
			<fieldset class="form">
				<g:hiddenField name="degree" value="${degreeInstance?.id}" />
				<div id="palette-left" style="float:left; width:48%;">
					<div class="${hasErrors(bean: degreeInstance, field: 'college', 'error')} ">
						<label for="college" class="control-label"><g:message code="degree.college.label" default="School/College" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="college" class="dataInput" from="${bannerColleges}" optionKey="stvcoll_desc" optionValue="stvcoll_desc" noSelection="${['':'-Choose College-']}" value="${degreeInstance?.college}"
								onChange="${remoteFunction(controller: 'degree',action: 'updateMajors',params: '\'college=\' + escape(this.value)',onSuccess: 'updateMajor(data)')}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'college', 'error')}</span>
						</div>
					</div>
						
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')} ">
						<label for="degreeType" class="control-label"><g:message code="degree.type.label" default="Degree Type" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeType" class="dataInput" value="${degreeInstance?.degreeType }" optionKey="desc" optionValue="desc"  from="${degreeType.findAll([sort:'desc'])}" noSelection="${['':'-Choose Type-']}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')}</span>
						</div>
					</div>
					
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')} ">
						<label for="degreeSubject" class="control-label"><g:message code="degree.degreeSubject.label" default="Degree Subject" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeSubject" class="dataInput" from="${bannerMajors}"  noSelection="${['':'-Choose Subject-']}" value="${degreeInstance?.degreeSubject }"
								onChange="${remoteFunction(controller: 'degree',action: 'updateConcentrations',params: '\'major=\' + escape(this.value)',onSuccess: 'updateConcentration(data)')}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')}</span>
						</div>
					</div>
					
					<div class="${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')} required">
						<label for="totalCredits" class="control-label"><g:message code="degree.totalCredits.label" default="Total Degree Credits" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:field type="number decimal" name="totalCredits" class="dataInput" required="" style="width: 214px; height:24px;"	 value="${degreeInstance.totalCredits}"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')}</span>
						</div>
					</div>
					
				</div>
				
				<div id="palette-right" style="float:right;width:48%;">
					<div class="${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')} required">
						<label for="catalogYear" class="control-label"><g:message code="degree.catalogYear.label" default="Catalog Year" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="catalogYear" class="dataInput" from="${['2013-2014','2014-2015','2015-2016','2016-2017','2017-2018','2018-2019','2019-2020','2020-2021']}" noSelection="${['':'-Choose Year-']}" value="${degreeInstance?.catalogYear }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'level', 'error')} ">
						<label for="subtype" class="control-label"><g:message code="degree.level.label" default="Degree Level" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="level" class="dataInput" from="${['Major','Minor','Certificate']}" noSelection="${['':'-Choose Level-']}" value="${degreeInstance?.level }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'level', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')} required">
						<label for="degreeConcentration" class="control-label"><g:message code="degree.degreeConcentration.label" default="Degree Option" /><span class="required-indicator">:</span></label>
						<div class="controls">
							<g:select name="degreeConcentration" class="dataInput" from="${bannerConcentrations}" noSelection="${['':'']}" value="${degreeInstance?.degreeConcentration }"/>
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')} ">
						<label for="cumulativeGPArequired" class="control-label"><g:message code="degree.cumulativeGPArequired.label" default="Cumulative GPA Required:" /></label>
						<div class="controls">
							<g:select name="cumulativeGPArequired" class="dataInput" required="" from="${['4.0','3.95','3.9','3.85','3.8','3.75','3.7','3.65','3.6','3.55','3.5','3.45','3.4','3.35','3.3','3.25','3.2','3.15','3.1','3.05','3.0','2.95','2.9','2.85','2.8','2.75','2.7','2.65','2.6','2.55','2.5','2.45','2.4','2.35','2.3','2.25','2.2','2.15','2.1','2.05','2.0']}" noSelection="['':'']" value="${degreeInstance?.cumulativeGPArequired}" />
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')}</span>
						</div>
					</div>
					<div class="${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')} ">
						<label for="degreeApproved" class="control-label"><g:message code="degree.degreeApproved.label" default="Degree Approved:" /></label>
						<div class="controls">
							<g:select name="degreeApproved" required="" class="dataInput" from="${['Y','N']}" noSelection="['':'']" value="${degreeInstance?.degreeApproved}" />
							<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeApproved', 'error')}</span>
						</div>
					</div>
				</div>
				<div style="clear:both;"></div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')} required">
				<label for="degreeComment" class="control-label"><g:message code="degree.degreeComment.label" default="Degree Comment" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:textArea name="degreeComment" class="dataInput" value="${degreeInstance?.degreeComment}" rows="2" style="width:88%;"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')}</span>
				</div>
			</div>
			</fieldset>
			<div class="form-actions" >
				<g:submitButton name="update" class="btn btn-primary" value="${message(code: 'default.button.save.label', default: 'Save')}" />
				<a href="#DeleteModal" role="button" class="" data-toggle="modal">Delete</a>
				<g:render template="/_common/modals/deleteDialog" model="[item: item]"/>
			</div>
		</g:form>
	</section>
<script type="text/javascript">
	function updateMajor(items) {
	    var x = document.getElementById('degreeSubject');
	    var i = x.length;
	    while (i > 1) {
	        i--;
	        x.remove(i);
	    }	
	    // Rebuild the select
	    for (i=0; i < items.length; i++) {
			var option = document.createElement('option');
	        option.text = items[i];
	        try {
	                x.add(option,x.options[null]); // doesn't work in IE
	        }
	        catch(ex) {
	                x.add(option); // IE only
	        }
	    }
	}
    function updateConcentration(items) {
	    var x = document.getElementById('degreeConcentration');
	    var i = x.length;
	    while (i > 1) {
	        i--;
	        x.remove(i);
	    }
	
	    // Rebuild the select
	    for (i=0; i < items.length; i++) {
	    	var option = document.createElement('option');
	        option.text = items[i];
	        try {
	                x.add(option,x.options[null]); // doesn't work in IE
	        }
	        catch(ex) {
	                x.add(option); // IE only
	        }
	    }
	}
	function updateConcentrationEdit(concentration){
			$.ajax({
				type:'POST',
				data:'major='+ escape($(degreeSubject).val()),
				url:'/degreebuilder/degree/updateConcentrations',
				success:function(data){
					updateConcentration(data);
					$("#degreeConcentration").val(concentration);
				},
				error:function(XMLHttpRequest,textStatus,errorThrown){}
			});
	}
</script>
</body>

</html>
