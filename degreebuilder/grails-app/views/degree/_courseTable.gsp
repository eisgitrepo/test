<table id="table" class="table courseTable" style="width:100%;  border:0px solid black !important; background-color:white;">
	<thead>
	</thead>
	<tbody>
		<g:each in="${courseBlockInstance?.courses?.sort()}" var="course">
			<tr>
				<td class="courseId" style="display:none;">${course.id }</td>
				<td class="courseSubject">${course.courseSubject }</td>
				<td class="courseNumber">${course.courseNumber}</td>
				<td class="title">${course.title}</td>
				<td class="courseCredits">${course.courseCredits}</td>
				<td class="term">${course.term}</td>
				<td class="courseYear">${course.courseYear}</td>			
<%--				<td><img alt="delete" src="${resource(dir: 'images', file: 'delete.png')}" class="deleteCourse" ><td>--%>
				<td><img alt="edit" src="${resource(dir: 'images', file: 'edit.png')}" class="courseEdit"><td>
             	<td><input type="button" style="color: #B80707" class="deleteCourse" value="X" title="Delete this course" /></td>
			</tr>
		</g:each>
	</tbody>
</table>