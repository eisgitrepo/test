<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'print.css')}" type="text/css" media="print">
	<style type="text/css" media="screen">
		.criteria {
			margin-right:20em;
		}
		table td {
			padding-right:7em;
		}
		.firstCell {
			padding-left:1.5em;
		}
		table th {
			text-align:left;
			text-decoration:underline;
		}
</style>
</head>
<body>
<section id="show-degree" class="first">

	${fieldValue(bean: degreeInstance, field: "college")}<br>
	${fieldValue(bean: degreeInstance, field: "level")}  in ${fieldValue(bean: degreeInstance, field: "degreeSubject")}<br>
	<g:message code="degree.catalogYear.label" default="Catalog Year" />: ${fieldValue(bean: degreeInstance, field: "catalogYear")}<br>
	<br>
	
	
	<!-- TODO Need to loop through each degreeBlock and display the relevant information -->
	
	<!-- DegreeBlock Information -->
	<g:each in="${degreeInstance.degreeBlocks}" var="degreeBlock">
		<strong>${degreeBlock?.encodeAsHTML()}</strong>
		<g:each in="${degreeBlock.courseBlocks}" var="courseBlock">
			<p>Name: ${courseBlock.name}</p>
			<p>Rule: ${courseBlock.rule}</p>
			<span class="criteria">Criteria: ${courseBlock.eval}</span><span class="credits">Number of Credits ${courseBlock.courseBlockCredits}</span><br>
			<span>Course Listing</span>
			<table id="degreeBlockCourses" >
				<g:each in="${courseBlock.courses}" var="course">
					<tr><td class="firstCell">${course.courseSubject} ${course.courseNumber}</td><td>${course.title}</td><td>${course.courseCredits}</td><td>${course.term}</td></tr>		
				</g:each>
			</table>
			<br>
		</g:each>
		<p>Commentary: ${degreeBlock.degreeBlockComment}</p>
	</g:each>
	<!-- End of DegreeBlock Information -->
	
	<strong>Degree Commentary</strong>
	<p>${degreeInstance.degreeComment}</p>
</section>
</body>
</html>