<%@ page import="degreebuilder.Course" %>
<div id="courseEdit" class="web_dialog" style="width:750px; height:260px;">
	<div id="courseSearchHead" >
		<input type="button" onClick="$('#courseEdit').fadeOut(300)" value="X" style="float:right; width:30px;"/>
		<span><strong>Edit Course</strong></span>
	</div>

	<div id="currentCourse"></div>	
<table id="currentCourse" class="table courseTable" style="width:100%;  border:0px solid black !important; background-color:white;">
	<thead>
		<tr id="courses" style="border-bottom:1px solid lightgrey;">
		  	<th id="courses">Subject Code</th>
			<th id="courses">Course Number</th>
			<th id="courses">Title</th>
			<th id="courses">Low to High</th>
		</tr>
	</thead>
	<tbody>	
		<tr>
<%--		<td id="currentCourseId"></td>--%>
			<td id="currentCourseSubject"></td>
			<td id="currentCourseNumber"></td>
			<td id="currentCourseTitle"></td>
			<td id="currentCourseCredits"></td>
		</tr>
	</tbody>
</table>
	<div class="currentCourseTerm">
		<div style="width:21%; float:left;">
			<label><input type=checkbox id="fallCB" name="Fall" value="F" >Fall</label><br>
			<label><input type=checkbox id="springCB" name="Spring" value="S" >Spring</label>
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox id="fallOddCB" name="Fall-Odd" value="FO" >Fall-Odd</label><br>
			<label><input type=checkbox id="springOddCB" name="Spring-Odd" value="SO" >Spring-Odd</label>
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox id="fallEvenCB" name="Fall-Even" value="FE" >Fall-Even</label><br>
			<label><input type=checkbox id="springEvenCB" name="Spring-Even" value="SE" >Spring-Even</label>					
		</div>
		<div style="width:21%; float:left;">
			<label><input type=checkbox id="summerCB" name="Summer" value="SU" >Summer</label><br>
			<label><input type=checkbox id="intermittentlyCB" name="Intermittently" value="I" >Intermittently</label>
		</div>
		<div style="width:21%; float:left;">
	    	<input class="btn btn-primary updateCourse" onClick="updateCourse()" type="button" value="Update Course"/>
        </div>
	</div>
</div>