<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>

<section id="copyDegree" class="first">
	<g:form action="copyDegreesByCatalogYear" class="form-horizontal" >
		<fieldset class="form">
			<div class="control-group fieldcontain ">
				<label for="oldCatalogYear" class="control-label">Old Catalog Year</label>
				<div class="controls">
					<g:textField name="oldCatalogYear" maxlength="200" value="${oldCatalogYear}"/>
					<span class="help-inline"></span>
				</div>
				<label for="newCatalogYear" class="control-label">New Catalog Year</label>
				<div class="controls">
					<g:textField name="newCatalogYear" maxlength="200" value="${newCatalogYear}"/>
					<span class="help-inline"></span>
				</div>
			</div>
		</fieldset>
		<div class="form-actions">
			<g:submitButton name="copy" class="btn btn-primary" value="Copy" />
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		</div>
	</g:form>	
</section>		
</body>
</html>
