 	<div class="degreeBlock" id="${degreeBlockInstance?.degreeBlockType?.id}" >
		<g:hiddenField class="degreeBlockId" name="degreeBlock" value="${degreeBlockInstance?.id}"/>
		<div class="elementHeader"  >
		<input class="removeElement" style="color: #B80707" onclick="elementToRemove = $(this).parent().parent(); removeElement();" type="button" value="X" style="width:23px;"/>
		<input class="minimizeElement" type="button" value="-" style="float:right; width:23px;"/>
			<strong>${degreeBlockInstance?.degreeBlockType?.name}</strong>
		</div>
		<div class="elementBody" >
			<div class="courseBlocks" id="courseBlocks">
				<g:each in="${degreeBlockInstance?.courseBlocks.sort{it.sortId}}" var="courseBlockInstance">
					<g:if test="${degreeBlockInstance.degreeBlockType.name != 'Additional Requirements'}">
						<g:render template="courseBlock" model="[courseBlockInstance:courseBlockInstance]"/>
					</g:if>
					<g:else>
						<div class="courseBlock" id="${courseBlockInstance?.id }" class="control-group fieldcontain"  style="border:1px solid black; margin:0.5em; padding:0.5em;">
							<g:hiddenField class="courseBlockId" name="courseBlockId" value="${courseBlockInstance?.id}"/>
							<div class=" ${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')} ">
								<label for="categoryName" class="category control-label"><g:message code="courseBlock.categoryName.label" default="Category Name" /></label>
								<div class="controls">
									<g:textField name="categoryName" value="${courseBlockInstance?.name }"/>
								</div>
							</div>
							<g:hiddenField name="rule" value=""/>
							<g:hiddenField name="eval" value=""/>
							<g:hiddenField name="credits" value=""/>
							<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')} ">
								<label for="comment" class="control-label"><g:message code="courseBlock.comment.label" default="Comment" /></label>
								<div class="controls">
									<g:textArea name="courseBlockComment" cols="40" rows="5" maxlength="2000" value="${courseBlockInstance?.courseBlockComment.decodeHTML().replaceAll('<br>','\n')}" style="width:88%;"/>
									<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')}</span>
								</div>
							</div>
							<g:hiddenField name="sortId" value="${courseBlockInstance?.sortId}"/>
<%--							<input type="button" style="#B80707" class="deleteCourseBlock" value="X" style="width:23px; margin-left:98%;" title="Delete this category block" />--%>
						</div>
					</g:else>
				</g:each>
			</div><!-- End of courseBlocks -->
			<g:if test="${degreeBlockInstance.degreeBlockType.name != 'Additional Requirements'}">
				<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockInstance, field: 'degreeBlockComment', 'error')}" style="padding:0.5em;">
					<label for="degreeBlockComment" class="control-label"><g:message code="degreeBlock.degreeBlockComment.label" default="Comment" /></label>
					<div class="controls">
						<g:textArea name="degreeBlockComment" rows="3" maxlength="2000" value="${degreeBlockInstance?.degreeBlockComment.decodeHTML().replaceAll('<br>','\n')}"style="width:370px;"/>
						<span class="help-inline">${hasErrors(bean: degreeBlockInstance, field: 'degreeBlockComment', 'error')}</span>
					</div>
				</div>
			</g:if>
			<g:else>
				<g:hiddenField name="degreeBlockComment" value=""/>
			</g:else>
			<g:if test="${degreeBlockInstance.degreeBlockType.name != 'Additional Requirements'}">
				<input class="addCategory" type="button" value="Add Subcategory" title="Add a subcategory block"/>
			</g:if>	
		</div><!-- End of elementBody -->
	</div> <!-- End of degreeBlock -->