<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create-degree" class="first">

	<g:hasErrors bean="${degreeInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${degreeInstance}" as="list" />
	</div>
	</g:hasErrors>
	
	<g:form action="saveDegree" class="form-horizontal" >
		<fieldset class="form">
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')} required">
				<label for="catalogYear" class="control-label"><g:message code="degree.catalogYear.label" default="Catalog Year" /><span class="required-indicator">:</span></label>
				<div class="controls">
<%--					<g:textField name="catalogYear" value="${degreeInstance?.catalogYear}"/>--%>
					<g:select name="catalogYear" from="${['2013-2014','2014-2015','2015-2016','2016-2017','2017-2018','2018-2019','2019-2020','2020-2021']}" noSelection="${['':'-Choose Year-']}" value="${degreeInstance?.catalogYear }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'catalogYear', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'college', 'error')} ">
				<label for="college" class="control-label"><g:message code="degree.college.label" default="School/College" /><span class="required-indicator">:</span></label>
				<div class="controls">
<%--					<g:textField name="college" value="${degreeInstance?.college}"/>--%>
					<g:select name="college" from="${bannerColleges}" optionKey="stvcoll_desc" optionValue="stvcoll_desc" noSelection="${['':'-Choose College-']}" value="${degreeInstance?.college}"
						onChange="${remoteFunction(controller: 'degree',action: 'updateMajors',params: '\'college=\' + escape(this.value)',onSuccess: 'updateMajor(data)')}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'college', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')} ">
				<label for="degreeType" class="control-label"><g:message code="degree.type.label" default="Degree Type" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:select name="degreeType" value="${degreeInstance?.degreeType }" optionKey="desc" optionValue="desc"  from="${degreeType.list()}" noSelection="${['':'-Choose Type-']}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeType', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')} ">
				<label for="degreeSubject" class="control-label"><g:message code="degree.degreeSubject.label" default="Degree Subject" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:select name="degreeSubject" from="${bannerMajors}"  noSelection="${['':'-Choose Subject-']}" value="${degreeInstance?.degreeSubject }"
						onChange="${remoteFunction(controller: 'degree',action: 'updateConcentrations',params: '\'major=\' + escape(this.value)',onSuccess: 'updateConcentration(data)')}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeSubject', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'level', 'error')} ">
				<label for="subtype" class="control-label"><g:message code="degree.level.label" default="Degree Level" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:select name="level" from="${['Major','Minor','Concentration','Certificate']}" noSelection="${['':'-Choose Level-']}" value="${degreeInstance?.level }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'level', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')} required">
				<label for="degreeConcentration" class="control-label"><g:message code="degree.degreeConcentration.label" default="Degree Option" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:select name="degreeConcentration" from="${bannerConcentrations}" noSelection="${['':'']}" value="${degreeInstance?.degreeConcentration }"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeConcentration', 'error')}</span>
				</div>
			</div>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')} required">
				<label for="totalCredits" class="control-label"><g:message code="degree.totalCredits.label" default="Total Degree Credits" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:field type="number decimal" name="totalCredits" required="" value="${degreeInstance.totalCredits}"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'totalCredits', 'error')}</span>
				</div>
			</div>
			<div class="eval ${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')} ">
				<label for="cumulativeGPArequired" class="control-label"><g:message code="degree.cumulativeGPArequired.label" default="cumulativeGPArequired" /></label>
				<div class="controls">
					<g:select name="cumulativeGPArequired" from="${['4.0','3.9','3.8','3.7','3.6','3.5','3.4','3.3','3.2','3.1','3.0','2.9','2.8','2.7','2.6','2.5','2.4','2.3','2.2','2.1','2.0']}" noSelection="['':'']" value="${degreeInstance?.cumulativeGPArequired}" />
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'cumulativeGPArequired', 'error')}</span>
				</div>
			</div>
		</fieldset>		
			<div class="control-group fieldcontain ${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')} required">
				<label for="degreeComment" class="control-label"><g:message code="degree.degreeComment.label" default="Degree Comment" /><span class="required-indicator">:</span></label>
				<div class="controls">
					<g:textArea name="degreeComment" value="${degreeInstance?.degreeComment}" rows="5" style="width:370px;"/>
					<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'degreeComment', 'error')}</span>
				</div>
			</div>
		<div class="form-actions">
			<g:submitButton name="create" class="btn btn-primary" value="Save/Continue" />
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		</div>
	</g:form>
</section>
<script type="text/javascript">
	function updateMajor(items) {
	    var x = document.getElementById('degreeSubject');
	    var i = x.length;
	    while (i > 1) {
	        i--;
	        x.remove(i);
	    }	
	    // Rebuild the select
	    for (i=0; i < items.length; i++) {
			var option = document.createElement('option');
	        option.text = items[i];
	        try {
	                x.add(option,x.options[null]); // doesn't work in IE
	        }
	        catch(ex) {
	                x.add(option); // IE only
	        }
	    }
	}
    function updateConcentration(items) {
	    var x = document.getElementById('degreeConcentration');
	    var i = x.length;
	    while (i > 1) {
	        i--;
	        x.remove(i);
	    }
	
	    // Rebuild the select
	    for (i=0; i < items.length; i++) {
	    	var option = document.createElement('option');
	        option.text = items[i];
	        try {
	                x.add(option,x.options[null]); // doesn't work in IE
	        }
	        catch(ex) {
	                x.add(option); // IE only
	        }
	    }
	}
</script>
</body>

</html>
