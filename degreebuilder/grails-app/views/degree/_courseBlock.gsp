<div class="courseBlock" id="${courseBlockInstance?.id }" class="control-group fieldcontain"  style="border:1px solid black; margin:0.5em; padding:0.5em;">
	<g:hiddenField class="courseBlockId" name="courseBlockId" value="${courseBlockInstance?.id}"/>
	<div class=" ${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')} ">
		<label for="categoryName" class="category control-label"><g:message code="courseBlock.categoryName.label" default="Category Name" /></label>
		<div class="controls">
			<g:textField name="categoryName" value="${courseBlockInstance?.name }"/>
		</div>
	</div>
	<div class=" ${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')} ">
		<label for="rule" class="rule control-label"><g:message code="courseBlock.rule.label" default="Category Rule" /></label>
		<div class="controls">
			<g:textField name="rule" value="${courseBlockInstance?.rule}"/>
			<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'rule', 'error')}</span>
		</div>
	</div>
	<div >
		<div class="eval ${hasErrors(bean: courseBlockInstance, field: 'eval', 'error')} ">
			<label for="eval" class="control-label"><g:message code="courseBlock.eval.label" default="Criterion" /></label>
			<div class="controls">
				<g:select name="eval" from="${['A+','A','A-','B+','B','B-','C+','C','C-','Pass']}" noSelection="['':'']" value="${courseBlockInstance?.eval}" />
				<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'eval', 'error')}</span>
			</div>
		</div>
		<div class=" ${hasErrors(bean: courseBlockInstance, field: 'courseBlockCredits', 'error')} ">
			<label for="credits" class="control-label"><g:message code="courseBlock.credits.label" default="Credits" /></label>
			<div class="controls">
				<g:textField name="credits" value="${courseBlockInstance?.courseBlockCredits}" class="credits"/>
				<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courseBlockCredits', 'error')}</span>
			</div>
		</div>
	</div>
	<div style='clear:both;'></div>
	<div class=" ${hasErrors(bean: courseBlockInstance, field: 'courses', 'error')} ">
		<label for="courses" class="control-label"><g:message code="courseBlock.courses.label" default="Courses" /></label>
		<div class="control">
			<input type="button" class="btnShowModal" value="Course Search Tool" />
			<div class="classTable">
<%--	             <g:each in="${courseBlockInstance?.courses}" var="course">--%>
<%--					${course.courseSubject}${course.courseNumber}${course.title}${course.courseCredits}${course.term}<br>--%>
<%--				 </g:each>--%>
				 <g:render template="courseTable" model="[courseBlockInstance:courseBlockInstance]"/>
			</div>
			<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courses', 'error')}</span>
		</div>
	</div>
	<div class="control-group fieldcontain ${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')} ">
				<label for="comment" class="control-label"><g:message code="courseBlock.comment.label" default="Comment" /></label>
				<div class="controls">
					<g:textArea name="courseBlockComment" cols="40" rows="5" maxlength="2000" value="${courseBlockInstance?.courseBlockComment.decodeHTML().replaceAll('<br>','\n')}" style="width:88%;"/>
					<span class="help-inline">${hasErrors(bean: courseBlockInstance, field: 'courseBlockComment', 'error')}</span>
				</div>
	</div>
	<div class=" ${hasErrors(bean: courseBlockInstance, field: 'sortId', 'error')} ">
		<label for="sortId" class="control-label"><g:message code="courseBlock.sortId.label" default="Sequence" /></label>
		<div class="controls">
			<g:textField name="sortId" value="${courseBlockInstance?.sortId}" style="width:30px"/>
			<span class="help-inline">${hasErrors(bean: sortId, field: 'sortId', 'error')}</span>
		</div>
	</div>
	<input type="button" style="#B80707" class="deleteCourseBlock" value="X" style="width:23px; margin-left:98%;" title="Delete this category block" />
<%--	<input class="saveCourseBlock" type="button" value="Save to Degree"/>--%>
</div><!-- End of courseBlock -->
<%--				+ \'name=\' + escape(categoryName) + \'rule=\' + escape(rule) + \'courseBlockCredits=\' + escape(credits) + \'eval=\' + escape(eval) + \'prereqNotes=\' + escape(prereqNotes)'--%>