<%@ page import="degreebuilder.DegreeBlockType" %>
<script>
$(function() {
    $( "#accordion" ).accordion({
		collapsible:true,
		heightStyle:"content"
     });    
  });

$('#elements  li').dblclick(function(){
	<%--	$('#elements li').not($(this)).removeClass('selectedAdd');--%>
	<%--	$(this).toggleClass('selectedAdd');--%>
		var element = $(this);
		blockType = element.find("#blockType").val();
		displaySeqno = element.find("#displaySeqno").val();
		createDegreeBlock();
	});
</script>
		<div id="accordion">
			<h3>Core Courses</h3>	
			<div style="padding-left:0.5em; padding-top:0.5em; padding-right:1.2em;">
				<ul style="margin:0px;">	
					<g:each in="${coreCoursesBlockTypeList}" var="degreeBlockType">
						<li class="element"><div id="${degreeBlockType.name}"><strong>${degreeBlockType.name}</strong>
<%--						<g:hiddenField name="blockType" value="${degreeBlockType.id}"/>--%>
						<g:hiddenField name="blockType" value="${degreeBlockType.name}"/>
						<g:hiddenField name="displaySeqno" type="number" value="${degreeBlockType.displaySeqno}"/></div></li>
					</g:each>
				</ul>
			</div>
			<h3>Degree Electives</h3>	
			<div style="padding-left:0.5em; padding-top:0.5em; padding-right:1.2em;">
				<ul style="margin:0px;">					
					<g:each in="${degreeElectivesBlockTypeList}" var="degreeBlockType">
						<li class="element"><div id="${degreeBlockType.name}"><strong>${degreeBlockType.name}</strong>
<%--						<g:hiddenField name="blockType" value="${degreeBlockType.id}"/>--%>
						<g:hiddenField name="blockType" value="${degreeBlockType.name}"/>
						<g:hiddenField name="displaySeqno" value="${degreeBlockType.displaySeqno}"/></div></li>
					</g:each>
				</ul>
			</div>
			<h3>Degree Other</h3>	
			<div style="padding-left:0.5em; padding-top:0.5em; padding-right:1.2em;">
				<ul style="margin:0px;">					
					<g:each in="${degreeOtherBlockTypeList}" var="degreeBlockType">
						<li class="element"><div id="${degreeBlockType.name}"><strong>${degreeBlockType.name}</strong>
<%--						<g:hiddenField name="blockType" value="${degreeBlockType.id}"/>--%>
						<g:hiddenField name="blockType" value="${degreeBlockType.name}"/>
						<g:hiddenField name="displaySeqno" value="${degreeBlockType.displaySeqno}"/></div></li>
					</g:each>
				</ul>
			</div>
			<h3>Degree Specific Gen Ed Requirements</h3>	
			<div style="padding-left:0.5em; padding-top:0.5em; padding-right:1.2em;">
				<ul style="margin:0px;">					
					<g:each in="${degreeSpecificBlockTypeList}" var="degreeBlockType">
						<li class="element"><div id="${degreeBlockType.name}"><strong>${degreeBlockType.name}</strong>
<%--						<g:hiddenField name="blockType" value="${degreeBlockType.id}"/>--%>
						<g:hiddenField name="blockType" value="${degreeBlockType.name}"/>
						<g:hiddenField name="displaySeqno" value="${degreeBlockType.displaySeqno}"/></div></li>
					</g:each>
				</ul>
			</div>
		</div>


