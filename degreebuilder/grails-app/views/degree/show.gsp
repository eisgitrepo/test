
<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
	<link rel="stylesheet" href="${resource(dir: 'css', file: 'print.css')}" type="text/css" media="print">
	<style type="text/css" media="screen">
		.criteria {
			margin-right:20em;
		}
		table td {
			padding-right:0px;
		}
		.firstCell {
			padding-left:3.0em;
			width:80px;
		}
		.secondCell {
			width:380px;
		}
		.thirdCell {
			width:120px;
		}
		.fourthCell {
			width:160px;
		}
		.categoryInfo{
			padding-left:1.5em;
		}
		
		table th {
			text-align:left;
			text-decoration:underline;
		}
</style>
</head>

<body>

<section id="show-degree" class="first">
	<div class="degreeHeader">
		<span style="margin-right:30px;">${degreeInstance.college}</span><span>Catalog Year: ${degreeInstance.catalogYear}</span><br>
<%--	Need to check for null values in the fields	--%>
		<span style="margin-right:30px;">Degree Type: ${degreeInstance.degreeType}</span><span>&nbsp; &nbsp;Level: ${degreeInstance.level } </span>
		<span>&nbsp; &nbsp;Subject: ${degreeInstance.degreeSubject}<g:if test="${degreeInstance.degreeConcentration?.length() > 0}"> &nbsp; &nbsp;  Option: ${degreeInstance.degreeConcentration}</g:if><g:if test="${degreeInstance.degreeTrack?.length() > 0}"> &nbsp; &nbsp; Track: ${degreeInstance.degreeTrack}</g:if></span><br>
		<span style="margin-right:30px;">Total Credits: ${degreeInstance.totalCredits} &nbsp; &nbsp; Cumulative GPA Required: ${degreeInstance.cumulativeGPArequired}</span>
	</div>
	<br>
	<!-- TODO Need to loop through each degreeBlock and display the relevant information -->
	
	<!-- DegreeBlock Information -->
	<g:each in="${degreeInstance?.degreeBlocks?.sort()}" var="degreeBlock">
		<strong>${degreeBlock?.degreeBlockType?.name?.encodeAsHTML()}</strong>
		<div class="categoryInfo">
			<g:set var="categoryIndex" value="${1}" />
			<g:each in="${degreeBlock.courseBlocks.sort{it.sortId}}" var="courseBlock">
				<g:if test="${categoryIndex == 1}">
					<span>Category Name: ${courseBlock.name}</span><br>
					<g:if test="${degreeBlock?.degreeBlockType?.name != 'Additional Requirements'}">
						<span>Rule: ${courseBlock.rule}</span><br>
						<span class="criteria">Criterion: <strong>${courseBlock.eval}</strong></span><span class="credits">Number of Credits <strong>${courseBlock.courseBlockCredits}</strong></span><br>
						<span style="padding-left:1.75em;">Course Listing</span>
						<table id="degreeBlockCourses" >
							<g:each in="${courseBlock.courses.sort()}" var="course">
								<tr>
									<td class="firstCell">${course.courseSubject} ${course.courseNumber}</td>
									<td class="secondCell">${course.title}</td>
									<td class="thirdCell">${course.courseCredits}</td>
									<td class="fourthCell">${course.term}</td>
								</tr>		
							</g:each>
						</table>
						<span>Commentary: ${courseBlock.courseBlockComment.decodeHTML()}</span><br>
						
						<br>
					</g:if>
					<g:else>
						<span>Commentary: ${courseBlock.courseBlockComment.decodeHTML()}</span><br>
					</g:else>
				</g:if>
				<g:else>
					<div id="subCategory" style="margin-left:45px;">
					<span>Subcategory Name: ${courseBlock.name}</span><br>
					<g:if test="${degreeBlock?.degreeBlockType?.name != 'Additional Requirements'}">
						<span>Rule: ${courseBlock.rule}</span><br>
						<span class="criteria">Criterion: <strong>${courseBlock.eval}</strong></span><span class="credits">Number of Credits <strong>${courseBlock.courseBlockCredits}</strong></span><br>
						<span style="padding-left:1.75em;">Course Listing</span>
						<table id="degreeBlockCourses" >
							<g:each in="${courseBlock.courses.sort()}" var="course">
								<tr>
									<td class="firstCell">${course.courseSubject} ${course.courseNumber}</td>
									<td class="secondCell">${course.title}</td>
									<td class="thirdCell">${course.courseCredits}</td>
									<td class="fourthCell">${course.term}</td>
								</tr>		
							</g:each>
						</table>
						<span>Commentary: ${courseBlock.courseBlockComment.decodeHTML()}</span><br>
						
						<br>
					</g:if>
					</div>
				</g:else>
				<g:set var="categoryIndex" value="${categoryIndex + 1}" />
			</g:each>
			<g:if test="${degreeBlock?.degreeBlockType?.name != 'Additional Requirements'}">
				<p>Commentary: ${degreeBlock.degreeBlockComment}</p>
			</g:if>
		</div>
	</g:each>
	<!-- End of DegreeBlock Information -->
	
	<strong>Degree Commentary</strong>
	<p>${degreeInstance.degreeComment}</p>
</section>

</body>

</html>
