<%@ page import="degreebuilder.DegreeType" %>

	<div class="control-group fieldcontain ${hasErrors(bean: degreeTypeInstance, field: 'code', 'error')} ">
		<label for="code" class="control-label"><g:message code="degreeType.code.label" default="Code" /></label>
		<div class="controls">
			<g:textField name="code" value="${degreeTypeInstance?.code}"/>
			<span class="help-inline">${hasErrors(bean: degreeTypeInstance, field: 'code', 'error')}</span>
		</div>
	</div>

	<div class="control-group fieldcontain ${hasErrors(bean: degreeTypeInstance, field: 'desc', 'error')} ">
		<label for="desc" class="control-label"><g:message code="degreeType.desc.label" default="Desc" /></label>
		<div class="controls">
			<g:textField name="desc" maxlength="80" value="${degreeTypeInstance?.desc}"/>
				<span class="help-inline">${hasErrors(bean: degreeTypeInstance, field: 'desc', 'error')}</span>
		</div>
	</div>

