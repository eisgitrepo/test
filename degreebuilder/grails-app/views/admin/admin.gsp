<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>
<div id="tabs">
  <ul>
    <li><a href="${createLink(controller: 'degreeType', action: 'list')}">Degree Types</a></li>
    <li><a href="${createLink(controller: 'degreeBlockType', action: 'list')}">Degree Block Types</a></li>
<%--    <li><a href="${createLink(controller: 'tabcomponent', action: 'search')}">Search</a></li>--%>
<%--    <li><a href="${createLink(controller: 'tabcomponent', action: 'result')}">Result</a></li>--%>
  </ul>
</div>
testing admin gsp
</body>
</html>