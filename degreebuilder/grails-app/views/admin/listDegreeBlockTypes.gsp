<%@ page import="degreebuilder.DegreeBlockType" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degreeBlockType.label', default: 'DegreeBlockType')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<section id="list-degreeBlockType" class="first">
	<table class="table table-bordered">
		<thead>
			<tr>
				<g:sortableColumn property="name" title="${message(code: 'degreeBlockType.name.label', default: 'Name')}" />
				<g:sortableColumn property="displayGroup" title="${message(code: 'degreeBlockType.displayGroup.label', default: 'Display Group')}" />
				<g:sortableColumn property="displaySeqno" title="${message(code: 'degreeBlockType.displaySeqno.label', default: 'Display Seqno')}" />
				<g:sortableColumn property="dateCreated" title="${message(code: 'degreeBlockType.dateCreated.label', default: 'Date Created')}" />
				<g:sortableColumn property="lastUpdated" title="${message(code: 'degreeBlockType.lastUpdated.label', default: 'Last Updated')}" />
			</tr>
		</thead>
		<tbody>
		<g:each in="${degreeBlockTypeInstanceList}" status="i" var="degreeBlockTypeInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td><g:link action="show" id="${degreeBlockTypeInstance.id}">${fieldValue(bean: degreeBlockTypeInstance, field: "name")}</g:link></td>
				<td>${fieldValue(bean: degreeBlockTypeInstance, field: "displayGroup")}</td>
				<td>${fieldValue(bean: degreeBlockTypeInstance, field: "displaySeqno")}</td>
				<td><g:formatDate date="${degreeBlockTypeInstance.dateCreated}" /></td>
				<td><g:formatDate date="${degreeBlockTypeInstance.lastUpdated}" /></td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${degreeBlockTypeInstanceTotal}" />
	</div>
</section>
</body>
</html>