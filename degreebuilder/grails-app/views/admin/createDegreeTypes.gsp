<%@ page import="degreebuilder.Degree" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degree.label', default: 'Degree')}" />
	<title><g:message code="default.create.label" args="[entityName]" /></title>
</head>

<body>

<section id="create-degree-type" class="first">
	<g:hasErrors bean="${degreeTypeInstance}">
	<div class="alert alert-error">
		<g:renderErrors bean="${degreeTypeInstance}" as="list" />
	</div>
	</g:hasErrors>

	<g:form method="post" class="form-horizontal" >
		<g:hiddenField name="id" value="${degreeTypeInstance?.id}" />
		<g:hiddenField name="version" value="${degreeTypeInstance?.version}" />
		<fieldset class="form">
			<g:render template="degreeTypeForm"/>
		</fieldset>
		<div class="form-actions">
			<g:actionSubmit class="btn btn-primary" action="save" value="${message(code: 'default.button.save.label', default: 'Save')}" />
            <button class="btn" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
		</div>
	</g:form>
</section>
</body>
</html>
