
<%@ page import="degreebuilder.DegreeBlockType" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degreeBlockType.label', default: 'DegreeBlockType')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-degreeBlockType" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlockType.name.label" default="Name" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: degreeBlockTypeInstance, field: "name")}</td>
				
			</tr>

			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlockType.displayGroup.label" default="Display Group"/></td>
				
				<td valign="top" class="value">${fieldValue(bean: degreeBlockTypeInstance, field: "displayGroup")}</td>
				
			</tr>
				
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlockType.displaySeqno.label" default="Display Seqno" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: degreeBlockTypeInstance, field: "displaySeqno")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlockType.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${degreeBlockTypeInstance?.dateCreated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="degreeBlockType.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${degreeBlockTypeInstance?.lastUpdated}" /></td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
