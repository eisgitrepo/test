<%@ page import="degreebuilder.DegreeBlockType" %>
			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockTypeInstance, field: 'name', 'error')} ">
				<label for="Name" class="control-label"><g:message code="degreeBlockType.name.label" default="Name" /></label>
				<div class="controls">
					<g:textField name="name" maxlength="50" value="${degreeBlockTypeInstance?.name}"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockTypeInstance, field: 'name', 'error')}</span>
				</div>
			</div>

			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockTypeInstance, field: 'displayGroup', 'error')} required">
				<label for="displayGroup" class="control-label"><g:message code="degreeBlockType.displayGroup.label" default="Display Group" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:select name="displayGroup" value="${degreeBlockTypeInstance.displayGroup}" from="${['Core Courses','Degree Electives','Degree Other','Degree Specific Gen Ed Courses']}"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockTypeInstance, field: 'displayGroup', 'error')}</span>
				</div>
			</div>
			
			<div class="control-group fieldcontain ${hasErrors(bean: degreeBlockTypeInstance, field: 'displaySeqno', 'error')} required">
				<label for="displaySeqno" class="control-label"><g:message code="degreeBlockType.displaySeqno.label" default="Display Seqno" /><span class="required-indicator">*</span></label>
				<div class="controls">
					<g:field type="number" name="displaySeqno" required="" value="${degreeBlockTypeInstance.displaySeqno}"/>
					<span class="help-inline">${hasErrors(bean: degreeBlockTypeInstance, field: 'displaySeqno', 'error')}</span>
				</div>
			</div>

