<%@ page import="degreebuilder.DegreeType" %>
<!doctype html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'degreeType.label', default: 'DegreeType')}" />
	<title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<section id="list-degreeType" class="first">
	<table class="table table-bordered">
		<thead>
			<tr>
				<g:sortableColumn property="code" title="${message(code: 'degreeType.code.label', default: 'Code')}" />
				<g:sortableColumn property="desc" title="${message(code: 'degreeType.desc.label', default: 'Desc')}" />
				<g:sortableColumn property="dateCreated" title="${message(code: 'degreeType.dateCreated.label', default: 'Date Created')}" />
				<g:sortableColumn property="lastUpdated" title="${message(code: 'degreeType.lastUpdated.label', default: 'Last Updated')}" />
			</tr>
		</thead>
		<tbody>
		<g:each in="${degreeTypeInstanceList}" status="i" var="degreeTypeInstance">
			<tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
				<td><g:link action="edit" id="${degreeTypeInstance.id}">${fieldValue(bean: degreeTypeInstance, field: "code")}</g:link></td>
				<td>${fieldValue(bean: degreeTypeInstance, field: "desc")}</td>
				<td><g:formatDate date="${degreeTypeInstance.dateCreated}" /></td>
				<td><g:formatDate date="${degreeTypeInstance.lastUpdated}" /></td>
			</tr>
		</g:each>
		</tbody>
	</table>
	<div class="pagination">
		<bs:paginate total="${degreeTypeInstanceTotal}" />
	</div>
</section>
</body>
</html>
