<html>
	<head>
		<title>403 - Forbidden!</title>
		<meta name="layout" content="kickstart" />
		<g:set var="layout_nomainmenu"		value="${true}" scope="request"/>
		<g:set var="layout_nosecondarymenu"	value="${true}" scope="request"/>
	</head>

<body>
	<content tag="header">
		<!-- Empty Header -->
	</content>
	
  	<section id="Error" class="">
		<div class="big-message">
			<div class="container">
				<h1>
					<g:message code="error.403.callout"/>
				</h1>
				<h2>
					<g:message code="error.403.title"/>
				</h2>
				<p>
					<g:message code="error.403.message"/>
				</p>
<%--				<object type="application/x-shockwave-flash" style="width:853px; height:480px;" data="http://www.youtube.com/v/E0TB3SjS6e4?color2=FBE9EC&amp;rel=0&amp;autoplay=1&amp;showsearch=0&amp;version=3">--%>
<%--			        <param name="movie" value="http://www.youtube.com/v/E0TB3SjS6e4?color2=FBE9EC&amp;rel=0&amp;autoplay=1&amp;showsearch=0&amp;version=3" />--%>
<%--			        <param name="allowFullScreen" value="true" />--%>
<%--			        <param name="allowscriptaccess" value="always" />--%>
<%--        		</object>--%>
<%--				<div class="actions">--%>
<%--					<a href="${createLink(uri: '/')}" class="btn btn-large btn-primary">--%>
<%--						<i class="icon-chevron-left icon-white"></i>--%>
<%--						<g:message code="error.button.backToHome"/>--%>
<%--					</a>--%>
<%--					<a href="${createLink(uri: '/contact')}" class="btn btn-large btn-success">--%>
<%--						<i class="icon-envelope"></i>--%>
<%--						<g:message code="error.button.contactSupport"/>--%>
<%--					</a>					--%>
<%--				</div>--%>
			</div>
		</div>
	</section>
  
  
  </body>
</html>