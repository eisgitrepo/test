<%@ page import="degreebuilder.Keyword" %>
	<div class="controls">
		<g:select name="keyword" class="dataInput" required="" from="${Keyword.list()}" 
			  optionKey="keyword?.id" optionValue="keyword?.value"
			  noSelection="${['':'-Choose Keyword-']}" value="${keywordInstance?.value}"/>
		<span class="help-inline">${hasErrors(bean: degreeInstance, field: 'keyword', 'error')}</span>
	</div>
<%--			<div class="control-group fieldcontain ${hasErrors(bean: keywordInstance, field: 'value', 'error')} ">--%>
<%--				<label for="value" class="control-label"><g:message code="keyword.value.label" default="Value" /></label>--%>
<%--				<div class="controls">--%>
<%--					<g:textField name="value" maxlength="200" value="${keywordInstance?.value}"/>--%>
<%--					<span class="help-inline">${hasErrors(bean: keywordInstance, field: 'value', 'error')}</span>--%>
<%--				</div>--%>
<%--			</div>--%>
<%----%>
<%--			<div class="control-group fieldcontain ${hasErrors(bean: keywordInstance, field: 'degrees', 'error')} ">--%>
<%--				<label for="degrees" class="control-label"><g:message code="keyword.degrees.label" default="Degrees" /></label>--%>
<%--				<div class="controls">--%>
<%--					--%>
<%--					<span class="help-inline">${hasErrors(bean: keywordInstance, field: 'degrees', 'error')}</span>--%>
<%--				</div>--%>
<%--			</div>--%>

