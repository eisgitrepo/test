
<%@ page import="degreebuilder.Keyword" %>
<!doctype html>
<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="layout" content="kickstart" />
	<g:set var="entityName" value="${message(code: 'keyword.label', default: 'Keyword')}" />
	<title><g:message code="default.show.label" args="[entityName]" /></title>
</head>

<body>

<section id="show-keyword" class="first">

	<table class="table">
		<tbody>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="keyword.value.label" default="Value" /></td>
				
				<td valign="top" class="value">${fieldValue(bean: keywordInstance, field: "value")}</td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="keyword.dateCreated.label" default="Date Created" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${keywordInstance?.dateCreated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="keyword.lastUpdated.label" default="Last Updated" /></td>
				
				<td valign="top" class="value"><g:formatDate date="${keywordInstance?.lastUpdated}" /></td>
				
			</tr>
		
			<tr class="prop">
				<td valign="top" class="name"><g:message code="keyword.degrees.label" default="Degrees" /></td>
				
				<td valign="top" style="text-align: left;" class="value">
					<ul>
					<g:each in="${keywordInstance.degrees}" var="d">
						<li><g:link controller="degree" action="show" id="${d.id}">${d?.encodeAsHTML()}</g:link></li>
					</g:each>
					</ul>
				</td>
				
			</tr>
		
		</tbody>
	</table>
</section>

</body>

</html>
