<%@ page import="degreebuilder.Degree"%>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="layout" content="kickstart" />
<g:set var="entityName"
	value="${message(code: 'keyword.label', default: 'Keyword')}" />
<title><g:message code="default.list.label" args="[entityName]" /></title>
<g:javascript src="datatables.js" />
</head>

<body>

	<section id="list-degree-keyword" class="first">
		<table class="table table-bordered" id="DegreeTable">
			<thead>
				<tr>
					<th><b>Id</b></th>
					<th><b>AI</b></th>
					<th><b>UpdatedBy</b></th>
					<th><b>Year</b></th>
					<th><b>School/College</b></th>
					<th><b>Type</b></th>
					<th><b>Level</b></th>
					<th><b>Subject</b></th>
					<th><b>Option</b></th>
					<th><b>Track</b></th>
					<th><b>CR</b></th>
				</tr>
			</thead>

			<tbody>
				<tr>
					<td>${fieldValue(bean: degreeInstance, field: "id")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "degreeApproved")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "updatedBy")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "catalogYear")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "college")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "degreeType")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "level")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "degreeSubject")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "degreeConcentration")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "degreeTrack")}</td>
					<td>${fieldValue(bean: degreeInstance, field: "totalCredits")}</td>
				</tr>
			</tbody>
		</table>
		<g:form action="save" class="form-horizontal">
			<fieldset class="form">
				<g:hiddenField name="degree" value="${degreeInstance.id}" />
				<table class="table table-bordered" id="keyWordTable">
					<thead>
						<tr>
							<g:sortableColumn property="keyword"
								title="${message(code: 'keyword.value.label', default: 'Keyword')}" />
						</tr>
					</thead>
					<tbody>
						<g:each in="${keywordInstanceList.sort{it.id}}" status="i" var="keywordInstance">
							<tr>
							<g:set var="checkboxChecked" value="false" />
							<g:if test="${degreeKeywordsSelected.contains(keywordInstance.id)}">
								<g:set var="checkboxChecked" value="true" />
							</g:if>
								<td><g:checkBox name="${keywordInstance.id}" checked="${checkboxChecked}"/> ${fieldValue(bean: keywordInstance, field: "value")}</td>
							</tr>
						</g:each>
					</tbody>
				</table>
			</fieldset>
			<div class="form-actions">
				<g:submitButton name="save" class="btn btn-primary"
					value="${message(code: 'default.button.save.label', default: 'Save KeyWords')}" />
				<button class="btn" type="reset">
					<g:message code="default.button.reset.label" default="Reset" />
				</button>
			</div>
		</g:form>

	</section>
	<script type="text/javascript">
		$(document).ready(function() {
			var oTable = $('#keyWordTable').dataTable({
				"bScrollInfinite" : true,
				"bScrollCollapes" : true,
				"sScrollY" : "350px"
			});

			$("thead input").keyup(function() {
				/* Filter on the column (the index) of this element */
				oTable.fnFilter(this.value, $("thead input").index(this));
			});

			/*
			 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
			 * the footer
			 */
			$("thead input").each(function(i) {
				asInitVals[i] = this.value;
			});

			$("thead input").focus(function() {
				if (this.className == "search_init") {
					this.className = "";
					this.value = "";
				}
			});

			$("thead input").blur(function(i) {
				if (this.value == "") {
					this.className = "search_init";
					this.value = asInitVals[$("thead input").index(this)];
				}
			});
		});
	</script>
</body>
</html>
