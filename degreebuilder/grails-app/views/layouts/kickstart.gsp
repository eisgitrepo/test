<!DOCTYPE html>
<%-- <html lang="${org.springframework.web.servlet.support.RequestContextUtils.getLocale(request).toString().replace('_', '-')}"> --%>
<html lang="${session.'org.springframework.web.servlet.i18n.SessionLocaleResolver.LOCALE'}">
<head>
    <title><g:layoutTitle default="${meta(name:'app.name')}" /></title>
    <style type="text/css" media="screen">
    .navbar-fixed-top {
    }
    .navbar-fixed-top, .navbar-fixed-bottom {
    }
    </style>
    <meta charset="utf-8">
    <meta name="viewport"		content="width=device-width, initial-scale=1.0">
    <meta name="description"	content="">
    <meta name="author"			content="">
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
    <link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon.png">
    <link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon-72x72.png"	sizes="72x72">
    <link rel="apple-touch-icon"	href="assets/ico/apple-touch-icon-114x114.png"	sizes="114x114">

    <%-- Manual switch for the skin can be found in /view/_menu/_config.gsp --%>
    <r:require modules="jquery"/>
    <r:require modules="dataTables"/>

    <r:require modules="bootstrap"/>
    <r:require modules="bootstrap_utils"/>

    <r:layoutResources />
    <g:layoutHead />
    <link href="https://www.umt.edu/_common/resources/css/um_branding.css" rel="stylesheet" type="text/css">
    <script src="https://www.umt.edu/_common/resources/js/um.js" type="text/javascript" xml:space="preserve">
    </script>
    <!--[if lt IE 7]>
           <script type="text/javascript" src="https://www.umt.edu/_common/resources/css/iepngfix/unitpngfix.js"></script>
       <![endif]-->
    <!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
    <!--[if lt IE 9]>
		<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

    <%-- For Javascript see end of body --%>
    <%--	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js" type="text/javascript"></script> --%>
    <script type="text/javascript">
        $( window ).load(function() {
            var wintimeout;

            function SetWinTimeout() {
                wintimeout = window.setTimeout("window.location.href='https://login.umt.edu/cas/logout';",7200000); //after 120 minutes i.e. 120 * 60 * 1000
            }
            $('body').click(function() {

                window.clearTimeout(wintimeout); //when user clicks remove timeout and reset it

                SetWinTimeout();

            });
            SetWinTimeout();
        });
    </script>
</head>

<body>
<div class="um_standard_header">
    <div id="accessibenav">
        Accessible Navigation. Go to: <a href="#mainnav">Navigation</a>
        <a href="#maincontent">Main Content</a>
        <a href="#footercontent">Footer</a>
    </div>
    <div id="logo">
        <a href="https://www.umt.edu/" target="_parent" title="The University of Montana Homepage"><img alt="The University of Montana" src="https://www.umt.edu/_common/resources/imx/umlogo200w.png" title="UM Logo"></a>
    </div>
    <div id="TopSearch">
        <div id="navrightlinks_wrap">
            <ul>
                <li>
                    <a href="https://www.umt.edu/home/atoz/">A to Z Index</a>
                </li>
                <li>
                    <a class="last" href="https://www.umt.edu/home/directory/">Directory</a>
                </li>
                <li>
                    <a class="header_last" href="https://www.umt.edu/">UM Home</a>
                </li>
            </ul>
        </div>
        <div class="search">
            <form accept-charset="utf-8" action="https://www.umt.edu/home/search/" id="cse-search-box" method="get">
                <label for="q">
                    Search UM
                </label>
                <input class="gradient_button search_input" id="q" name="q" onblur="resetText(this);" onfocus="clearBox(this);" placeholder="Search UM" value="Search UM" type="text"><input name="cx" value="004842374792843146445:2r-2xi1nlr4" type="hidden"><input name="cof" value="FORID:10" type="hidden"><input name="ie" value="UTF-8" type="hidden"><input class="gradient_button search_button" name="sa" value="Go" type="submit">
            </form>
        </div>
    </div>
</div>
<%--	<g:render template="/_menu/navbar"/>														--%>

<%--	<!-- Enable to overwrite Header by individual page -->--%>
<%--	<g:if test="${ pageProperty(name:'page.header') }">--%>
<%--   		<g:pageProperty name="page.header" />--%>
<%--	</g:if>--%>
<%--	<g:else>--%>
<%--		<g:render template="/layouts/header"/>														--%>
<%--	</g:else>--%>

<g:render template="/layouts/content"/>

<!-- Enable to overwrite Footer by individual page -->
<%--	<g:if test="${ pageProperty(name:'page.footer') }">--%>
<%--	    <g:pageProperty name="page.footer" />--%>
<%--	</g:if>--%>
<%--	<g:else>--%>
<%--		<g:render template="/layouts/footer"/>														--%>
<%--	</g:else>--%>

<!-- Enable to insert additional components (e.g., modals, javascript, etc.) by any individual page -->
<%--	<g:if test="${ pageProperty(name:'page.include.bottom') }">--%>
<%--   		<g:pageProperty name="page.include.bottom" />--%>
<%--	</g:if>--%>
<%--	<g:else>--%>
<%--		<!-- Insert a modal dialog for registering (for an open site registering is possible on any page) -->--%>
<%--		<g:render template="/_common/modals/registerDialog" model="[item: item]"/>--%>
<%--	</g:else>--%>

<!-- Included Javascript files and other resources -->
<r:layoutResources />
<div style="clear: both;">
</div>
<div class="um_standard_footer" id="footercontent">
    <ul>
        <li>
            <a href="https://www.umt.edu/">The University of Montana</a>
        </li>
        <li>
            Missoula, MT
        </li>
        <li>
            <a href="https://www.umt.edu/home/comments">Contact UM Switchboard</a>
        </li>
        <li class="last">
            <a href="https://www.umt.edu/home/accessibility">Accessibility</a>
        </li>
    </ul>
</div>
</body>

</html>