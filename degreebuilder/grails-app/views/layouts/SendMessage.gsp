<g:applyLayout name="main">
	<html>
	<head>
		<meta name="layout" content="main"/>
		<title><g:layoutTitle default="Try this"/></title>
		<g:layoutHead/>
	</head>
	<body>
		<div id="left">
			<g:pageProperty name="page.left1"/>
			<g:pageProperty name="page.left2"/>
			<g:pageProperty name="page.left3"/>
			<div id="left-top" style="border:1px solid black;">
				<g:pageProperty name="page.left-top"/>
			</div>
			<div id="left-bottom">
				<div id="box-left" style="border:1px solid black;">
					<g:pageProperty name="page.box-left"/>
				</div>
				<div id="box-right" style="border:1px solid black;">
					<g:pageProperty name="page.box-right"/>
				</div>
			</div>
		</div>
		<div id="right">
			<g:pageProperty name="page.right1"/>
			<g:pageProperty name="page.right2"/>
			<g:pageProperty name="page.right3"/>
			<div id="right-top" style="border:1px solid black;">
				<g:pageProperty name="page.right-top"/>
			</div>
			<div id="right-bottom" style="border:1px solid black;">
				<g:pageProperty name="page.right-bottom"/>
			</div>
		</div>
	</body>
	</html>
</g:applyLayout>
