package degreebuilder

import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured;

/**
 * DegreeBlockTypeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Secured(['ROLE_UMT_IT_EIS_DEGREE_BUILDER_ROLE_DEGREE_BUILDER_ADMIN'])
class DegreeBlockTypeController {
	def degreebuilderService
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
		def degreeBlockTypeInstanceList = DegreeBlockType.list(params)
		degreebuilderService.sortDegreeBlockTypes(degreeBlockTypeInstanceList)
//		degreeBlockTypeInstanceList.each { it ->
//			println it.name + " " + it.displaySeqno + " " + it.displayGroup
//        }
   		[degreeBlockTypeInstanceList: degreeBlockTypeInstanceList, degreeBlockTypeInstanceTotal: DegreeBlockType.count()]
    }

    def create() {
        [degreeBlockTypeInstance: new DegreeBlockType(params)]
    }

    def save() {
        def degreeBlockTypeInstance = new DegreeBlockType(params)
        if (!degreeBlockTypeInstance.save(flush: true)) {
            render(view: "create", model: [degreeBlockTypeInstance: degreeBlockTypeInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), degreeBlockTypeInstance.id])
        redirect(action: "show", id: degreeBlockTypeInstance.id)
    }

    def show() {
        def degreeBlockTypeInstance = DegreeBlockType.get(params.id)
        if (!degreeBlockTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "list")
            return
        }

        [degreeBlockTypeInstance: degreeBlockTypeInstance]
    }

    def edit() {
        def degreeBlockTypeInstance = DegreeBlockType.get(params.id)
        if (!degreeBlockTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "list")
            return
        }

        [degreeBlockTypeInstance: degreeBlockTypeInstance]
    }

    def update() {
        def degreeBlockTypeInstance = DegreeBlockType.get(params.id)
        if (!degreeBlockTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (degreeBlockTypeInstance.version > version) {
                degreeBlockTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'degreeBlockType.label', default: 'DegreeBlockType')] as Object[],
                          "Another user has updated this DegreeBlockType while you were editing")
                render(view: "edit", model: [degreeBlockTypeInstance: degreeBlockTypeInstance])
                return
            }
        }

        degreeBlockTypeInstance.properties = params

        if (!degreeBlockTypeInstance.save(flush: true)) {
            render(view: "edit", model: [degreeBlockTypeInstance: degreeBlockTypeInstance])
			return
        }		

		flash.message = message(code: 'default.updated.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), degreeBlockTypeInstance.id])
        redirect(action: "show", id: degreeBlockTypeInstance.id)
    }

    def delete() {
        def degreeBlockTypeInstance = DegreeBlockType.get(params.id)
        if (!degreeBlockTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "list")
            return
        }

        try {
            degreeBlockTypeInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'degreeBlockType.label', default: 'DegreeBlockType'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
