package degreebuilder

import org.springframework.dao.DataIntegrityViolationException

/**
 * DegreeBlockController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class DegreeBlockController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [degreeBlockInstanceList: DegreeBlock.list(params), degreeBlockInstanceTotal: DegreeBlock.count()]
    }

    def create(Long id) {
	//	println "Made it to the controller"
	//	params.each {
	//	println it
	//	}
		params.degree = Degree.get(id)
        [degreeBlockInstance: new DegreeBlock(params)]
    }

    def save() {
        def degreeBlockInstance = new DegreeBlock(params)
        if (!degreeBlockInstance.save(flush: true)) {
            render(view: "create", model: [degreeBlockInstance: degreeBlockInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), degreeBlockInstance.id])
        redirect(action: "show", id: degreeBlockInstance.id)
    }

    def show() {
        def degreeBlockInstance = DegreeBlock.get(params.id)
        if (!degreeBlockInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "list")
            return
        }

        [degreeBlockInstance: degreeBlockInstance]
    }

    def edit() {
        def degreeBlockInstance = DegreeBlock.get(params.id)
        if (!degreeBlockInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "list")
            return
        }

        [degreeBlockInstance: degreeBlockInstance]
    }

    def update() {
        def degreeBlockInstance = DegreeBlock.get(params.id)
        if (!degreeBlockInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (degreeBlockInstance.version > version) {
                degreeBlockInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'degreeBlock.label', default: 'DegreeBlock')] as Object[],
                          "Another user has updated this DegreeBlock while you were editing")
                render(view: "edit", model: [degreeBlockInstance: degreeBlockInstance])
                return
            }
        }

        degreeBlockInstance.properties = params

        if (!degreeBlockInstance.save(flush: true)) {
            render(view: "edit", model: [degreeBlockInstance: degreeBlockInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), degreeBlockInstance.id])
        redirect(action: "show", id: degreeBlockInstance.id)
    }

    def delete() {
        def degreeBlockInstance = DegreeBlock.get(params.id)
        if (!degreeBlockInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "list")
            return
        }

        try {
            degreeBlockInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'degreeBlock.label', default: 'DegreeBlock'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
