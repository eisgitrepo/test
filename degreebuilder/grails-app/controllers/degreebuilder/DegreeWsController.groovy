package degreebuilder

/**
 * DegreeWsController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class DegreeWsController {

	def show (Degree degree) {
		respond degree
	}
}
