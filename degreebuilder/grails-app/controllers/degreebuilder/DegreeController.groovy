package degreebuilder

import grails.converters.JSON
import javassist.bytecode.stackmap.BasicBlock.Catch;

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.context.ContextLoader;
import org.springframework.security.core.GrantedAuthority;
import grails.plugin.springsecurity.annotation.Secured;

/**
 * DegreeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Secured(['ROLE_UMT_IT_EIS_DEGREE_BUILDER_ROLE_DEGREE_BUILDER'])
class DegreeController {
	def degreebuilderService
	def springSecurityService
	def grailsApplication
	
    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

	def approveDegree = {
		if (params.degreeId) {
			def degreeInstance = Degree.get(params.degreeId)
			if (!degreeInstance) {
				flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), degreeId])
				return
			}
			if (degreeInstance.degreeApproved == '1') {
				degreeInstance.degreeApproved = null
				if (!degreeInstance.save(flush: true)) {
					return
				}
				render "Degree ${degreeInstance.id} not approved"
			}
			else{
				degreeInstance.degreeApproved = 1
				if (!degreeInstance.save(flush: true)) {
					return
				}
				render "Degree ${degreeInstance.id} was approved"
			}
		}
	}
	
	def copyDegrees = {
		
	}
	
	def copyDegreesByCatalogYear () {
	//	println params.oldCatalogYear
	//	println params.newCatalogYear
		degreebuilderService.copyDegreesByCatalogYear(params.oldCatalogYear, params.newCatalogYear)
		redirect (action: "list")
	}
	
    def createDegreeBlock = {
		def degree = Degree.get(params.degree)
		DegreeBlockType degreeBlockType = DegreeBlockType.findByNameLike(params.blockType)
		def degreeBlockInstance = degreebuilderService.saveDegreeBlock(degree,degreeBlockType,params)
		def courseBlockInstance = degreebuilderService.saveCourseBlock(degreeBlockInstance, params)
		degreeBlockInstance.addToCourseBlocks(courseBlockInstance)

		render (template:"degreeBlock", model:[degreeBlockInstance:degreeBlockInstance,courseBlockInstance:courseBlockInstance])
	}
		
	def createCourse = {
		def courseBlockInstance = CourseBlock.get(params.courseBlockId)
		def query = Course.where{
			courseBlock{id == params.courseBlockId}  && courseSubject == params.courseSubject && courseNumber == params.courseNumber
		}
		
		def results = query.list()
		if (results.size() > 0 ) {						
            response.status=500
			render (template:"courseTable", model:[courseBlockInstance:courseBlockInstance])
		}	
		else{	
			degreebuilderService.saveCourse(courseBlockInstance, params)
			courseBlockInstance.refresh()
			render (template:"courseTable", model:[courseBlockInstance:courseBlockInstance])
		}
	}
	
		def deleteCourse = {
		def courseInstance = Course.get(params.courseId)
		def courseBlockInstance = CourseBlock.get(courseInstance.courseBlock.id)
		courseBlockInstance.removeFromCourses(courseInstance)
		courseInstance.delete(flush:true)
		courseBlockInstance.refresh()
		render (template:"courseTable", model:[courseBlockInstance:courseBlockInstance])
		
	}

	def updateCourse = {
		def courseInstance = Course.get(params.courseId)
		def courseBlockInstance = CourseBlock.get(courseInstance.courseBlock.id)
		degreebuilderService.updateCourse(params)
		//courseBlockInstance.refresh()
		render (template:"courseTable", model:[courseBlockInstance:courseBlockInstance])
	}
	
	
	def createCourseBlock = {		
		def degreeBlockInstance = DegreeBlock.get(params.degreeBlockId)
		def courseBlockInstance = new CourseBlock(degreeBlock:degreeBlockInstance)
		courseBlockInstance = degreebuilderService.saveCourseBlock(degreeBlockInstance, params)
	//	courseBlockInstance.sortId = courseBlockInstance.id;
		render (template:"courseBlock", model:[degreeBlockInstance:degreeBlockInstance,courseBlockInstance:courseBlockInstance])
	}

	def deleteCourseBlock = {
		def courseBlockInstance = CourseBlock.get(params.courseBlockId)
		courseBlockInstance.delete(flush: true)				
	}
	
	
	def deleteDegreeBlock = {
		def degreeBlockInstance = DegreeBlock.get(params.degreeBlockId)
		degreeBlockInstance.delete(flush: true)
		
	}
	
    def _editDegreeBlock = {
			def degreeInstance = Degree.get(params.degree)
			render (template:"editDegreeBlock", model:[degreeInstance:degreeInstance])
	}

    def updateCourseBlock = {
		degreebuilderService.updateCourseBlock(params)
//		flash.message = message(code: 'default.updated.message', args: [message(code: 'degree.label', default: 'Save Successful'), degreeInstance.id])
		
	}
	
	def copy() {
		def degreeInstance = Degree.get(params.id)
		if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
			redirect(action: "list")
			return
		}
		Degree newDegreeInstance = degreebuilderService.copyDegree(degreeInstance)
		newDegreeInstance.updatedBy =  springSecurityService.getPrincipal().username
		
		Boolean ok = newDegreeInstance.validate()
		if (!newDegreeInstance.save(flush: true, failOnError:true)) {
			flash.message = "Copy failed"
			render(view: "show", model: [degreeInstance: degreeInstance])
			return
		}
        newDegreeInstance.catalogId = newDegreeInstance.id
        //println "ID: "+ newDegreeInstance.id
       // println "Catalog ID: " + newDegreeInstance.catalogId

		degreebuilderService.cloneKeywords(degreeInstance,newDegreeInstance)
		flash.message = "Copy succeeded -- new degree ${newDegreeInstance.id} copied from degree ${degreeInstance.id} by ${newDegreeInstance.updatedBy}"
		redirect(action: "palette", id: newDegreeInstance.id, calledFrom:params.calledFrom)
	}
	
	def create() {
		def bannerColleges = degreebuilderService.getCollegesFromBanner()
    	def bannerMajors = degreebuilderService.getMajorsFromBanner()
		def bannerCourses = degreebuilderService.getCoursesFromBanner()
		def bannerConcentrations = degreebuilderService.getConcsFromBanner()
		def degreeType = new DegreeType()
		[degreeInstance: new Degree(params), bannerColleges:bannerColleges, bannerMajors:bannerMajors, bannerConcentrations:bannerConcentrations, bannerCourses:bannerCourses, degreeType:degreeType]
    }
	
	def delete() {
        def degreeInstance = Degree.get(params.id)
        if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
            redirect(action: "list")
            return
        }

        try {
            degreeInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
	
	def displayDegreeBlockTypes() {
		def degreeBlockTypeInstanceList = DegreeBlockType.list(params)
		degreebuilderService.sortDegreeBlockTypes(degreeBlockTypeInstanceList)
		def coreCoursesBlockTypeList = []
		def degreeElectivesBlockTypeList = []
		def degreeOtherBlockTypeList = []
		def degreeSpecificBlockTypeList = []
		degreeBlockTypeInstanceList.each { it ->
			if (it.displayGroup == 'Core Courses'){
				coreCoursesBlockTypeList.add (it)
			}
			if (it.displayGroup == 'Degree Electives'){
				degreeElectivesBlockTypeList.add (it)
			}
			if (it.displayGroup == 'Degree Other'){
				degreeOtherBlockTypeList.add (it)
			}
			if (it.displayGroup == 'Degree Specific Gen Ed Courses'){
				degreeSpecificBlockTypeList.add (it)
			}
		}
		
		render(template: "degreeBlockTypes",
			model:	[coreCoursesBlockTypeList: coreCoursesBlockTypeList,
					degreeElectivesBlockTypeList: degreeElectivesBlockTypeList,
					degreeOtherBlockTypeList: degreeOtherBlockTypeList,
					degreeSpecificBlockTypeList: degreeSpecificBlockTypeList]
		)
	}
		
    def edit() {
        def degreeInstance = Degree.get(params.id)
        if (!degreeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
            redirect(action: "list")
            return
		}

		def bannerColleges = degreebuilderService.getCollegesFromBanner()
		def bannerMajors = degreebuilderService.getMajorsFromBanner()
		def bannerCourses = degreebuilderService.getCoursesFromBanner()
		def bannerConcentrations = degreebuilderService.getConcsFromBanner()
		
        [degreeInstance: degreeInstance, bannerColleges:bannerColleges, bannerMajors:bannerMajors, bannerConcentrations:bannerConcentrations, bannerCourses:bannerCourses, degreeType:degreeType]
    }	

	def index() {
        redirect(action: "list", params: params)
    }
	
	def list() {
		String netid = SecurityContextHolder?.getContext()?.getAuthentication()?.getName()
		if (netid != null){
			session.currentUser = netid
		}
		
		if (session.isAdminUser == null){
			def grouperRoles = SecurityContextHolder?.getContext()?.getAuthentication()?.getAuthorities()
			session.isAdminUser ='false'
			grouperRoles.each { it ->
				if (it.role == "ROLE_UMT_IT_EIS_DEGREE_BUILDER_ROLE_DEGREE_BUILDER_ADMIN")	
					session.isAdminUser = 'true'
			}			
		}
		
 //      params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [degreeInstanceList: Degree.list(params), degreeInstanceTotal: Degree.count()]    
	}

    def palette() {
		//check if this is a new degree or an update
		def degreeInstance
		if(params.id == null){
			degreeInstance = new Degree(params)
			degreeInstance.updatedBy =  springSecurityService.getPrincipal().username
			if (!degreeInstance.save(flush: true)) {		
				render(view: "list")
				flash.message = "There was a problem creating a new degree"
				return
			}
			params.id = degreeInstance.id
		}
		else{
			degreeInstance = Degree.get(params.id)
		}
    	def bannerColleges = degreebuilderService.getCollegesFromBanner()
    	def bannerMajors = degreebuilderService.getMajorsFromBanner()
		def bannerCourses = degreebuilderService.getCoursesFromBanner()
		def bannerConcentrations = degreebuilderService.getConcsFromBanner()
		def degreeType = new DegreeType()

		[degreeInstance: degreeInstance, bannerColleges:bannerColleges, bannerMajors:bannerMajors, bannerConcentrations:bannerConcentrations, bannerCourses:bannerCourses, degreeType:degreeType, calledFrom:params.calledFrom]
    }
	
    def saveDegree(){
		def degreeInstance = new Degree(params)
		degreeInstance.updatedBy =  springSecurityService.getPrincipal().username
		if (!degreeInstance.save(flush: true)) {
			def bannerColleges = degreebuilderService.getCollegesFromBanner()
			def bannerMajors = degreebuilderService.getMajorsFromBanner()
			def bannerCourses = degreebuilderService.getCoursesFromBanner()
			def bannerConcentrations = degreebuilderService.getConcsFromBanner()
			def degreeType = new DegreeType()
			render(view: "create", model: [degreeInstance: degreeInstance, bannerColleges:bannerColleges, bannerMajors:bannerMajors, bannerConcentrations:bannerConcentrations, bannerCourses:bannerCourses, degreeType:degreeType])
			//render(view: "create", model: [degreeInstance: degreeInstance])
			return
		}
		redirect(action: "palette", id: degreeInstance.id)
	}

	def show() {
        def degreeInstance = Degree.get(params.id)
        if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
            redirect(action: "list")
            return
        }

        [degreeInstance: degreeInstance]
    }

	def showAll() {
		//def allDegreesByCollege = Degree.findAllByCollegeAndCatalogYearAndDegreeApprovedAndDegreeSubjectIlike("Missoula College","2015-2016","1","%nursing%")
		def allDegreesByCatalogYear = Degree.findAllByCatalogYearAndDegreeApproved("2015-2016","1")
		allDegreesByCatalogYear.each{
			degreebuilderService.getDegreeCourseDescriptions(it)
		}


		//def allDegreesByCatalogYear = Degree.findAllByCatalogYearAndDegreeApproved("2015-2016","1")


		/*def degreeInstance = Degree.get(params.id)
		if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
			redirect(action: "list")
			return
		}*/

		[allDegreesByCollege:allDegreesByCatalogYear]
	}
    def update() {
    	def degreeInstance = Degree.get(params.degree)
		degreeInstance.updatedBy =  springSecurityService.getPrincipal().username
		degreeInstance.save()
    	if (!degreeInstance) {
    		flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
    				redirect(action: "list")
    				return
    	}
		degreebuilderService.updateDegree(params)

		def duplicateDegrees = Degree.where{
			catalogYear == params.catalogYear &&
					college == params.college &&
					degreeSubject == params.degreeSubject &&
					degreeType == params.degreeType &&
					level == params.level &&
					totalCredits == params.totalCredits &&
					cumulativeGPArequired == params.cumulativeGPArequired
		}
		List duplicateDegreeIds = []
		duplicateDegrees.each{
		//	println "duplicate degree with id of: " + it.id
			duplicateDegreeIds.add(it.id)
		}
    	if(duplicateDegreeIds.size() > 1){
    		flash.message = "WARNING! Duplicate degree was created. Degree with an id of ${degreeInstance.id} matches degree(s) ${duplicateDegreeIds}"			
		}
		else{
			flash.message = message(code: 'default.updated.message', args: [message(code: 'degree.label', default: 'Degree'), degreeInstance.id])
		}
    			redirect(action: "show", id: degreeInstance.id)
    }
	
    def updateConcentrations(String major) {
			def concentrations = degreebuilderService.getConcsFromBanner2(major)
			render concentrations as JSON
	}

    def updateDegree(){
		def degreeInstance = Degree.get(params.id)
		degreeInstance.updatedBy =  springSecurityService.getPrincipal().username
		degreeInstance.properties = params
		if (!degreeInstance.save(flush: true)) {
			def bannerColleges = degreebuilderService.getCollegesFromBanner()
			def bannerMajors = degreebuilderService.getMajorsFromBanner()
			def bannerCourses = degreebuilderService.getCoursesFromBanner()
			def bannerConcentrations = degreebuilderService.getConcsFromBanner()
			def degreeType = new DegreeType()			
			render(view: "edit",model:[degreeInstance: degreeInstance, bannerColleges:bannerColleges, bannerMajors:bannerMajors, bannerConcentrations:bannerConcentrations, bannerCourses:bannerCourses, degreeType:degreeType])
//			flash.message = message(code: 'default.updated.message', args: [message(code: 'degree.label', default: 'Save Unsuccessful'), degreeInstance.id])
			return
		}
		redirect(action: "palette", id: degreeInstance.id)
	}

	def updateMajors(String college) {
		def majors = degreebuilderService.getMajorsFromBanner(college)
		render majors as JSON
	}}
