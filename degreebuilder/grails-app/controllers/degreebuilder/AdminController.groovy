package degreebuilder

/**
 * AdminController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class AdminController {

	def index() {
		render(view: 'admin')
	}
	
	def listDegreeTypes(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		render(view: 'listDegreeTypes', model:[degreeTypeInstanceList: DegreeType.list(params).sort{it.code}, degreeTypeInstanceTotal: DegreeType.count()])
	}
	
	def createDegreeType(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		render(view: 'listDegreeTypes', model:[degreeTypeInstanceList: DegreeType.list(params).sort{it.code}, degreeTypeInstanceTotal: DegreeType.count()])
	}
	
	def editDegreeType(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		render(view: 'listDegreeTypes', model:[degreeTypeInstanceList: DegreeType.list(params).sort{it.code}, degreeTypeInstanceTotal: DegreeType.count()])
	}
	
	def listDegreeBlockTypes(Integer max) {
		params.max = Math.min(max ?: 10, 100)
		render(view: 'listDegreeBlockTypes', model:[degreeBlockTypeInstanceList: DegreeBlockType.list(params).sort{it.name}, degreeBlockTypeInstanceTotal: DegreeBlockType.count()])
	}
}
