package degreebuilder

import org.springframework.dao.DataIntegrityViolationException

/**
 * CourseBlockController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class CourseBlockController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def create() {
        [courseBlockInstance: new CourseBlock(params)]
    }

    def delete() {
        def courseBlockInstance = CourseBlock.get(params.id)
        if (!courseBlockInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "list")
            return
        }

        try {
            courseBlockInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "show", id: params.id)
        }
    }

    def edit() {
        def courseBlockInstance = CourseBlock.get(params.id)
        if (!courseBlockInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "list")
            return
        }

        [courseBlockInstance: courseBlockInstance]
    }

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [courseBlockInstanceList: CourseBlock.list(params), courseBlockInstanceTotal: CourseBlock.count()]
    }

    def save() {
        def courseBlockInstance = new CourseBlock(params)
        if (!courseBlockInstance.save(flush: true)) {
            render(view: "create", model: [courseBlockInstance: courseBlockInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), courseBlockInstance.id])
        redirect(action: "show", id: courseBlockInstance.id)
    }

    def show() {
        def courseBlockInstance = CourseBlock.get(params.id)
        if (!courseBlockInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "list")
            return
        }

        [courseBlockInstance: courseBlockInstance]
    }

    def update() {
        def courseBlockInstance = CourseBlock.get(params.id)
        if (!courseBlockInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (courseBlockInstance.version > version) {
                courseBlockInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'courseBlock.label', default: 'CourseBlock')] as Object[],
                          "Another user has updated this CourseBlock while you were editing")
                render(view: "edit", model: [courseBlockInstance: courseBlockInstance])
                return
            }
        }

        courseBlockInstance.properties = params

        if (!courseBlockInstance.save(flush: true)) {
            render(view: "edit", model: [courseBlockInstance: courseBlockInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'courseBlock.label', default: 'CourseBlock'), courseBlockInstance.id])
        redirect(action: "show", id: courseBlockInstance.id)
    }
}
