package degreebuilder

import org.springframework.dao.DataIntegrityViolationException
import grails.plugin.springsecurity.annotation.Secured;

/**
 * DegreeTypeController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
@Secured(['ROLE_UMT_IT_EIS_DEGREE_BUILDER_ROLE_DEGREE_BUILDER_ADMIN'])
class DegreeTypeController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
        redirect(action: "list", params: params)
    }

    def list() {
        params.max = Math.min(params.max ? params.int('max') : 10, 100)
        [degreeTypeInstanceList: DegreeType.list(params), degreeTypeInstanceTotal: DegreeType.count()]
    }

    def create() {
        [degreeTypeInstance: new DegreeType(params)]
    }

    def save() {
        def degreeTypeInstance = new DegreeType(params)
        if (!degreeTypeInstance.save(flush: true)) {
            render(view: "create", model: [degreeTypeInstance: degreeTypeInstance])
            return
        }

		flash.message = message(code: 'default.created.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), degreeTypeInstance.id])
        redirect(action: "show", id: degreeTypeInstance.id)
    }

    def show() {
        def degreeTypeInstance = DegreeType.get(params.id)
        if (!degreeTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "list")
            return
        }

        [degreeTypeInstance: degreeTypeInstance]
    }

    def edit() {
        def degreeTypeInstance = DegreeType.get(params.id)
        if (!degreeTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "list")
            return
        }

        [degreeTypeInstance: degreeTypeInstance]
    }

    def update() {
        def degreeTypeInstance = DegreeType.get(params.id)
        if (!degreeTypeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "list")
            return
        }

        if (params.version) {
            def version = params.version.toLong()
            if (degreeTypeInstance.version > version) {
                degreeTypeInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'degreeType.label', default: 'DegreeType')] as Object[],
                          "Another user has updated this DegreeType while you were editing")
                render(view: "edit", model: [degreeTypeInstance: degreeTypeInstance])
                return
            }
        }

        degreeTypeInstance.properties = params

        if (!degreeTypeInstance.save(flush: true)) {
            render(view: "edit", model: [degreeTypeInstance: degreeTypeInstance])
            return
        }

		flash.message = message(code: 'default.updated.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), degreeTypeInstance.id])
        redirect(action: "show", id: degreeTypeInstance.id)
    }

    def delete() {
        def degreeTypeInstance = DegreeType.get(params.id)
        if (!degreeTypeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "list")
            return
        }

        try {
            degreeTypeInstance.delete(flush: true)
			flash.message = message(code: 'default.deleted.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
			flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'degreeType.label', default: 'DegreeType'), params.id])
            redirect(action: "show", id: params.id)
        }
    }
}
