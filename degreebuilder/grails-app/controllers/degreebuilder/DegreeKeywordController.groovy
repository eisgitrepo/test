package degreebuilder

import org.springframework.dao.DataIntegrityViolationException

/**
 * DegreeKeywordController
 * A controller class handles incoming web requests and performs actions such as redirects, rendering views and so on.
 */
class DegreeKeywordController {
	def degreebuilderService
	
	static allowedMethods = [save: "POST", update: "POST", delete: "POST"]
	
    def index() {
        redirect(action: "list", params: params)
    }
	
	def list() {
		def degreeInstance = Degree.get(params.id)
		def degreeKeywordsSelected = DegreeKeyword.findAllWhere(degreeId:degreeInstance.id).keywordId

		if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
			redirect(action: "list")
			return
		}
		
		
//		params.max = Math.min(params.max ? params.int('max') : 10, 100)
		[keywordInstanceList: Keyword.list(params), keywordInstanceTotal: Keyword.count(),degreeInstance: degreeInstance,degreeKeywordsSelected:degreeKeywordsSelected]
	}
	
	def save(){
		degreebuilderService.saveDegreeKeyWords(params)
		redirect(action:"palette",controller:"degree", id: params.degree)	
	}
	
	
	def create() {
		def degreeInstance = Degree.get(params.id)
		//println "create"
		if (!degreeInstance) {
			flash.message = message(code: 'default.not.found.message', args: [message(code: 'degree.label', default: 'Degree'), params.id])
			redirect(action: "list")
			return
		}
		[degreeInstance: degreeInstance]
				redirect(action: "list", params: params)
	}
}


