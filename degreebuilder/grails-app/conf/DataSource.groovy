dataSource {
	pooled = true
	driverClassName = "oracle.jdbc.OracleDriver"
	username = "degreq_usr"
	password = "degreq!@#"
	logSql = false
	
	properties {
		maxActive = 20
		maxIdle = 15
		minIdle = 1
		initialSize = 1
		maxWait = 10000
		minEvictableIdleTimeMillis=1800000
		timeBetweenEvictionRunsMillis=1800000
		numTestsPerEvictionRun=3
		testOnBorrow=true
		testWhileIdle=true
		testOnReturn=true
   
		validationQuery = "select 1 from dual"

	}
}
//RLR - use this datasource definition for the in-memory h2 database
//dataSource {
//	pooled = true
//	driverClassName = "org.h2.Driver"
//	username = "sa"
//	password = ""
//}
hibernate {
	cache.use_second_level_cache = true
	cache.use_query_cache = false
	cache.region.factory_class = 'net.sf.ehcache.hibernate.EhCacheRegionFactory'
}
// environment specific settings
environments {
	development {
		dataSource {
			dbCreate = "" // one of 'create', 'create-drop', 'update', 'validate', '' //RLR - set to create-drop for in-memory h2 database
			url = "jdbc:oracle:thin:@griz02.umt.edu:7893:test11"
			//url = "jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred"
			//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000" //RLR - use this url for the in-memory h2 database
		}
	}
	test {
		dataSource {
			dbCreate = ""
			//url = "jdbc:h2:mem:devDb;MVCC=TRUE;LOCK_TIMEOUT=10000"
			url = "jdbc:oracle:thin:@griz02.umt.edu:7893:test11"
		}
	}
	production {
		dataSource {
			dbCreate = ""
			url = "jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred"

		}
	}
}
