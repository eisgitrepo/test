import degreebuilder.Course
import degreebuilder.CourseBlock
import degreebuilder.Degree
import degreebuilder.DegreeBlock
import degreebuilder.DegreeBlockType
import degreebuilder.DegreeType
import grails.converters.JSON
import java.text.DecimalFormat
import java.text.NumberFormat
//Added for shibboleth pre-authentication
import grails.plugin.springsecurity.SecurityFilterPosition
import grails.plugin.springsecurity.SpringSecurityUtils

class BootStrap {
	
	def degreebuilderService
    def init = { servletContext ->
		SpringSecurityUtils.clientRegisterFilter('shibAuthFilter', SecurityFilterPosition.PRE_AUTH_FILTER.order + 10)
		    JSON.registerObjectMarshaller(Degree){
			NumberFormat df = new DecimalFormat("0.0##")
			
			def returnArray = [:]
			returnArray['id'] = it.catalogId
			returnArray['originalId'] = it.id
			returnArray['college'] = it.college
			returnArray['degreeSubject'] = it.degreeSubject
			returnArray['catalogYear'] = it.catalogYear
			returnArray['level'] = it.level
			returnArray['degreeType'] = it.degreeType
			returnArray['degreeTrack'] = it.degreeTrack
			returnArray['totalCredits'] = it.totalCredits
			returnArray['cumulativeGPArequired'] = df.format(it.cumulativeGPArequired)
			returnArray['capstone'] = it.capstone
			returnArray['degreeComment'] = it.degreeComment?.trim()
			returnArray['degreeConcentration'] = it.degreeConcentration
			returnArray['degreeBlocks'] = it.degreeBlocks.sort()
			return returnArray
		}
		JSON.registerObjectMarshaller(DegreeBlock){
			Map courseBlockMap = [:]
			courseBlockMap['blockName'] = it.degreeBlockType.name
			courseBlockMap['blockComment'] = it.degreeBlockComment?.trim()
			
			if(it.courseBlocks.size() > 1){ 
				// Store first courseBlock as parent
				// Any subsequent courseBlocks are children of the parent courseBlock
				Map subcategoryMap = [:]
				ArrayList courseBlocks = []
				it.courseBlocks.each{
					courseBlocks.add(it)
				}
				courseBlocks = courseBlocks.sort{it.sortId}

				courseBlockMap['category'] = courseBlocks.first()
				
				//it.courseBlocks is a Hibernate PersistentSet, which can't be
				//modified, you have to copy it somewhere and modify the copy
				def tempArrayList = new ArrayList()
				courseBlocks.each { 
					tempArrayList.add(it)
				}
				tempArrayList.remove(0)
				//it.courseBlocks = it.courseBlocks.remove(it.courseBlocks.first())
				subcategoryMap['subcategory'] = tempArrayList
				//courseBlockMap.putAll(subcategoryMap)
				
				courseBlockMap.put('subcategories', tempArrayList)

			}
			else{
				courseBlockMap['category'] = it.courseBlocks
			}
			return courseBlockMap
		}
		JSON.registerObjectMarshaller(CourseBlock){
			def returnArray = [:]
			returnArray['name'] = it.name
			returnArray['rule'] = it.rule
			returnArray['courseBlockCredits'] = it.courseBlockCredits
			returnArray['eval'] = it.eval
			returnArray['prereqNotes'] = it.prereqNotes
			returnArray['courseBlockComment'] = it.courseBlockComment?.trim()
			returnArray['courses'] = it.courses.sort()
			return returnArray
		}
		JSON.registerObjectMarshaller(Course){
			def returnArray = [:]
			returnArray['courseSubject'] = it.courseSubject
			returnArray['courseNumber'] = it.courseNumber
			returnArray['title'] = it.title
			returnArray['courseCredits'] = it.courseCredits
			returnArray['term'] = it.term
			returnArray['description'] = it.description
			returnArray['courseYear'] = it.courseYear
			returnArray['attributes'] = degreebuilderService.getCourseAttributes(it.courseSubject, it.courseNumber)
			return returnArray
		}
	}
    def destroy = {
    }

}