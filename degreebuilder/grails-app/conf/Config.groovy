import grails.util.Environment;

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format
// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination
grails.mime.file.extensions = true // enables the parsing of file extensions from URLs into the request format
grails.mime.use.accept.header = false
grails.mime.types = [
    all:           '*/*',
    atom:          'application/atom+xml',
    css:           'text/css',
    csv:           'text/csv',
    form:          'application/x-www-form-urlencoded',
    html:          ['text/html','application/xhtml+xml'],
    js:            'text/javascript',
    json:          ['application/json', 'text/json'],
    multipartForm: 'multipart/form-data',
    rss:           'application/rss+xml',
    text:          'text/plain',
    xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// The default codec used to encode data with ${}
grails.views.default.codec = "none" // none, html, base64
grails.views.gsp.encoding = "UTF-8"
grails.converters.encoding = "UTF-8"
// add to handle production of output from jaxrs restful calls
// without these entries the full structure of the degree is not
// returned because hasMany associations are not handled
grails.converters.json.default.deep = false
grails.converters.xml.default.deep = false
grails.converters.default.circular.reference.behaviour = 'INSERT_NULL'
// enable Sitemesh preprocessing of GSP pages
grails.views.gsp.sitemesh.preprocess = true
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false
def appName = grails.util.Metadata.current.getApplicationName()

grails.util.Environment.executeForCurrentEnvironment {
	    development {
	        grails.logging.jul.usebridge = true
	        host {
				address = "http://" + java.net.InetAddress.getLocalHost().getHostAddress()
				port = ":8080/"
			}
	    }
		test {
			grails.logging.jul.usebridge = true
			host {
				address = "https://staging.umt.edu"
				port = "/"
			}
		}
	    production {
	        grails.logging.jul.usebridge = false
			host {
				address = "https://www.umt.edu"
				port = "/"
			}
	    }
}

def appUrl = host.address + host.port + appName

grails.resources.modules = {
	core {
		dependsOn 'jquery, utils'
	}
	paletteScript {
		dependsOn 'jquery'
		resource url:'/js/palette.js' , linkOverride: appUrl + '/static/js/palette.js'
	}
	dataTables {
		dependsOn 'jquery'
		resource url:'/js/jquery.datatables.js', linkOverride: appUrl + '/static/js/jquery.datatables.js'
	}
}

// log4j configuration
log4j = {
    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
           'org.codehaus.groovy.grails.web.pages',          // GSP
           'org.codehaus.groovy.grails.web.sitemesh',       // layouts
           'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
           'org.codehaus.groovy.grails.web.mapping',        // URL mapping
           'org.codehaus.groovy.grails.commons',            // core / classloading
           'org.codehaus.groovy.grails.plugins',            // plugins
           'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
           'org.springframework',
           'org.hibernate',
           'net.sf.ehcache.hibernate'
}

grails.config.defaults.locations = [KickstartResources]

//Spring Security Preauthentication/Shibboleth integration
grails.plugin.springsecurity.active = true
grails.plugin.springsecurity.providerNames = ['preauthAuthenticationProvider']
grails.plugin.springsecurity.securityConfigType = 'InterceptUrlMap'

//Control which areas of application require different roles from UserDetailsService
grails.plugin.springsecurity.interceptUrlMap = [
	'/ws/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
	'/api/**': ['IS_AUTHENTICATED_ANONYMOUSLY'],
	'/secure/receptor': ['IS_AUTHENTICATED_ANONYMOUSLY'],//used by the cas server
	'/**':['ROLE_UMT_IT_EIS_DEGREE_BUILDER_ROLE_DEGREE_BUILDER']
	]

if (grails.util.Environment.current == grails.util.Environment.DEVELOPMENT) {
	
    InetAddress addr = InetAddress.getLocalHost();
    String hostName = addr.getCanonicalHostName();
	  
	//Mac
	if (hostName == 'EIS310.gs.umt.edu') {
		 grails.plugin.springsecurity.logout.afterLogoutUrl = "https://eis-bannerlocaldev1.ito.umt.edu/Shibboleth.sso/Logout?return=https://logintest.umt.edu/idp/profile/Logout"
	}
	//Ron
	else if (hostName == 'EIS303A.gs.umt.edu') {
		 grails.plugin.springsecurity.logout.afterLogoutUrl = "https://eis-bannerlocaldev2.ito.umt.edu/Shibboleth.sso/Logout?return=https://logintest.umt.edu/idp/profile/Logout"
	}
	//Ty
	else if (hostName == 'EIS301B.gs.umt.edu') {
		 grails.plugin.springsecurity.logout.afterLogoutUrl = "https://eis-bannerlocaldev3.ito.umt.edu/Shibboleth.sso/Logout?return=https://logintest.umt.edu/idp/profile/Logout"
	}
	//Russ
	else if (hostName == 'EIS301A-1.gs.umt.edu') {
	     grails.plugin.springsecurity.logout.afterLogoutUrl = "https://eis-bannerlocaldev4.ito.umt.edu/Shibboleth.sso/Logout?return=https://logintest.umt.edu/idp/profile/Logout"
	}
	
	
} else
if (grails.util.Environment.current == grails.util.Environment.TEST) {
	
	grails.plugin.springsecurity.logout.afterLogoutUrl = "https://staging.umt.edu/Shibboleth.sso/Logout?return=https://logintest.umt.edu/idp/profile/Logout"

} else
if (grails.util.Environment.current == grails.util.Environment.PRODUCTION) {

	grails.plugin.springsecurity.logout.afterLogoutUrl = "https://www.umt.edu/Shibboleth.sso/Logout?return=https://login.umt.edu/idp/profile/Logout"
	
}

grails.plugin.springsecurity.logout.postOnly = false



// Uncomment and edit the following lines to start using Grails encoding & escaping improvements

/* remove this line 
// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside null
                scriptlet = 'none' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }
}
remove this line */
