// Place your Spring DSL code here
beans = {
	// Shibboleth integration
	
	//Custom User Details Service
	userDetailsService(degreebuilder.NetIdAuthoritiesPopulator)
	
	//Spring framework wrapper for custom service
	userDetailsServiceWrapper(org.springframework.security.core.userdetails.UserDetailsByNameServiceWrapper) {
        userDetailsService = ref('userDetailsService')
    }
    
	//Tells application to use preauthentication
    preauthAuthenticationProvider(org.springframework.security.web.authentication.preauth.PreAuthenticatedAuthenticationProvider) {
        preAuthenticatedUserDetailsService = ref('userDetailsServiceWrapper')
    }
    
	//Filter which uses the preauthentication provider
    shibAuthFilter(org.springframework.security.web.authentication.preauth.RequestHeaderAuthenticationFilter) {
        principalRequestHeader = 'uid' //this is the shib header that contains the user ID. Use 'uid' for full username, 'uidnoe' for no 'e' on end
        checkForPrincipalChanges = true
        invalidateSessionOnPrincipalChange = true
        continueFilterChainOnUnsuccessfulAuthentication = false
		exceptionIfHeaderMissing = false //allows access to API, in conjunction with apache server
        authenticationManager = ref('authenticationManager')
    }
}
