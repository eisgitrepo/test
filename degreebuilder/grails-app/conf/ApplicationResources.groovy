modules = {
    application {
        resource url:'js/application.js'
    }
	bootstrap {
		resource url:'less/bootstrap/bootstrap.less',attrs:[rel: "stylesheet/less", type:'css']
		dependsOn 'jquery'
	}
	bootstrap_utils {
		dependsOn 'jquery'
	}
	
}