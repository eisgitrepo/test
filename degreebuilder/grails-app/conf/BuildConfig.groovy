 grails.servlet.version = "2.5" // Change depending on target container compliance (2.5 or 3.0)
grails.project.class.dir = "target/classes"
grails.project.test.class.dir = "target/test-classes"
grails.project.test.reports.dir = "target/test-reports"
grails.project.target.level = 1.6
grails.project.source.level = 1.6
grails.server.host=java.net.InetAddress.getLocalHost().getHostAddress()
grails.project.dependency.resolution = {
    inherits("global") {
    }
    log "warn"
    checksums true
    legacyResolve false

    repositories {
        inherits true

        grailsPlugins()
        grailsHome()
        grailsCentral()

        mavenLocal()
        mavenCentral()
		mavenRepo "http://wts-maven.ito.umt.edu:8080/nexus/content/groups/public"
		mavenRepo "https://maven.atlassian.com/content/repositories/atlassian-public/"
		mavenRepo "http://maven.restlet.org/"
        mavenRepo "http://repo.grails.org/grails/repo"
//		mavenRepo "https://code.lds.org/nexus/content/groups/main-repo"
//        mavenRepo "http://artifactory.quirk.biz/artifactory/grails-plugins/"
    }
	/*
	 */
    dependencies {
		compile "edu.internet2:grouper-client:1.4.2"
		compile "oracle:ojdbc:10.2.0.2"
//		compile "com.atlassian.jira:jira-rest-java-client:2.0.0-m2"
//		compile 'com.oracle:ojdbc6:11.2.0.3'
    }

    plugins {
        compile ":hibernate:3.6.10.2"
        runtime ":jquery:1.8.3"
        runtime ":resources:1.1.6"

        build ":tomcat:7.0.42"

        runtime ":database-migration:1.3.2"

        compile ':cache:1.0.1'
		
		compile(':kickstart-with-bootstrap:0.9.6')
		
		compile ':spring-security-core:2.0-RC2'
		
		compile ':jaxrs:0.10'
					
    }	
}