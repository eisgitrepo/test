package degreebuilder

/**
 * Block
 * A domain class describes the data object and it's mapping to the database
 */
class CourseBlock {

	Long   id
	Long   sortId
	Long   version	
    Date   dateCreated
	Date   lastUpdated	
	String name
	String rule
	String courseBlockCredits
	String eval
	String prereqNotes
	String courseBlockComment = " "
	
	static hasMany = [courses: Course] // tells GORM to associate other domain objects for a 1-n mapping
	static belongsTo = [degreeBlock: DegreeBlock] // tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
	
	
	static mapping = {
		sort "id"
	    version false
	    table 'course_block'
	    columns{
			   id column: 'id'
			   sortId column: 'sort_id'
			   name column: 'name'
			   rule column: 'course_block_rule'
			   courseBlockCredits column: 'credits'
			   eval column: 'eval'
			   prereqNotes column: 'prereq_notes'
			   courseBlockComment column: 'course_comment'
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	   }
	   id generator:'sequence', params:[sequence:'course_block_id_sequence']
	   courses sort: "courseSubject", lazy: false
	}
		   
	static constraints = {
		name(nullable: true, maxSize: 500)
		rule(nullable: true, maxSize: 500 )
		courseBlockCredits(nullable: true, maxSize: 10 )
		eval(nullable: true, maxSize: 4)
		prereqNotes(nullable: true, maxSize: 2000)
		courseBlockComment(nullable: true, maxSize: 2000)
		dateCreated(nullable: true)
		lastUpdated(nullable: true)
		sortId(nullable:true)
	}
	
//	static {
//		grails.converters.JSON.registerObjectMarshaller(CourseBlock){
//			return it.properties.findAll {k,v -> k != degreeBlock}
//		}
//	}
//	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${name}";
	}
}
