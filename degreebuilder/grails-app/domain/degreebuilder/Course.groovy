package degreebuilder

/**
 * Course
 * A domain class describes the data object and it's mapping to the database
 */

class Course implements Comparable {

	/* Default (injected) attributes of GORM */
	Long	id
	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	String courseSubject
	String courseNumber
	String title
	String courseCredits
	String term
	String courseYear
	String description
	
	static transients = ['description']
	
	static belongsTo = [courseBlock:CourseBlock] // tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
	
	static mapping = {
	   sort "id"
	   version false
	   table 'course'
	   columns{
			   id column: 'id'
			   courseSubject column: 'subject_code'
			   courseNumber column: 'course_number'
			   title column: 'title'
			   courseCredits column: 'credits'
			   term column: 'term'
			   courseYear column: 'course_year'
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	   }
	   id generator:'sequence', params:[sequence:'course_id_sequence']
	}
			  
	static constraints = {
		courseSubject(nullable: true, maxSize: 5 )
		courseNumber(nullable: true, maxSize: 5 )
		title(nullable: true, maxSize: 80)
		courseCredits(nullable: true, maxSize: 8)
		term(nullable: true, maxSize: 40)
		courseYear(nullable: true, maxSize: 20)
		dateCreated(nullable: true)
		lastUpdated(nullable: true)
	}
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${courseSubject} ${courseNumber}";
	}
	
	int compareTo(otherCourse){
		if (courseSubject > otherCourse.courseSubject){
			return 1
		}
		else if (courseSubject < otherCourse.courseSubject){
			return -1
		}
		else{
			if (courseNumber > otherCourse.courseNumber){
				return 1
			}
			else if (courseNumber < otherCourse.courseNumber){
				return -1
			}
			else{
				return 0
			}
		}
	}
}
