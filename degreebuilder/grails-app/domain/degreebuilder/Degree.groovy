package degreebuilder
import grails.rest.*

/**
 * Degree
 * A domain class describes the data object and it's mapping to the database
 */
@Resource
class Degree {

	/* Default (injected) attributes of GORM */
	Long	id
	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	String college
	String degreeSubject
	String catalogYear
	String level
	String degreeType
	Long   totalCredits
	Double cumulativeGPArequired	
	String capstone
	String degreeComment = " "
	String degreeConcentration
	String updatedBy
	String degreeTrack
	String degreeApproved
    Long   catalogId
	
	static hasMany = [degreeBlocks: DegreeBlock, keywords:Keyword]	// tells GORM to associate other domain objects for a 1-n mapping
 
	static mapping = {
	   sort "id"
	   version false
	   table 'degree'
	   columns{
			   id column: 'id'
			   college column: 'college'
			   degreeConcentration column: 'concentration'
			   degreeSubject column: 'subject'
			   catalogYear column: 'catalog_year'
			   level column: 'degree_level'
			   degreeType column: 'type'
			   totalCredits column: 'total_credits'
			   cumulativeGPArequired column: 'cume_gpa_req'
			   capstone column: 'capstone'
			   degreeComment column: 'degree_comment'				   
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
			   updatedBy column: 'updated_by'
			   degreeTrack column: 'track'
			   degreeApproved column: 'degree_approved'
               catalogId column: 'catalog_id'
	   }
	   id generator:'sequence', params:[sequence:'degree_id_sequence']
           keywords joinTable:[name:"degree_keyword",key:'degree_id']
	   	   degreeBlocks lazy: true
	}   
	   	   
	static constraints = {
		college(nullable: true, maxSize: 40)
		degreeSubject(nullable: true, maxSize: 60 )
		catalogYear(nullable: true, maxSize: 9 )
		level(nullable: true, maxSize: 30)
		degreeType(nullable: true, maxSize: 50)
		totalCredits(nullable: true, maxSize: 3)
		cumulativeGPArequired(nullable: true, maxSize: 3)		
		capstone(nullable: true, maxSize: 30)
		degreeComment(nullable: true, maxSize: 2000)
		degreeConcentration(nullable:true, maxSize: 30)
		lastUpdated(nullable: true)
		updatedBy(nullable: true)
		dateCreated(nullable: true)
		degreeTrack(nullable:true)
		degreeApproved(nullable:true, maxSize: 1)
        catalogId(nullable: true)
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "${college}";
	}
	
	def getDegreeBlockTypes() {
		return degreeBlocks.collect {"${it.degreeBlockType.name}"}
	}
	def getNumberCourseBlocks(){
		int courseBlockCount = 0
		degreeBlocks.each{it ->
			courseBlockCount += it.courseBlocks.size()
		}
		return courseBlockCount
	}
}
