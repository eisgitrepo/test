package degreebuilder

import java.util.Date;

/**
 * Keyword
 * A domain class describes the data object and it's mapping to the database
 */

class Keyword {

	/* Default (injected) attributes of GORM */
	Long	id
	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	String value
	
	static hasMany = [degrees:Degree]
	static belongsTo = Degree 
	
	static mapping = {
	   sort "id"
	   version false
	   table 'keyword'
	   columns{
			   id column: 'id'
			   value column: 'value'

			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	   }
	   id generator:'sequence', params:[sequence:'keyword_id_sequence']

	   degrees joinTable:[name:"degree_keyword",key:'keyword_id']
   	}
			  
	static constraints = {
		value(nullable: true, maxSize: 200)
		dateCreated(nullable: false)
		lastUpdated(nullable: true)
	}
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		return "${value}";
	}
}	
