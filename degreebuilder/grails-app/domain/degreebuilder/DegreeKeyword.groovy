package degreebuilder

/**
 * DegreeKeyword
 * A domain class describes the data object and it's mapping to the database
 */
class DegreeKeyword {

	/* Default (injected) attributes of GORM */
	Long	id
	Long	version
	Long	degreeId
	Long	keywordId
	String  updatedBy
	
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
//	static belongsTo	= []	// tells GORM to cascade commands: e.g., delete this object if the "parent" is deleted.
//	static hasOne		= []	// tells GORM to associate another domain object as an owner in a 1-1 mapping
//	static hasMany		= []	// tells GORM to associate other domain objects for a 1-n or n-m mapping
//	static mappedBy		= []	// specifies which property should be used in a mapping 
	
    static mapping = {
		sort "id"
		version false
		table 'degree_keyword'
		columns{
				id column: 'id'
				version column: 'version'
				degreeId column: 'degree_id'
				keywordId column: 'keyword_id'
				updatedBy column: 'updated_by'
				dateCreated column: 'date_created'
				lastUpdated column: 'last_updated'
		}
		id generator:'sequence', params:[sequence:'degree_keyword_id_sequence']
    }
    
	static constraints = {
		degreeId(nullable: true)
		keywordId(nullable: true)
		updatedBy(nullable: true)
		dateCreated(nullable: true)
		lastUpdated(nullable: true)
	 }
	
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${name}";
//	}
}
