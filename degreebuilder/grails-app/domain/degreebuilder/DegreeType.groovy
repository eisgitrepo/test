package degreebuilder

/**
 * DegreeTYPE
 * A domain class describes the data object and it's mapping to the database
 */
class DegreeType {

	Long	id
	Long	version
	String  code
    String  desc
	Date	dateCreated
	Date	lastUpdated
	
	static mapping = {
	sort "code"
	   version false
	   table 'degree_type'
	   columns{
			   code column: 'code'
			   desc column: 'description'
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	   }
	
	   id generator:'sequence', params:[sequence:'course_id_sequence']
	}
			  
	static constraints = {
		code(nullable: true, maxsize: 8)
		desc(nullable: true, maxSize: 80)
		dateCreated(nullable: true)
		lastUpdated(nullable: true)
	}
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI
	public String toString() {
		return "${code}";
	}
}