package degreebuilder

/**
 * DegreeBlockType
 * An instance represents one of the degree block types
 */
class DegreeBlockType implements Comparable {

	/* Default (injected) attributes of GORM */
	Long			id
	Long			version
	String			name
	String			displayGroup
	int				displaySeqno
	def 			degreeBlockTypeGroups = ['Core Courses': 1, 'Degree Electives': 2, 'Degree Other': 3, 'Degree Specific Gen Ed Courses': 4]
	static transients = ['degreeBlockTypeGroups']
	
	static hasMany = [degreeBlock: DegreeBlock]
	
	
	int getSeqnoPlusOne(){
		return displaySeqno + 1
	}
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated
	
	static mapping = {
		sort "displayGroup"
		version false
		table 'degree_block_type'
		columns{
			   id column: 'id'
			   name column: 'name'
			   displayGroup column: 'display_group'
			   displaySeqno column: 'display_seqno'
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	    }
		id generator:'sequence', params:[sequence:'degree_block_type_id_sequence']
	}
    
	static constraints = { 
		name(nullable: false, unique: true, maxSize: 60)
		displayGroup(nullable: false) 
		displaySeqno(nullable: false, unique: 'displayGroup', maxSize: 3, min: 1)
		dateCreated(nullable: true)
		lastUpdated(nullable: true)
    }
	
	/*
	 * Methods of the Domain Class
	 */
	@Override	// Override toString for a nicer / more descriptive UI 
	public String toString() {
		return "Block Type: ${name} Group: ${displayGroup} Seqno: ${displaySeqno} Date Created: ${dateCreated} Date Last Updated: ${lastUpdated}";
	}

	int compareTo(otherDegreeBlockType){
		if (getGroupSeqno(displayGroup) > getGroupSeqno(otherDegreeBlockType.displayGroup)){
			return 1
		}
		else if (getGroupSeqno(displayGroup) < getGroupSeqno(otherDegreeBlockType.displayGroup)){
			return -1
		}
		else{
			if (displaySeqno > otherDegreeBlockType.displaySeqno){
				return 1
			}
			else if (displaySeqno < otherDegreeBlockType.displaySeqno){
				return -1
			}
			else{
				return 0
			}
		}
	}
	
	int getGroupSeqno(String displayGroup) {
		int retval = degreeBlockTypeGroups[displayGroup]
		return retval
	}

}