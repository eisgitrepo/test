package degreebuilder

/**
 * DegreeBlock
 * A domain class describes the data object and it's mapping to the database
 */
class DegreeBlock implements Comparable {

	/* Default (injected) attributes of GORM */
	Long	id
	Long    sortId
	Long	version
	
	/* Automatic timestamping of GORM */
	Date	dateCreated
	Date	lastUpdated	
	String 	degreeBlockComment = " "
	
	static  hasMany	 = [courseBlocks: CourseBlock]	// TELLS GORM TO ASSOCIATE OTHER DOMAIN OBJECTS FOR A 1-N MAPPING
	static  belongsTo = [degree: Degree] // TELLS GORM TO CASCADE COMMANDS: E.G., DELETE THIS OBJECT IF THE "PARENT" IS DELETED.
	static  hasOne = [degreeBlockType: DegreeBlockType]
	
	static mapping = {
		courseBlocks sort:'id',order:'asc'
		sort "id"
	    version false
	    table 'degree_block'
	    columns{
			   id column: 'id'
			   sortId column: 'sort_id'
			   degreeBlockComment column: 'block_comment'
			   dateCreated column: 'date_created'
			   lastUpdated column: 'last_updated'
	   }
		id generator:'sequence', params:[sequence:'degree_block_id_sequence']
		courseBlocks lazy: false
	} 
	     
	 static constraints = {
	     degreeBlockComment(nullable: true, maxSize: 2000)
		 lastUpdated(nullable: true)
		 dateCreated(nullable: true)
		 sortId(nullable: true)
	   }
	   
	/*
	 * Methods of the Domain Class
	 */
//	@Override	// Override toString for a nicer / more descriptive UI 
//	public String toString() {
//		return "${degreeBlockType} Comment: ${degreeBlockComment} Display Seqno: ${displaySeqno} Date Created: ${dateCreated} Date Last Updated: ${lastUpdated}";
//	}

//	 int compareTo(otherDegreeBlock){
//		 if (degreeBlockType > otherDegreeBlock.degreeBlockType) {
//			 return 1
//		 }
//		 else if (degreeBlockType < otherDegreeBlock.degreeBlockType) {
//			 return -1
//		 }
//		 else {
//			 return 0
//		 }
//	 }
// }
	 	int compareTo(otherDegreeBlock){
		if (degreeBlockType > otherDegreeBlock.degreeBlockType) {
				return 1
		}
		else if (degreeBlockType < otherDegreeBlock.degreeBlockType) {
				return -1
		}
		else {
			if (sortId > otherDegreeBlock.sortId){
				return 1
			}
			else if (sortId < otherDegreeBlock.sortId){
				return -1
			}
		}
	}
}	