package degreebuilder

import static org.grails.jaxrs.response.Responses.*

import javax.ws.rs.Consumes
import javax.ws.rs.GET
import javax.ws.rs.HEAD
import javax.ws.rs.Produces
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.POST
import javax.ws.rs.core.Response

//Hi Ron

@Path('/api')
@Consumes(['application/json'])
@Produces(['application/json'])
class DegreeCollectionResource {

    def degreeResourceService
	// OK - DATA
	@GET
	Response readAll() {
		ok degreeResourceService.getYears()
	}
	@GET
	@Path('/{year}/degrees')
	Response  readAllDegrees(@PathParam('year') String year){
		ok degreeResourceService.readDegreesByCatalogYear(year)
	}
	
	

	@GET
	@Path('/{year}')
	Response getCollegesByCatalogYear(@PathParam('year') String year) {
		ok degreeResourceService.readCollegesByCatalogYear(year)
	}
	
	@GET
	@Path('/{year}/{college}')
	Response getDegreesByCatalogYearCollege(
			@PathParam('year') String year,
			@PathParam('college') String college
	) {
//		ok degreeResourceService.readDepartmentsByCatalogYearAndCollege(year, college)
		ok degreeResourceService.readDegreesByCatalogYearAndCollege(year, college)
	}

	@GET
	@Path('/{year}/{college}/{department}')
	Response getDegreesByCatalogYearCollegeDepartment(
			@PathParam('year') String year,
			@PathParam('college') String college,
			@PathParam('department') String department
	) {
		ok degreeResourceService.readDegreesByCatalogYearCollegeAndDepartment(year, college, department)
	}

	@GET
	@Path('/law')
	Response getLawCourses(
	) {
		ok degreeResourceService.readLawCourses()
	}

//	@GET
	@Path('/{year}/{college}/{department}/{id}')
	DegreeResource getDegreeByYearCollegeDepartmentId(
		@PathParam('year') String year,
		@PathParam('college') String college,
		@PathParam('department') String department,
		@PathParam('id') Long id) {
		new DegreeResource(degreeResourceService: degreeResourceService, id:id)
	}

//	@GET
	@Path('/degree/{catalogYear}/{catalogId : .+}')
	DegreeResource Resource(
            @PathParam('catalogId') String catalogId,
            @PathParam('catalogYear') String catalogYear) {
		List<Long> catalogIdList = Arrays.asList(catalogId.split("\\|"))
//		new DegreeResource(degreeResourceService: degreeResourceService, id:id)
		new DegreeResource(degreeResourceService: degreeResourceService, catalogId:catalogIdList,catalogYear:catalogYear)
//		ok degreeResourceService.readById(idList)
	}
	
//	@GET
//	@Path('/search/{word : .+}')
//	Response readDegreesByKeyword(@PathParam('word') String word){
//		List<String> WordList = Arrays.asList(word.split("\\|"))
//		ok degreeResourceService.readDegreesByWord(WordList)
//	}
	
	@GET
	@Path('/search/{catalogYear}/{word : .+}')
	Response readDegreesByCatalogYearAndKeyword(
			@PathParam('word') String word,
			@PathParam('catalogYear') String catalogYear){
		List<String> WordList = Arrays.asList(word.split("\\|"))
				ok degreeResourceService.readDegreesByCatalogYearAndWord(catalogYear,WordList)
	}
			
	@GET
	@Path('/{subjectCode}/{courseNumber}/desc')
	Response getCourseDescription(
			@PathParam('subjectCode') String subjectCode,
			@PathParam('courseNumber') String courseNumber
	) {
		ok degreeResourceService.getCourseDescription(subjectCode, courseNumber)
	}
		
	@GET
	@Path('/desc/{courseIds : .+}')
	Response getCourseDescriptions(
			@PathParam('courseIds') String courseIds) {
		List<String> courseIdsList = Arrays.asList(courseIds.split("\\|"))
		return Response
				.status(200)
				.header("Access-Control-Allow-Origin", "*")
				.entity(degreeResourceService.getCourseDescriptions(courseIdsList))
				.build();
		//ok degreeResourceService.getCourseDescriptions(courseIdsList)
	}			
			
}
