package degreebuilder

import org.apache.commons.lang.StringUtils;
import org.apache.jasper.compiler.Node.ParamsAction;
import org.apache.tomcat.util.http.Parameters

import grails.converters.JSON;
import groovy.sql.Sql

import org.codehaus.groovy.grails.web.json.JSONObject
/**
 * DegreebuilderService
 * A service class encapsulates the core business logic of a Grails application
 */
class DegreebuilderService {
	def grailsApplication
	def springSecurityService
	def dataSource
	def stvmajrs
	static transactional = true
	
		def getCollegesFromBanner() {
			def sqlStatement = "select stvcoll_code, stvcoll_desc from stvcoll where stvcoll_system_req_ind is null order by stvcoll_desc"
			def results = getRows(sqlStatement)
			return results
		}
		
		def getConcsFromBanner() {
			def sqlStatement = "select distinct y.stvmajr_desc from sobcurr, stvcoll, smrprle, sorcmjr a, stvmajr x, stvmajr y, sorccon where sobcurr.sobcurr_curr_rule = a.sorcmjr_curr_rule and sobcurr.sobcurr_coll_code = stvcoll_code and sobcurr.sobcurr_program = smrprle_program and a.sorcmjr_majr_code = x.stvmajr_code and sorccon_cmjr_rule = a.sorcmjr_cmjr_rule and sorccon_majr_code_conc = y.stvmajr_code and a.sorcmjr_adm_ind = 'Y' and a.sorcmjr_term_code_eff = (select max(b.sorcmjr_term_code_eff) from sorcmjr b where a.sorcmjr_curr_rule = b.sorcmjr_curr_rule and a.sorcmjr_majr_code = b.sorcmjr_majr_code) order by 1"
			return getRows(sqlStatement).collect {
				"${it.STVMAJR_DESC}"
			}
		}
		
		def getConcsFromBanner2(String major) {
			def majorCode = stvmajrs.find{it.STVMAJR_DESC == major}?.STVMAJR_CODE

//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			return sql.rows("select distinct y.stvmajr_desc from sobcurr, stvcoll, smrprle, sorcmjr a, stvmajr y, sorccon"+
							" where sobcurr.sobcurr_curr_rule = a.sorcmjr_curr_rule and sobcurr.sobcurr_coll_code = stvcoll_code"+
							" and sobcurr.sobcurr_program = smrprle_program and a.sorcmjr_majr_code = ?"+
							" and sorccon_cmjr_rule = a.sorcmjr_cmjr_rule and sorccon_majr_code_conc = y.stvmajr_code and a.sorcmjr_adm_ind = 'Y'"+
							" and a.sorcmjr_term_code_eff = (select max(b.sorcmjr_term_code_eff) from sorcmjr b where a.sorcmjr_curr_rule ="+
							" b.sorcmjr_curr_rule and a.sorcmjr_majr_code = b.sorcmjr_majr_code) order by 1", [majorCode]).collect {

				"${it.STVMAJR_DESC}"
			}
		}
		
		def getCoursesFromBanner() {
            def sqlStatement = """select a.scbcrse_subj_code,
                                         a.scbcrse_crse_numb,
                                         a.scbcrse_title,
                                         a.scbcrse_credit_hr_low,
                                         a.scbcrse_credit_hr_ind,
                                         a.scbcrse_credit_hr_high, 
                                         dbms_lob.substr(c.scbdesc_text_narrative,4000,1) scbdesc_text_narrative 
                                    from scbcrse a, scbcrky, scbdesc c 
                                   where a.scbcrse_subj_code = scbcrky.scbcrky_subj_code 
                                     and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb 
                                     and scbcrky.scbcrky_term_code_end = '999999' 
                                     and a.scbcrse_subj_code <> '-' 
                                     and a.scbcrse_eff_term = (select max(b.scbcrse_eff_term) 
                                                                 from scbcrse b 
                                                                where b.scbcrse_subj_code = a.scbcrse_subj_code 
                                                                  and b.scbcrse_crse_numb = a.scbcrse_crse_numb) 
                                     and a.scbcrse_crse_numb not like 'T%' 
                                     and c.scbdesc_subj_code = a.scbcrse_subj_code 
                                     and c.scbdesc_crse_numb = a.scbcrse_crse_numb 
                                     and c.scbdesc_term_code_eff = (select max(b.scbdesc_term_code_eff) 
                                                                      from scbdesc b 
                                                                     where c.scbdesc_subj_code = b.scbdesc_subj_code 
                                                                       and c.scbdesc_crse_numb = b.scbdesc_crse_numb)
                                   order by 1, 2"""
			return getRows(sqlStatement)
		}

		def getLawCoursesFromBanner() {
			def sqlStatement = """select  a.scbcrse_subj_code,
									       a.scbcrse_crse_numb,
									       a.scbcrse_title,
									       a.scbcrse_credit_hr_low,
									       a.scbcrse_credit_hr_ind,
									       a.scbcrse_credit_hr_high,
									       a.scbcrse_eff_term 
									  from scbcrse a, scbcrky 
									 where a.scbcrse_subj_code = 'LAW'
									   and a.scbcrse_subj_code <> '-'
									   and a.scbcrse_crse_numb not like 'T%'
									   and scbcrky.scbcrky_term_code_end = '999999'
									   and a.scbcrse_subj_code = scbcrky.scbcrky_subj_code
									   and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb
									   and a.scbcrse_eff_term =
									       (select max(b.scbcrse_eff_term)
									          from scbcrse b
									         where b.scbcrse_subj_code = a.scbcrse_subj_code
									           and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
									 order by 1, 2"""
			return getRows(sqlStatement)
		}
				
		def getDegreeIdsByWord(word){
			def sql = new Sql(dataSource)
//			return sql.rows("select distinct degree.id from degree,degree_keyword, keyword where degree.id = degree_keyword.degree_id and degree_keyword.keyword_id = keyword.id and keyword.value = ?", [keyword]).collect{"${it.id}"}			
			String sqlStatement = """select id from degree
							   where lower(subject) like lower('%$word%')
                               or  lower(track) like lower('%$word%')
                               or  lower(concentration) like lower('%$word%')
                               union
                               select degree_id from degree_keyword 
                               where keyword_id in (select id from keyword
                               where lower(value) like lower('%$word%'))"""
			return sql.rows(sqlStatement).collect{"${it.id}"}			
		}
		
		def getDegreeIdsByCatalogYearAndWord(catalogYear,word){
			def sql = new Sql(dataSource)
//			return sql.rows("select distinct degree.id from degree,degree_keyword, keyword where degree.id = degree_keyword.degree_id and degree_keyword.keyword_id = keyword.id and keyword.value = ?", [keyword]).collect{"${it.id}"}			
			String sqlStatement = """select id from degree
			where catalog_year = '$catalogYear' 
			and (lower(subject) like lower('%$word%')
			or  lower(track) like lower('%$word%')
			or  lower(concentration) like lower('%$word%'))
			union
			select degree_id from degree_keyword, degree 
			where keyword_id in (select id from keyword
			where lower(value) like lower('%$word%'))
			and degree_keyword.degree_id = degree.id
			and degree.catalog_year = '$catalogYear'"""
			return sql.rows(sqlStatement).collect{"${it.id}"}			
		}
		
		def getDegreeCourseDescriptions(Degree degree) {
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			degree.degreeBlocks.each() {
						 it.courseBlocks.each() {
								it.courses.each(){
									   it.description = sql.rows("select cast(a.scbdesc_text_narrative as varchar2(3999)) scbdesc_text_narrative from scbdesc a where  a.scbdesc_subj_code = ? and a.scbdesc_crse_numb = ? and a.scbdesc_term_code_eff = (select max(b.scbdesc_term_code_eff) from scbdesc b       where a.scbdesc_subj_code = b.scbdesc_subj_code and   a.scbdesc_crse_numb = b.scbdesc_crse_numb)",[it.courseSubject,it.courseNumber]).collect {
											  "${it.scbdesc_text_narrative}"
									   }
									   
									   it.description = it.description.substring(1, it.description.length()-1)
								}
								
						 }
				   
			}

		}
		
		def getCourseDescription(String subjectCode, String courseNumber) {
			//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			return sql.rows("""
    select c.scbdesc_subj_code,
                 c.scbdesc_crse_numb,
                 nvl((select scrsyln_long_course_title
                 from scrsyln
                where scrsyln_subj_code = c.scbdesc_subj_code
                  and scrsyln_crse_numb = c.scbdesc_crse_numb),
                  a.scbcrse_title) title,
                 cast(c.scbdesc_text_narrative as varchar2(3999)) descr,
                 decode(a.scbcrse_credit_hr_ind,
                        'TO',
                        a.scbcrse_credit_hr_low || ' ' || a.scbcrse_credit_hr_ind || ' ' ||
                        a.scbcrse_credit_hr_high,
                        'OR',
                        a.scbcrse_credit_hr_high,
                        a.scbcrse_credit_hr_low) credits
            from scbcrse a, scbcrky, scbdesc c, stvsubj d
          where a.scbcrse_subj_code = scbcrky.scbcrky_subj_code
             and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb
             and scbcrky.scbcrky_term_code_end = '999999'
             and a.scbcrse_subj_code <> '-'
             and a.scbcrse_eff_term =
                 (select max(b.scbcrse_eff_term)
                    from scbcrse b
                   where b.scbcrse_subj_code = a.scbcrse_subj_code
                     and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
             and a.scbcrse_crse_numb not like 'T%'
             and c.scbdesc_subj_code = a.scbcrse_subj_code
             and c.scbdesc_crse_numb = a.scbcrse_crse_numb
            and c.scbdesc_subj_code = ?
             and c.scbdesc_crse_numb = ?
             and c.scbdesc_term_code_eff =
                 (select max(b.scbdesc_term_code_eff)
                    from scbdesc b
                   where c.scbdesc_subj_code = b.scbdesc_subj_code
                     and c.scbdesc_crse_numb = b.scbdesc_crse_numb)
             and d.stvsubj_code(+) = a.scbcrse_subj_code
          order by 1, 2
			""",
			[subjectCode, courseNumber]
			)
												   
			//description = description.substring(1, description.length()-1)
		}
		
		def getCoursesFromBannerByDept(String department){
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			return sql.rows(
				"""select  c.scbdesc_subj_code,
						   c.scbdesc_crse_numb,
						   a.scbcrse_title,
					       cast(c.scbdesc_text_narrative as varchar2(3999)) scbdesc_text_narrative
					  from scbcrse a, scbcrky, scbdesc c
					 where a.scbcrse_subj_code = scbcrky.scbcrky_subj_code
					   and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb
					   and scbcrky.scbcrky_term_code_end = '999999'
					   and a.scbcrse_subj_code <> '-'
					   and a.scbcrse_eff_term =
					       (select max(b.scbcrse_eff_term)
					          from scbcrse b
					         where b.scbcrse_subj_code = a.scbcrse_subj_code
					           and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
					   and a.scbcrse_crse_numb not like 'T%'
					   and c.scbdesc_subj_code = a.scbcrse_subj_code
					   and c.scbdesc_crse_numb = a.scbcrse_crse_numb
					   and a.scbcrse_dept_code = ?
					   and c.scbdesc_term_code_eff =
					       (select max(b.scbdesc_term_code_eff)
					          from scbdesc b
					         where c.scbdesc_subj_code = b.scbdesc_subj_code
					           and c.scbdesc_crse_numb = b.scbdesc_crse_numb)
					 order by 1, 2""",[department]
				)
			
		}
		
		def getCoursesFromBannerByCollegeAndDept(String college, String department){
			def sql = new Sql(dataSource)
//			nvl((select scrsyln_long_course_title
//				from scrsyln
//			   where scrsyln_subj_code = c.scbdesc_subj_code
//				 and scrsyln_crse_numb = c.scbdesc_crse_numb),
//				   a.scbcrse_title) scbcrse_title,
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "jupiter", "eiscanh3lp", "oracle.jdbc.OracleDriver")
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			return sql.rows(
				"""
					select c.scbdesc_subj_code,
					       c.scbdesc_crse_numb,
					      nvl((select scrsyln_long_course_title
             from scrsyln 
            where scrsyln_term_code_eff =
                 (select max(b.scrlevl_eff_term)
                    from scrlevl b
                   where b.scrlevl_subj_code = scbcrse_subj_code
                     and b.scrlevl_crse_numb = scbcrse_crse_numb)
              and scrsyln_subj_code = c.scbdesc_subj_code
              and scrsyln_crse_numb = c.scbdesc_crse_numb),
              a.scbcrse_title) scbcrse_title,  
					       cast(c.scbdesc_text_narrative as varchar2(3999)) scbdesc_text_narrative,
					       d.stvsubj_desc,
					       decode(a.scbcrse_credit_hr_ind,
					              'TO',
					              a.scbcrse_credit_hr_low || ' ' || a.scbcrse_credit_hr_ind || ' ' ||
					              a.scbcrse_credit_hr_high,
					              'OR',
					              a.scbcrse_credit_hr_high,
					              a.scbcrse_credit_hr_low) credits,
					       (select 'UG'
					          from scrlevl a
					         where a.scrlevl_subj_code = scbcrse_subj_code
					           and a.scrlevl_crse_numb = scbcrse_crse_numb
					           and a.scrlevl_eff_term =
					               (select max(b.scrlevl_eff_term)
					                  from scrlevl b
					                 where b.scrlevl_subj_code = a.scrlevl_subj_code
					                   and b.scrlevl_crse_numb = a.scrlevl_crse_numb)
					           and a.scrlevl_levl_code = '01') undergrad,
					       (select 'G'
					          from scrlevl a
					         where a.scrlevl_subj_code = scbcrse_subj_code
					           and a.scrlevl_crse_numb = scbcrse_crse_numb
					           and a.scrlevl_eff_term =
					               (select max(b.scrlevl_eff_term)
					                  from scrlevl b
					                 where b.scrlevl_subj_code = a.scrlevl_subj_code
					                   and b.scrlevl_crse_numb = a.scrlevl_crse_numb)
					           and a.scrlevl_levl_code = '02') grad
					  from scbcrse a, scbcrky, scbdesc c, stvsubj d
					where a.scbcrse_subj_code = scbcrky.scbcrky_subj_code
					   and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb
					   and scbcrky.scbcrky_term_code_end = '999999'
					   and a.scbcrse_subj_code <> '-'
					   and a.scbcrse_eff_term =
					       (select max(b.scbcrse_eff_term)
					          from scbcrse b
					         where b.scbcrse_subj_code = a.scbcrse_subj_code
					           and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
					   and a.scbcrse_crse_numb not like 'T%'
					   and c.scbdesc_subj_code = a.scbcrse_subj_code
					   and c.scbdesc_crse_numb = a.scbcrse_crse_numb
					   and a.scbcrse_dept_code = ?
					   and a.scbcrse_coll_code = ?
					   and c.scbdesc_term_code_eff =
					       (select max(b.scbdesc_term_code_eff)
					          from scbdesc b
					         where c.scbdesc_subj_code = b.scbdesc_subj_code
					           and c.scbdesc_crse_numb = b.scbdesc_crse_numb)
					   and d.stvsubj_code(+) = a.scbcrse_subj_code
					order by 1, 2

				""",[department,  college]
				)
			
		}
		
//		select  c.scbdesc_subj_code,
//		c.scbdesc_crse_numb,
//		a.scbcrse_title,
//		cast(c.scbdesc_text_narrative as varchar2(3999)) scbdesc_text_narrative,
//   from scbcrse a, scbcrky, scbdesc c
//  where a.scbcrse_subj_code = scbcrky.scbcrky_subj_code
//	and a.scbcrse_crse_numb = scbcrky.scbcrky_crse_numb
//	and scbcrky.scbcrky_term_code_end = '999999'
//	and a.scbcrse_subj_code <> '-'
//	and a.scbcrse_eff_term =
//		(select max(b.scbcrse_eff_term)
//		   from scbcrse b
//		  where b.scbcrse_subj_code = a.scbcrse_subj_code
//			and b.scbcrse_crse_numb = a.scbcrse_crse_numb)
//	and a.scbcrse_crse_numb not like 'T%'
//	and c.scbdesc_subj_code = a.scbcrse_subj_code
//	and c.scbdesc_crse_numb = a.scbcrse_crse_numb
//	and a.scbcrse_dept_code = ?
//	and a.scbcrse_coll_code = ?
//	and c.scbdesc_term_code_eff =
//		(select max(b.scbdesc_term_code_eff)
//		   from scbdesc b
//		  where c.scbdesc_subj_code = b.scbdesc_subj_code
//			and c.scbdesc_crse_numb = b.scbdesc_crse_numb)
//  order by 1, 2
		
		def getCourseAttributes (String subjectCode, String courseNumber) {
			def sql = new Sql(dataSource)
			return sql.rows(
				"""
					/*select stvattr_desc attribute
					from scrattr, stvattr
					where scrattr.scrattr_subj_code = ?
					and scrattr.scrattr_crse_numb = ?
					and scrattr.scrattr_attr_code = stvattr.stvattr_code*/
					select stvattr_desc attribute
					from scrattr a, stvattr
					where a.scrattr_subj_code = ?
					and a.scrattr_crse_numb = ?
					and a.scrattr_attr_code = stvattr.stvattr_code
					and scrattr_eff_term = (
					  select max(b.scrattr_eff_term)
						from scrattr b
					   where a.scrattr_crse_numb = b.scrattr_crse_numb
						 and a.scrattr_subj_code = b.scrattr_subj_code)

				""",[subjectCode,  courseNumber]
			).collect {"${it.attribute}"}.unique()
			
		}
		
		def getMajorsFromDepartment(String department) {
			def sql = new Sql(dataSource)
			def stvdepts = sql.rows(
				"""
					select distinct a.sorcmjr_majr_code, x.stvmajr_desc
					from sobcurr, smrprle, sorcmjr a, stvmajr x, stvcoll
					where sobcurr.sobcurr_curr_rule = a.sorcmjr_curr_rule
					and sobcurr.sobcurr_coll_code = stvcoll_code
					and a.sorcmjr_dept_code = ?
					and sobcurr.sobcurr_program = smrprle_program
					and a.sorcmjr_majr_code = x.stvmajr_code
					and smrprle.smrprle_levl_code = '01'
					and a.sorcmjr_adm_ind = 'Y'
					and a.sorcmjr_term_code_eff = (
						select max(b.sorcmjr_term_code_eff)
                        from sorcmjr b
                        where a.sorcmjr_curr_rule = b.sorcmjr_curr_rule
					)  
				""",[department]
			)
			def results = stvdepts.collect {
				"${it.stvmajr_desc}"
			}.unique()
			return results
		}

		def getDepartmentsFromBanner(String college) {
			def sql = new Sql(dataSource)
			def stvdepts = sql.rows(
				"""
					select distinct a.sorcmjr_dept_code, stvdept_desc
					from sobcurr, smrprle, sorcmjr a, stvmajr x, stvcoll, stvdept
					where sobcurr.sobcurr_curr_rule = a.sorcmjr_curr_rule
					and sobcurr.sobcurr_coll_code = ?
					and a.sorcmjr_majr_code like '%'
					and stvdept_code = a.sorcmjr_dept_code
					and sobcurr.sobcurr_program = smrprle_program
					and a.sorcmjr_majr_code = x.stvmajr_code
					and smrprle.smrprle_levl_code = '01'
					and a.sorcmjr_adm_ind = 'Y'
					and a.sorcmjr_term_code_eff = (
						select max(b.sorcmjr_term_code_eff)
                        from sorcmjr b
                        where a.sorcmjr_curr_rule = b.sorcmjr_curr_rule
					)  
				""",[college]
			)
			def results = stvdepts.collect {
				"${it.SORCMJR_DEPT_CODE}:${it.stvdept_desc}"
			}.unique()
			return results
		}
		
		def getMajorsFromBanner(String college) {
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			stvmajrs = sql.rows("select distinct x.stvmajr_desc, x.stvmajr_code, '1' sortType from sobcurr, smrprle, sorcmjr a, stvmajr x, stvcoll where sobcurr.sobcurr_curr_rule = a.sorcmjr_curr_rule and sobcurr.sobcurr_coll_code = stvcoll_code and stvcoll_desc = ? and sobcurr.sobcurr_program = smrprle_program and a.sorcmjr_majr_code = x.stvmajr_code and a.sorcmjr_adm_ind = 'Y' and a.sorcmjr_term_code_eff = (select max(b.sorcmjr_term_code_eff) from sorcmjr b where a.sorcmjr_curr_rule = b.sorcmjr_curr_rule) union select '==============', ' ', '2' from sys.dual union select distinct x.stvmajr_desc || ' (Minor)', x.stvmajr_code, '3' sortType from sobcurr, smrprle, sorcmnr a, stvmajr x where sobcurr.sobcurr_curr_rule = a.sorcmnr_curr_rule and sobcurr.sobcurr_coll_code = '00' and sobcurr.sobcurr_program = smrprle_program and a.sorcmnr_majr_code_minr = x.stvmajr_code and a.sorcmnr_adm_ind = 'Y' and a.sorcmnr_term_code_eff = (select max(b.sorcmnr_term_code_eff) from sorcmnr b where a.sorcmnr_curr_rule = b.sorcmnr_curr_rule and a.sorcmnr_majr_code_minr = b.sorcmnr_majr_code_minr) order by 3,1",[college])
			def results = stvmajrs.collect {
				"${it.STVMAJR_DESC}"
			}.unique()
			return results
		}
		
		def getRows(String sqlStatement){
//			def sql = Sql.newInstance("jdbc:oracle:thin:@umadmn.umt.edu:7895:admnred", "degreq_usr", "degreq!@#", "oracle.jdbc.OracleDriver")
			def sql = new Sql(dataSource)
			return sql.rows(sqlStatement)
		}
		
		def getYears() {
			def years = []
			def degrees = Degree.findAll()
			degrees.each() {
				years.add(it.catalogYear)
			}
			years.removeAll([null])
			years.unique().sort()
			//println years
		}
		
		def saveCourse(CourseBlock courseBlock, params){
			def course = new Course(courseSubject:params.courseSubject, courseNumber:params.courseNumber, title:params.title, courseCredits:params.courseCredits, term:params.term, courseYear:params.courseYear,courseBlock: courseBlock )
			course.save(flush:true, failOnError:true)
			return course
		}
		
		def updateCourse(params){
			def course = Course.get(params.courseId)
			course.term = params.term
			course.save(flush:true, failOnError:true)
			return course
		}
		
		def saveCourseBlock(DegreeBlock degreeBlock, params){
			def courseBlock = new CourseBlock(name:params.name, rule:params.rule, credits:params.credits,eval:params.eval,prereqNotes:params.prereqNotes, degreeBlock: degreeBlock)
			courseBlock.save(flush:true, failOnError:true)
			courseBlock.sortId = courseBlock.id;
			courseBlock.save(flush:true, failOnError:true)
			return courseBlock
		}
		
		def saveDegree(params){
	
			def degree = new Degree(params)
			degree.save(flush:true, failOnError:true)
			
			def degreeBlock = new DegreeBlock(blockType: params.blockType, degreeBlockComment:params.degreeBlockComment, degree: degree)
			degreeBlock.save(flush:true, failOnError:true)
			
			def courseBlock = new CourseBlock(name:params.name, rule:params.rule, credits:params.credits,eval:params.eval,prereqNotes:params.prereqNotes, degreeBlock: degreeBlock)
			courseBlock.save(flush:true, failOnError:true)
			
			def course = new Course(courseSubject:params.courseSubject, courseNumber:params.courseNumber, title:params.title, credits:params.credits,term:params.term, courseYear:params.courseYear, courseBlock: courseBlock)
			course.save(flush:true, failOnError:true)
			
			return degree
		}

		def saveDegreeKeyWords(params){
			params.remove('save')
			params.remove('action')
			params.remove('controller')
			
			// delete existing keywords for this degree
			def degreeKeywords = DegreeKeyword.findAllWhere(degreeId: params.degree.toLong())
			degreeKeywords.each{
				it.delete()
			}
			
			// add the keywords saved by user
			params.each{
				if (it.value.contains("on")){
					DegreeKeyword degreeKeyword = new DegreeKeyword()
					degreeKeyword.degreeId = params.degree.toLong()
					degreeKeyword.keywordId = it.key.toLong()
					degreeKeyword.updatedBy =  springSecurityService.getPrincipal().username
					degreeKeyword.save(flush:true, failOnError:true)
				}
			}
			
			return			
		}
		
		def saveDegreeBlock(Degree degree, DegreeBlockType degreeBlockType, params){
			def degreeBlock = new DegreeBlock(degreeBlockType: degreeBlockType, degree: degree, displaySeqno:params.displaySeqno)
			degreeBlock.sortId = degreeBlock.id;
			degreeBlock.save(flush:true, failOnError:true)
			degreeBlock.sortId = degreeBlock.id;
			degreeBlock.save(flush:true, failOnError:true)
			return degreeBlock
		}
		
		def sortDegreeBlockTypes (List degreeBlockTypes) {
			degreeBlockTypes.sort { a, b ->
				def comparison = (a.compareTo(b))
				if (comparison) return comparison
				return 0
			}
			return degreeBlockTypes
		}
		
		def updateCourses(params){
			JSONObject courseJson = JSON.parse(params.courseList)
			return
		}
		
		def updateDegree(params){
 			def degreeInstance = Degree.get(params.degree)
			degreeInstance.catalogYear = params.catalogYear
			degreeInstance.college = params.college
			degreeInstance.degreeType = params.degreeType
			degreeInstance.degreeSubject = params.degreeSubject
			degreeInstance.totalCredits = params.totalCredits.toLong()
			degreeInstance.level = params.level
			degreeInstance.degreeConcentration = params.degreeConcentration
			degreeInstance.cumulativeGPArequired = params.cumulativeGPArequired.toDouble()
			degreeInstance.degreeApproved = params.degreeApproved
			degreeInstance.degreeComment = params.degreeComment.encodeAsHTML().replaceAll('\n',"<br>")
			degreeInstance.degreeTrack = params.degreeTrack
			degreeInstance.save()
			
			//more than one degreeBlock and more than one courseBlock
			if(degreeInstance.degreeBlocks?.size() > 1){
				updateDegreeBlocks(params)
				updateCourseBlocks(params)
			}
			//one degreeBlock and one courseBlock
			else if(degreeInstance.degreeBlocks?.size() == 1 && degreeInstance.getNumberCourseBlocks() == 1){
				updateDegreeBlock(params.degreeBlock, params.degreeBlockComment)
				updateCourseBlock(params.courseBlockId, params.credits, params.categoryName, params.eval, params.rule, params.courseBlockComment, params.sortId)
			}
			//one degreeBlock and more than one courseBlock
			else if(degreeInstance.degreeBlocks?.size() == 1 && degreeInstance.getNumberCourseBlocks() > 1){
				updateDegreeBlock(params.degreeBlock, params.degreeBlockComment)
				updateCourseBlocks(params)
			}
			
			return
		}
		def updateDegreeBlocks(params){
			for(int i = 0; i< params.degreeBlock?.size(); i++){
				updateDegreeBlock(params.degreeBlock[i],params.degreeBlockComment[i])
			}

			return
		}
		def updateDegreeBlock(String degreeBlock, String degreeBlockComment){
			def degreeBlockInstance = DegreeBlock.get(degreeBlock)
			degreeBlockInstance?.degreeBlockComment = degreeBlockComment.encodeAsHTML().replaceAll('\n',"<br>")
			degreeBlockInstance.save(flush:true)
			
			return
		}
		def updateCourseBlocks(params){
			for(int i = 0; i< params.courseBlockId?.size(); i++){
				updateCourseBlock(params.courseBlockId[i],params.credits[i],params.categoryName[i],params.eval[i],params.rule[i], params.courseBlockComment[i], params.sortId[i])
			}
			
			return
		}
		def updateCourseBlock(String courseBlockId, String credits, String categoryName, String eval, String rule, String courseBlockComment, String sortId){
			def courseBlockInstance = CourseBlock.get(courseBlockId)
			courseBlockInstance.courseBlockCredits = credits
			courseBlockInstance.name = categoryName
			courseBlockInstance.eval = eval
			courseBlockInstance.rule = rule
			courseBlockInstance.courseBlockComment = courseBlockComment.encodeAsHTML().replaceAll('\n',"<br>")
			courseBlockInstance.sortId = sortId.toLong()
			courseBlockInstance.save(flush:true)
			return
		}
		
		def copyDegree(def degreeInstance){
			def newDegree = deepClone(degreeInstance)
			return newDegree
		}
		
		def copyDegreesByCatalogYear(String oldCatalogYear, String newCatalogYear){
			// is year copyable
			// has year been copied
		    // copy
			// update new degree catalog year
			// unlock all new degrees
			def degrees = Degree.findAllByCatalogYear(oldCatalogYear)
			//println "copyDegreeByCatalogYear() pre closure"
			degrees.each { oldDegree ->
				Degree newDegree = deepClone(oldDegree)
				newDegree.catalogYear = newCatalogYear 
				newDegree.degreeApproved = null
                newDegree.catalogId = oldDegree.catalogId
				newDegree.save()
				cloneKeywords (oldDegree, newDegree)
			}
		}
		
		def cloneKeywords(Degree degreeInstance, Degree newDegree){
			degreeInstance.keywords.each{
				DegreeKeyword degreeKeyword = new DegreeKeyword()
				degreeKeyword.degreeId = newDegree.id.toLong()
				degreeKeyword.keywordId = it.id.toLong()
				degreeKeyword.updatedBy =  springSecurityService.getPrincipal().username
				degreeKeyword.save(flush:true, failOnError:true)
			}
		}
		
		public Object deepClone(domainInstanceToClone) {
			//Our target instance for the instance we want to clone
			def newDomainInstance = domainInstanceToClone.getClass().newInstance()
			
			//Returns a DefaultGrailsDomainClass (as interface GrailsDomainClass) for inspecting properties
			def domainClass = grailsApplication.getDomainClass(newDomainInstance.getClass().name)
			
			domainClass?.persistentProperties.each {prop ->
				if(prop.name != "keywords"){
					if (prop.association && prop.name != "degreeBlockType") {
						if (prop.owningSide) {
							//we have to deep clone owned associations
							if (prop.oneToOne) {
								def newAssociationInstance = deepClone(domainInstanceToClone?."${prop.name}")
								newDomainInstance."${prop.name}" = newAssociationInstance
							}
							else {
								domainInstanceToClone."${prop.name}".each { associationInstance ->
									def newAssociationInstance = deepClone(associationInstance)
									newDomainInstance."addTo${StringUtils.capitalize(prop.name)}"(newAssociationInstance)
								}
							}
						}
					}
					else {
						//If the property isn't an association then simply copy the value
						newDomainInstance."${prop.name}" = domainInstanceToClone."${prop.name}"
					}
				}
			}
			
			return newDomainInstance
		}
		def copy(Degree degreeInstance){
			def newDegree = new Degree()
			newDegree.catalogYear = degreeInstance.catalogYear
			newDegree.college = degreeInstance.college
			newDegree.degreeType = degreeInstance.degreeType
			newDegree.degreeSubject = degreeInstance.degreeSubject
			newDegree.totalCredits = degreeInstance.totalCredits.toLong()
			newDegree.level = degreeInstance.level
			newDegree.degreeConcentration = degreeInstance.degreeConcentration
			newDegree.cumulativeGPArequired = degreeInstance.cumulativeGPArequired.toDouble()
			newDegree.degreeComment = degreeInstance.degreeComment
			newDegree.degreeTrack = degreeInstance.degreeTrack
			
			return newDegree
		}
}
