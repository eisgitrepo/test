package degreebuilder

import java.text.DecimalFormat
import java.text.NumberFormat
import java.util.List;

import org.codehaus.groovy.grails.web.mapping.LinkGenerator
import org.grails.jaxrs.provider.DomainObjectNotFoundException

class DegreeResourceService {

	def degreebuilderService
	
	LinkGenerator grailsLinkGenerator
	
	def getYears() {
		degreebuilderService.getYears()
	}

	def readByCatalogYear(year) {
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYear(year))
		degrees.each() {
			it.degreeBlocks = null
		}
		degrees
	}
	
	def readDegreesByCatalogYear(year) {
		NumberFormat df = new DecimalFormat("0.0##")
		
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYear(year))
		def degreesDisplay = []
		for (Degree degree: degrees){
			
			def currentDegree = [:]
			currentDegree['id'] = degree.catalogId
			currentDegree['originalId'] = degree.id
			currentDegree['college'] = degree.college
			currentDegree['degreeSubject'] = degree.degreeSubject
			currentDegree['catalogYear'] = degree.catalogYear
			currentDegree['level'] = degree.level
			currentDegree['degreeType'] = degree.degreeType
			currentDegree['degreeTrack'] = degree.degreeTrack
			currentDegree['totalCredits'] = degree.totalCredits
			currentDegree['cumulativeGPArequired'] = df.format(degree.cumulativeGPArequired)
			currentDegree['capstone'] = degree.capstone
			currentDegree['degreeComment'] = degree.degreeComment?.trim()
			currentDegree['degreeConcentration'] = degree.degreeConcentration
			currentDegree['link'] = grailsLinkGenerator.link(uri: "/api/degree/${degree.id}", absolute: 'true')
			degreesDisplay.add(currentDegree)
		}
		degreesDisplay
	}
	
	def readCollegesByCatalogYear(year) {
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYear(year))
		
		def collegeNames = []
		degrees.each() {
			collegeNames.add(it.college)
		}
		collegeNames.unique()
		
		def collegeCodeValuePairs = new CodeValueList(degreebuilderService.getCollegesFromBanner()).codeValues
		def colleges = []
		collegeNames.each{
			def collegeDisplay = [:]
			def name = it
			def code = collegeCodeValuePairs.find { it.value == name }.code
			collegeDisplay['name'] = it
			collegeDisplay['code'] = code
			collegeDisplay['link'] = grailsLinkGenerator.link(uri: "/api/${year}/${code.replaceAll(' ','%')}", absolute:'true')
			colleges.add(collegeDisplay)
		}
		colleges
	}

	def removeUnapprovedDegrees(List degrees) {
		degrees.removeAll (degrees.findAll { !(it.degreeApproved == '1') })
		degrees
	}
	
	def readDegreesByCatalogYearAndCollege(year, collegeCode){
		NumberFormat df = new DecimalFormat("0.0##")
		
		def collegeCodeValuePairs = new CodeValueList(degreebuilderService.getCollegesFromBanner()).codeValues
		def collegeName = collegeCodeValuePairs.find { it.code == collegeCode }.value
//		def degrees = Degree.findAllByCatalogYearAndCollege(year, collegeName)
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYearAndCollege(year, collegeName))
		def degreesDisplay = []
		for (Degree degree: degrees){
			def currentDegree = [:]
			currentDegree['id'] = degree.catalogId
			currentDegree['originalId'] = degree.id
			currentDegree['college'] = degree.college
			currentDegree['degreeSubject'] = degree.degreeSubject
			currentDegree['catalogYear'] = degree.catalogYear
			currentDegree['level'] = degree.level
			currentDegree['degreeType'] = degree.degreeType
			currentDegree['degreeTrack'] = degree.degreeTrack
			currentDegree['totalCredits'] = degree.totalCredits
			currentDegree['cumulativeGPArequired'] = df.format(degree.cumulativeGPArequired)
			currentDegree['capstone'] = degree.capstone
			currentDegree['degreeComment'] = degree.degreeComment?.trim()
			currentDegree['degreeConcentration'] = degree.degreeConcentration
			currentDegree['link'] = grailsLinkGenerator.link(uri: "/api/degree/${degree.id}", absolute: 'true')
			degreesDisplay.add(currentDegree)
		}
		degreesDisplay
	}

	def readDepartmentsByCatalogYearAndCollege (year, collegeCode) {
		NumberFormat df = new DecimalFormat("0.0##")
		
		def collegeCodeValuePairs = new CodeValueList(degreebuilderService.getCollegesFromBanner()).codeValues
		def collegeName = collegeCodeValuePairs.find { it.code == collegeCode }.value
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYearAndCollege(year, collegeName))
		def departments = degreebuilderService.getDepartmentsFromBanner(collegeCode)
		
		def departmentsDisplay = []
		for (String department: departments){
			def currentDepartment = [:]
			def result = []
			result = department.split(':')
			currentDepartment['code'] = result[0]
			currentDepartment['description'] = result[1]
			currentDepartment['link'] = grailsLinkGenerator.link(uri: "/api/${year}/${collegeCode}/${currentDepartment.code}", absolute:'true')
			
			def majors = degreebuilderService.getMajorsFromDepartment(currentDepartment['code'])
			def degreesForDepartment = []
			for (String major: majors){
				for (Degree degree : degrees){
					if (major == degree.degreeSubject){
						def currentDegree = [:]
						currentDegree['id'] = degree.catalogId
						currentDegree['originalId'] = degree.id
						currentDegree['college'] = degree.college
						currentDegree['degreeSubject'] = degree.degreeSubject
						currentDegree['catalogYear'] = degree.catalogYear
						currentDegree['level'] = degree.level
						currentDegree['degreeType'] = degree.degreeType
						currentDegree['degreeTrack'] = degree.degreeTrack
						currentDegree['totalCredits'] = degree.totalCredits
						currentDegree['cumulativeGPArequired'] = df.format(degree.cumulativeGPArequired)
						currentDegree['capstone'] = degree.capstone
						currentDegree['degreeComment'] = degree.degreeComment?.trim()
						currentDegree['degreeConcentration'] = degree.degreeConcentration
						currentDegree['link'] = grailsLinkGenerator.link(uri: "/api/degree/${degree.id}", absolute: 'true')

						degreesForDepartment.add(currentDegree)
					}
				}
			}
			currentDepartment['degrees'] = degreesForDepartment
			departmentsDisplay.add(currentDepartment)
		}
		departmentsDisplay
	}
	
	def readDegreesByCatalogYearCollegeAndDepartment (year, collegeCode, departmentCode) {
		NumberFormat df = new DecimalFormat("0.0##")
		
		def collegeCodeValuePairs = new CodeValueList(degreebuilderService.getCollegesFromBanner()).codeValues
		def collegeName = collegeCodeValuePairs.find { it.code == collegeCode }.value
		def degrees = removeUnapprovedDegrees(Degree.findAllByCatalogYearAndCollege(year, collegeName))
		def majors = degreebuilderService.getMajorsFromDepartment(departmentCode)
		def courses = degreebuilderService.getCoursesFromBannerByCollegeAndDept(collegeCode, departmentCode)
		
		def obj = []
		def displayDegrees = []
		def degreesDisplay = []
		def displayCourses = []
		def coursesDisplay = []
		def subjectsDisplay = []
		
		for (String major: majors){
			for (Degree degree: degrees){
				if (major == degree.degreeSubject){
					displayDegrees.add(degree)
				}
			}
		}
		
		displayDegrees.each{
			def degreeDisplay = [:]
			degreeDisplay['id'] = it.catalogId
			degreeDisplay['originalId'] = it.id
			degreeDisplay['college'] = it.college
			degreeDisplay['degreeSubject'] = it.degreeSubject
			degreeDisplay['catalogYear'] = it.catalogYear
			degreeDisplay['level'] = it.level
			degreeDisplay['degreeType'] = it.degreeType
			degreeDisplay['degreeTrack'] = it.degreeTrack
			degreeDisplay['totalCredits'] = it.totalCredits
			degreeDisplay['cumulativeGPArequired'] = df.format(it.cumulativeGPArequired)
			degreeDisplay['capstone'] = it.capstone
			degreeDisplay['degreeComment'] = it.degreeComment?.trim()
			degreeDisplay['degreeConcentration'] = it.degreeConcentration
			degreeDisplay['link'] = grailsLinkGenerator.link(uri: "/api/degree/${it.id}", absolute: 'true')
			degreesDisplay.add(degreeDisplay)
		}
		obj.add("degrees":degreesDisplay)
		
		String prevSubjectCode = null
		String currentSubjectCode
		def subjectDisplay = [:]
		def courseDisplay = [:]
				
		courses.each{ subject ->
			currentSubjectCode = subject.scbdesc_subj_code
			if (currentSubjectCode != prevSubjectCode) {
				subjectDisplay = [:]
				subjectDisplay['code'] = subject.scbdesc_subj_code
				subjectDisplay['description'] = subject.stvsubj_desc
				coursesDisplay = []
				courses.each { course ->
					if(course.scbdesc_subj_code == subject.scbdesc_subj_code){
						courseDisplay = [:]
						courseDisplay['courseSubject'] = course.scbdesc_subj_code
						courseDisplay['courseNumber'] = course.scbdesc_crse_numb
						courseDisplay['courseCredits'] = course.credits						
						courseDisplay['title'] = course.scbcrse_title
						courseDisplay['description'] = course.scbdesc_text_narrative
						courseDisplay['grad'] = course.grad
						courseDisplay['undergrad'] = course.undergrad
						courseDisplay['attribute'] = degreebuilderService.getCourseAttributes(course.scbdesc_subj_code, course.scbdesc_crse_numb)
						coursesDisplay.add(courseDisplay)
					}
					subjectDisplay['courses'] = coursesDisplay
				}
				subjectsDisplay.add(subjectDisplay)
				prevSubjectCode = subject.scbdesc_subj_code
			}
			
		}
		
		obj.add("subjects":subjectsDisplay)
		obj
	}
	
	def read(id) {

		def degree = Degree.get(id)
		if (!degree) {
			throw new DomainObjectNotFoundException(Degree.class, id)
		}
		degreebuilderService.getDegreeCourseDescriptions(degree)
		
		degree
	}

	def readLawCourses() {
	    def courses = degreebuilderService.getLawCoursesFromBanner()
        
		courses
	}
	
	
	def readById(idList){
		List degreeList = []
		idList.each{
			if(it.substring(it.length() -1) == "/"){
				it = it.substring(0,it.length()-1)
			}
			def degree = Degree.get(it.toLong())
			if(!degree){
				throw new DomainObjectNotFoundException(Degree.class, it)
			}
			if (!degree.degreeApproved){
				return
				//throw new DomainObjectNotFoundException(Degree.class, it)
			}
			degreebuilderService.getDegreeCourseDescriptions(degree)
			degreeList.add(degree)
		}
		degreeList
	}
	
    def readByCatalogId(catalogIdList,catalogYear){
		List degreeList = []

		catalogIdList.each{
			if(it.substring(it.length() -1) == "/"){
				it = it.substring(0,it.length()-1)
			}

			//def degree = Degree.get(it.toLong())
            def degree = Degree.findByCatalogIdAndCatalogYear(it.toLong(),catalogYear)
			if(!degree){
				throw new DomainObjectNotFoundException(Degree.class, it)
			}
			if (!degree.degreeApproved){
				return
				//throw new DomainObjectNotFoundException(Degree.class, it)
			}
			degreebuilderService.getDegreeCourseDescriptions(degree)
			degreeList.add(degree)
		}
		degreeList
	}
	
	
	def readDegreesByWord(wordList){
		NumberFormat df = new DecimalFormat("0.0##")
		
		//for each keyword
		//find the keyword id
		//find the degree id mapped to that keyword id
		//add id to the lookup list if it isn't already there
		//return readById(lookup list)
		ArrayList degreeIdList = []
		wordList.each{
			if(it.substring(it.length() -1) == "/"){
				it = it.substring(0,it.length()-1)
			}
			degreebuilderService.getDegreeIdsByWord(it).each{
				degreeIdList.add(it)
			}
		}
		def degreeList = readById(degreeIdList)
		
		def degrees = removeUnapprovedDegrees(degreeList)
		def degreesDisplay = []
		degreeList.each{
			def currentDegree = [:]
			currentDegree['id'] = it.catalogId
			currentDegree['originalId'] = it.id
			currentDegree['college'] = it.college
			currentDegree['degreeSubject'] = it.degreeSubject
			currentDegree['catalogYear'] = it.catalogYear
			currentDegree['level'] = it.level
			currentDegree['degreeType'] = it.degreeType
			currentDegree['degreeTrack'] = it.degreeTrack
			currentDegree['totalCredits'] = it.totalCredits
			currentDegree['cumulativeGPArequired'] = df.format(it.cumulativeGPArequired)
			currentDegree['capstone'] = it.capstone
			currentDegree['degreeComment'] = it.degreeComment?.trim()
			currentDegree['degreeConcentration'] = it.degreeConcentration
			currentDegree['link'] = grailsLinkGenerator.link(uri: "/api/degree/${it.id}", absolute: 'true')
			degreesDisplay.add(currentDegree)
		}
		degreesDisplay
	}
	def readDegreesByCatalogYearAndWord(String catalogYear, def wordList){
		
		NumberFormat df = new DecimalFormat("0.0##")
		
		//for each keyword
		//find the keyword id
		//find the degree id mapped to that keyword id
		//add id to the lookup list if it isn't already there
		//return readById(lookup list)
		ArrayList degreeIdList = []
				wordList.each{
			if(it.substring(it.length() -1) == "/"){
				it = it.substring(0,it.length()-1)
			}
			degreebuilderService.getDegreeIdsByCatalogYearAndWord(catalogYear,it).each{
				degreeIdList.add(it)
			}
		}
		def degreeList = readById(degreeIdList)
				
				def degrees = removeUnapprovedDegrees(degreeList)
				def degreesDisplay = []
						degreeList.each{
			def currentDegree = [:]
					currentDegree['id'] = it.catalogId
					currentDegree['originalId'] = it.id
					currentDegree['college'] = it.college
					currentDegree['degreeSubject'] = it.degreeSubject
					currentDegree['catalogYear'] = it.catalogYear
					currentDegree['level'] = it.level
					currentDegree['degreeType'] = it.degreeType
					currentDegree['degreeTrack'] = it.degreeTrack
					currentDegree['totalCredits'] = it.totalCredits
					currentDegree['cumulativeGPArequired'] = df.format(it.cumulativeGPArequired)
					currentDegree['capstone'] = it.capstone
					currentDegree['degreeComment'] = it.degreeComment?.trim()
							currentDegree['degreeConcentration'] = it.degreeConcentration
							currentDegree['link'] = grailsLinkGenerator.link(uri: "/api/degree/${it.id}", absolute: 'true')
							degreesDisplay.add(currentDegree)
		}
		degreesDisplay
	}


	def getColleges() {
		def results = degreebuilderService.getCollegesFromBanner()
		def descResults = []
		results.collect {
			descResults.add(it.STVCOLL_DESC)
		}
		descResults
	}
	
	def getCourseDescription(String subjectCode, String courseNumber) {
		def results = degreebuilderService.getCourseDescription(subjectCode, courseNumber)
		results
	}
	
	def getCourseDescriptions(courseIdsList){
		def descList = []
		
		courseIdsList.each{
			String subjectCode
			String courseNumber
			if(it.substring(it.length() -1) == "/"){
				subjectCode = it.substring(0,it.indexOf("/"))
				courseNumber = it.substring(it.indexOf("/")+1,it.length()-1)
			}
			else{
				subjectCode = it.substring(0,it.indexOf("/"))
				courseNumber = it.substring(it.indexOf("/")+1,it.length())
			}
			def titleAndDescription = degreebuilderService.getCourseDescription(subjectCode, courseNumber)
			def course = [:]
			def courseInfo = [:]
			titleAndDescription.each {
			  courseInfo['title'] = titleAndDescription.title.toString().replace("[","").replace("]","")
			  courseInfo['description'] = titleAndDescription.descr.toString().replace("[","").replace("]","")
			  courseInfo['credits'] = titleAndDescription.credits.toString().replace("[","").replace("]","")
			}

			course[subjectCode+courseNumber] = courseInfo
			descList.add(course)
		}
		descList
	}
		
}
