var blockType;
var elementToRemove;
var currentCourseBlockId;
var currentCourseId;
var currentClassTable;
var currentCourseSubject;
var currentCourseNumber;
var currentCourseTitle;
var currentCourseCredits;
var currentCourseTerm;
var displaySeqno;
$(function() {
//	$("ul li").each(function() {
//		$(this).draggable({
//			helper : "clone"
//		});
//	});
//	$("#droppable").droppable(
//			{
//				activeClass : "ui-state-hover",
//				hoverClass : "ui-state-active",
//				accept : ":not(.ui-sortable-helper)",
//				drop : function(event, ui) {
//					var targetElem = $(this).attr("id");
//					$(this).height(function(index, height) {
//						return (height + 200);
//					}).slideDown();
//					if ($(ui.draggable).hasClass('draggable-source'))
//						$(ui.draggable).clone().appendTo(this).removeClass(
//								'draggable-source');
//					else
//						$(ui.draggable).appendTo(this);
//					console.log(this.id)
//				}
//			}).sortable({
//		items : "li:not(.placeholder)",
//		sort : function() {
//			$(this).removeClass("ui-state-default");
//		}
//	});
	
//	$('#elements  li').click(function(){
//		$('#elements li').not($(this)).removeClass('selectedAdd');
//		$(this).toggleClass('selectedAdd');
//		var element = $(this);
//		blockType = element.find("#blockType").val();
//		displaySeqno = element.find("#displaySeqno").val();
//	});
//	$("#removeElement").click(function(e){
//		$('#right-side ul li.selected').appendTo($('#elements'));
//	});
});
//Get the selected course in the course search tool
$(document).on('click',".courseSearchTable tr",function(e){
	$(this).siblings('tr').removeClass('selectedCourse');
	$(this).toggleClass('selectedCourse');
	var courseCreditLow = $(this).closest('tr').children('td.courseCreditLow').text();
	var courseCreditInd = $(this).closest('tr').children('td.courseCreditInd').text();
	var courseCreditHigh = $(this).closest('tr').children('td.courseCreditHigh').text();
	/*
	 	For courses with a null value in the indicator, the low value should be utilized. 
		For courses with a TO value, the range should be displayed (both low and high). 
		For courses with an OR value, the high value should be displayed. 
	 * */
	if(courseCreditInd != "TO" && courseCreditInd != "OR"){
		currentCourseCredits = courseCreditLow;
	}
	else if(courseCreditInd == "TO"){
		currentCourseCredits = courseCreditLow +" To " + courseCreditHigh;
	}
	else if(courseCreditInd == "OR"){
		currentCourseCredits = courseCreditHigh;
	}
	else{
		currentCourseCredits = courseCreditLow;
	}
	currentCourseSubject = $(this).closest('tr').children('td.courseSubject').text();
	currentCourseNumber = $(this).closest('tr').children('td.courseNumber').text();
	currentCourseTitle = $(this).closest('tr').children('td.courseTitle').text();
//	currentCourseCredits = $(this).closest('tr').children('td.courseCreditLow').text() + courseCreditHigh;
});

//add the selected course and additional information to the degree
$(document).on('click',".addCourse",function(e){
	var courseTable = $('#classList');
	var term = $(".courseSemesters :checked").map(function(_,el){
		return $(el).val();
	}).get().sort();
	$.ajax({
		type:'POST',
		data:'courseBlockId='+ escape(currentCourseBlockId)+'&courseSubject=' + escape(currentCourseSubject)+'&courseNumber=' 
			+ escape(currentCourseNumber)+'&title=' + encodeURIComponent(currentCourseTitle)+'&courseCredits=' 
			+ escape(currentCourseCredits)+'&term=' + escape(term), 
		url:'/degreebuilder/degree/createCourse',
		success:function(data){
			$(currentClassTable).html(data);
			$('.selectedCourse').toggleClass('selectedCourse');
			$('#courseYearSelect').val('');
			$('.courseSemesters').find('input:checkbox').prop('checked', false); 

			},
		error:function(XMLHttpRequest){alert("Duplicate Course - This course has already been added.");}	
	});
});
$(document).on('keyup','.searchTerm', function(e){
	var inputVal = $(this).val();
	var table = $('#courseSearchTable');
	table.find('tr').each(function(index, row){
		var allCells = $(row).find('td');
		if(allCells.length > 0)
		{
			var found = false;
			allCells.each(function(index, td)
			{
				var regExp = new RegExp(inputVal, 'i');
				if(regExp.test($(td).text()))
				{
					found = true;
					return false;
				}
			});
			if(found == true)$(row).show();else $(row).hide();
		}
	});
});

$(document).on('click',".courseEdit",function(e){
	currentCourseId = $(this).parent().parent().find('.courseId').html();
	currentClassTable = $(this).parent().parent().parent().parent().parent();
	$("#currentCourseId").html( $(this).parent().parent().find('.courseId').html() ); 
	$("#currentCourseSubject").html( $(this).parent().parent().find('.courseSubject').html() ); 
	$("#currentCourseNumber").html( $(this).parent().parent().find('.courseNumber').html() ); 
	$("#currentCourseTitle").html( $(this).parent().parent().find('.title').html() ); 
	$("#currentCourseCredits").html( $(this).parent().parent().find('.courseCredits').html() ); 
	$("#currentCourseTerm").html( $(this).parent().parent().find('.term').html() ); 
	
    $("#courseEdit").fadeIn(300);
  
    
    $('#springCB').prop('checked', false);
	$('#fallCB').prop('checked', false);
	$('#springOddCB').prop('checked', false);
	$('#fallOddCB').prop('checked', false);
	$('#springEvenCB').prop('checked', false);
	$('#fallEvenCB').prop('checked', false);
	$('#summerCB').prop('checked', false);
	$('#intermittentlyCB').prop('checked', false);
	var term_arr = $(this).parent().parent().find('.term').html().split(',');
    
    jQuery.each(term_arr, function(i,val){
//      console.log(val);
//  	console.log(i);    
    	    
        if (val == 'S') {
        	$('#springCB').prop('checked', true);
        }
        else if (val == 'F') {
        	$('#fallCB').prop('checked', true);
        }
        else if (val == 'SO') {
        	$('#springOddCB').prop('checked', true);
        }
        else if (val == 'FO') {
        	$('#fallOddCB').prop('checked', true);
        }
        else if (val == 'SE') {
        	$('#springEvenCB').prop('checked', true);
        }
        else if (val == 'FE') {
        	$('#fallEvenCB').prop('checked', true);
        }
        else if (val == 'SU') {
        	$('#summerCB').prop('checked', true);
        }
        else if (val == 'I') {
        	$('#intermittentlyCB').prop('checked', true);
        }
    });
});

$(document).ready(function() {
	var asInitVals = new Array();
	updateSubcategory();
	$(".elementBody").slideToggle();
	$(".btnShowModal").click(function(e) {
		ShowDialog(true);
		e.preventDefault();
	});
    $.ajax({
		type:'POST',
		url:'/degreebuilder/degree/displayDegreeBlockTypes',
		success:function(data){
			$('#elements').html(data);
			},
		error:function(XMLHttpRequest,textStatus,errorThrown){}
	});
	
	$(document).on('click',".btnShowModal",function(e){
		currentCourseBlockId = $(this).parent().parent().parent().find('#courseBlockId').val();
		currentClassTable = $(this).siblings('.classTable');
		ShowDialog(true);
		e.preventDefault();
		
	});
	
	$(".btnClose").click(function(e) {
		HideDialog();
		e.preventDefault();
	});
	
	$(document).on('click',".minimizeElement",function(e){
	    $(this).parent().siblings('.elementBody').slideToggle();
	});
	var oTable = $('#courseSearchTable').dataTable({
		"bScrollInfinite": true,
		"bScrollCollapes": true,
		"sScrollY": "200px"
	});
	
	$("thead input").keyup( function () {
		/* Filter on the column (the index) of this element */
		oTable.fnFilter( this.value, $("thead input").index(this) );
	});
			
	/*
	 * Support functions to provide a little bit of 'user friendlyness' to the textboxes in 
	 * the header
	 */
	$("thead input").each( function (i) {
		asInitVals[i] = this.value;
	});
	
	$("thead input").focus( function () {
		if ( this.className == "search_init" )
		{
			this.className = "";
			this.value = "";
		}
	});
	
	$("thead input").blur( function (i) {
		if ( this.value == "" )
		{
			this.className = "search_init";
			this.value = asInitVals[$("thead input").index(this)];
		}
	});
//	$(document).on('click',".elementHeader",function(e){
//		$(this).toggleClass('selectedRemove');
//		elementToRemove = $(this).parent();
//	});


//	$(document).on('click','#table > :not(thead)').delegate("tr","click",function(){
//		$(this).css("background-color","#FF3700");
//		$(this).fadeOut(400, function() {
//			$(this).remove();
//		});
//	});	
});//end of document ready

function ShowDialog(modal) {
	$("#overlay").show();
	$("#dialog").fadeIn(300);
	$(".courseSemesters :checked").prop("checked", false);
	if (modal) {
		$("#overlay").unbind("click");
	} else {
		$("#overlay").click(function(e) {
			HideDialog();
		});
	}
}

function removeElement(){
	var x;
	var r=confirm("Would you like to delete this degree element?");
	if (r==true){
		elementToRemove.remove();
		$.ajax({
			type:'POST',
			data:'degreeBlockId='+ escape(elementToRemove.find('input.degreeBlockId').val()), 
			url:'/degreebuilder/degree/deleteDegreeBlock',
			success:function(data,textStatus){
				
				},
			error:function(XMLHttpRequest,textStatus,errorThrown){}
		});
	}
	else{}
};

function updateCourse(){
	$("#courseEdit").hide();
    currentCourseTerm = $(".currentCourseTerm :checked").map(function(_,el){
		return $(el).val();
	}).get();
	$.ajax({
		type:'POST',
		data:'courseId='+ escape(currentCourseId) + '&term=' + escape(currentCourseTerm),
		url:'/degreebuilder/degree/updateCourse',
		success:function(data){
			$(currentClassTable).html(data);
		},
		error:function(XMLHttpRequest,textStatus,errorThrown){}
	});	
}

function HideDialog() {
	$("#overlay").hide();
	$("#dialog").fadeOut(300);
}

function updateSubcategory(){
	//for each degreeBlock, every child courseBlock that's not the first gets changed
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('label.category ').text("Subcategory Name");
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('label.category ').css('width','125px');
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('input#categoryName').css('margin-left','5px');
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('input#categoryName').css('width','87%');
	
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('label.rule ').text("Subcategory Rule");
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('label.rule ').css('width','125px');
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('input#rule').css('margin-left','5px');
	$(".degreeBlock").find(".courseBlock").not(':first-child').find('input#rule').css('width','87%');
};
